<?php
$db = [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=127.0.0.1;dbname=sikursus',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
if($_SERVER["SERVER_NAME"] == "demo.pukul.in"){
    $db = [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=127.0.0.1;dbname=app_ifl',
        'username' => 'root',
        'password' => '0274373122',
        'charset' => 'utf8',
    ];
}
return [
    'components' => [
        'db' => $db,
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];

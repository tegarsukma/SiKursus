-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2017 at 01:26 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sikursus`
--

-- --------------------------------------------------------

--
-- Table structure for table `action`
--

CREATE TABLE IF NOT EXISTS `action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` int(11) NOT NULL,
  `action` varchar(100) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role` (`role`),
  KEY `menu` (`menu`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1582 ;

--
-- Dumping data for table `action`
--

INSERT INTO `action` (`id`, `menu`, `action`, `role`) VALUES
(1299, 5, 'create', 1),
(1298, 5, 'view', 1),
(1297, 5, 'index', 1),
(1296, 4, 'registeruser', 1),
(1295, 4, 'delete', 1),
(1294, 4, 'update', 1),
(1293, 4, 'create', 1),
(1292, 4, 'view', 1),
(1291, 4, 'index', 1),
(1290, 3, 'setting', 1),
(1289, 3, 'delete', 1),
(1288, 3, 'update', 1),
(1287, 3, 'create', 1),
(1286, 3, 'view', 1),
(1285, 3, 'index', 1),
(1284, 2, 'delete', 1),
(1283, 2, 'update', 1),
(1282, 2, 'create', 1),
(1281, 2, 'view', 1),
(1280, 2, 'index', 1),
(1576, 18, 'delete', 2),
(1232, 25, 'view', 6),
(1231, 25, 'index', 6),
(1230, 24, 'delete', 6),
(1229, 24, 'update', 6),
(1228, 24, 'create', 6),
(1227, 24, 'view', 6),
(1226, 24, 'index', 6),
(1225, 21, 'delete', 6),
(1224, 21, 'update', 6),
(1223, 21, 'create', 6),
(1222, 21, 'view', 6),
(1221, 21, 'index', 6),
(1220, 20, 'delete', 6),
(1219, 20, 'update', 6),
(1218, 20, 'create', 6),
(1217, 20, 'view', 6),
(1216, 20, 'index', 6),
(1215, 18, 'delete', 6),
(1214, 18, 'update', 6),
(1213, 18, 'create', 6),
(1212, 18, 'view', 6),
(1211, 18, 'index', 6),
(1210, 17, 'delete', 6),
(1209, 17, 'update', 6),
(1208, 17, 'create', 6),
(1207, 17, 'view', 6),
(1206, 17, 'index', 6),
(1205, 8, 'delete', 6),
(1575, 18, 'update', 2),
(1574, 18, 'create', 2),
(1573, 18, 'view', 2),
(822, 25, 'create', 5),
(821, 25, 'view', 5),
(820, 25, 'index', 5),
(819, 24, 'reject', 5),
(818, 24, 'approve', 5),
(1572, 18, 'index', 2),
(1571, 17, 'delete', 2),
(1570, 17, 'update', 2),
(1569, 17, 'create', 2),
(817, 24, 'delete', 5),
(816, 24, 'update', 5),
(815, 24, 'create', 5),
(814, 24, 'view', 5),
(813, 24, 'index', 5),
(812, 21, 'delete', 5),
(811, 21, 'update', 5),
(810, 21, 'create', 5),
(809, 21, 'view', 5),
(808, 21, 'index', 5),
(807, 20, 'delete', 5),
(806, 20, 'update', 5),
(805, 20, 'create', 5),
(804, 20, 'view', 5),
(803, 20, 'index', 5),
(802, 19, 'view', 5),
(801, 19, 'index', 5),
(823, 25, 'update', 5),
(824, 25, 'delete', 5),
(825, 27, 'index', 5),
(826, 27, 'view', 5),
(827, 27, 'create', 5),
(828, 27, 'update', 5),
(829, 27, 'delete', 5),
(830, 27, 'approve', 5),
(831, 27, 'reject', 5),
(1204, 8, 'update', 6),
(1203, 8, 'create', 6),
(1202, 8, 'view', 6),
(1201, 8, 'index', 6),
(1568, 17, 'view', 2),
(1567, 17, 'index', 2),
(1566, 16, 'delete', 2),
(1565, 16, 'update', 2),
(1564, 16, 'create', 2),
(1563, 16, 'view', 2),
(1562, 16, 'index', 2),
(1233, 25, 'create', 6),
(1234, 25, 'update', 6),
(1235, 25, 'delete', 6),
(1236, 27, 'index', 6),
(1237, 27, 'view', 6),
(1238, 27, 'create', 6),
(1239, 27, 'update', 6),
(1240, 27, 'delete', 6),
(1300, 5, 'update', 1),
(1301, 5, 'delete', 1),
(1561, 15, 'delete', 2),
(1560, 15, 'update', 2),
(1559, 15, 'create', 2),
(1558, 15, 'view', 2),
(1557, 15, 'index', 2),
(1556, 14, 'delete', 2),
(1555, 14, 'update', 2),
(1554, 14, 'create', 2),
(1553, 14, 'view', 2),
(1552, 14, 'index', 2),
(1551, 13, 'delete', 2),
(1550, 13, 'update', 2),
(1549, 13, 'create', 2),
(1548, 13, 'view', 2),
(1547, 13, 'index', 2),
(1546, 12, 'delete', 2),
(1545, 12, 'update', 2),
(1544, 12, 'create', 2),
(1543, 12, 'view', 2),
(1542, 12, 'index', 2),
(1541, 11, 'delete', 2),
(1540, 11, 'update', 2),
(1539, 11, 'create', 2),
(1538, 11, 'view', 2),
(1537, 11, 'index', 2),
(1536, 9, 'delete', 2),
(1535, 9, 'update', 2),
(1534, 9, 'create', 2),
(1533, 9, 'view', 2),
(1532, 9, 'index', 2),
(1531, 8, 'delete', 2),
(1530, 8, 'update', 2),
(1529, 8, 'create', 2),
(1528, 8, 'view', 2),
(1527, 8, 'index', 2),
(1526, 7, 'delete', 2),
(1525, 7, 'update', 2),
(1524, 7, 'create', 2),
(1523, 7, 'view', 2),
(1522, 7, 'index', 2),
(1521, 6, 'delete', 2),
(1520, 6, 'update', 2),
(1519, 6, 'create', 2),
(1518, 6, 'view', 2),
(1517, 6, 'index', 2),
(1577, 19, 'index', 2),
(1578, 19, 'view', 2),
(1579, 19, 'create', 2),
(1580, 19, 'update', 2),
(1581, 19, 'delete', 2);

-- --------------------------------------------------------

--
-- Table structure for table `aktifitas`
--

CREATE TABLE IF NOT EXISTS `aktifitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `status_aktifitas` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status_aktifitas` (`status_aktifitas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `buku_tamu`
--

CREATE TABLE IF NOT EXISTS `buku_tamu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(200) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `perihal` text NOT NULL,
  `orang_tujuan` varchar(100) NOT NULL,
  `jam_kedatangan` varchar(100) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_report`
--

CREATE TABLE IF NOT EXISTS `campaign_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` text NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_pelajaran`
--

CREATE TABLE IF NOT EXISTS `jadwal_pelajaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mata_pelajaran` int(11) NOT NULL,
  `kelas` varchar(100) NOT NULL,
  `hari` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mata_pelajaran` (`mata_pelajaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_harian`
--

CREATE TABLE IF NOT EXISTS `jurnal_harian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pelajaran` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `attitut_siswa` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pelajaran` (`pelajaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `marketer_report`
--

CREATE TABLE IF NOT EXISTS `marketer_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(1000) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran`
--

CREATE TABLE IF NOT EXISTS `mata_pelajaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mata_pelajaran` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`id`, `mata_pelajaran`) VALUES
(1, 'grammar');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `parent` varchar(100) NOT NULL,
  `module` varchar(100) NOT NULL,
  `controller` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module` (`module`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu`, `icon`, `parent`, `module`, `controller`) VALUES
(1, 'Setting', 'fa fa-gear', '', '2', ''),
(2, 'Menu Backend', 'fa fa-align-justify', '1', '2', 'menu'),
(3, 'Role', 'fa fa-check-circle', '1', '2', 'role'),
(4, 'User', 'fa fa-users', '', '2', 'user'),
(5, 'Module Aplikasi', 'fa fa-asterisk', '', '2', 'module'),
(6, 'Data Siswa', 'fa fa-users', '', '6', 'siswa'),
(7, 'Pembayaran Siswa', 'fa fa-money', '', '7', 'pembayaran-siswa'),
(8, 'Pegawai', 'fa fa-user', '', '4', 'pegawai'),
(9, 'Buku Tamu', 'fa fa-book', '', '8', 'buku-tamu'),
(10, 'Kearsipan', 'fa fa-folder-open', '', '8', ''),
(11, 'Surat', 'fa fa-mail-forward', '10', '8', 'surat'),
(12, 'Campaign Report', 'fa fa-sticky-note', '', '3', 'campaign-report'),
(13, 'Marketer Report', 'fa fa-bookmark-o', '', '3', 'marketer-report'),
(14, 'Aktifitas', 'fa fa-briefcase', '', '3', 'aktifitas'),
(15, 'Jadwal Pelajaran', 'fa fa-calendar', '', '5', 'jadwal-pelajaran'),
(16, 'Jurnal Harian', 'fa fa-book', '', '5', 'jurnal-harian'),
(17, 'Mata Pelajaran', 'fa fa-flag-checkered', '', '5', 'mata-pelajaran'),
(18, 'Silabus ', 'fa fa-align-justify', '', '5', 'silabus');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1479231545),
('m140506_102106_rbac_init', 1479231864);

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE IF NOT EXISTS `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `module`) VALUES
(1, 'frontend'),
(2, 'backend'),
(3, 'marketing'),
(4, 'HRD'),
(5, 'akademik'),
(6, 'Kesiswaan'),
(7, 'Keuangan'),
(8, 'administrasi');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(100) NOT NULL,
  `no_pegawai` varchar(100) NOT NULL,
  `tanggal_lahir` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_siswa`
--

CREATE TABLE IF NOT EXISTS `pembayaran_siswa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siswa` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `siswa` (`siswa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pembayaran_siswa`
--

INSERT INTO `pembayaran_siswa` (`id`, `siswa`, `keterangan`, `tanggal`, `jumlah`) VALUES
(1, 1, 'ini bayar', '21-Mar-2017', 100000);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'developer'),
(2, 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `role_menu`
--

CREATE TABLE IF NOT EXISTS `role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` int(11) NOT NULL,
  `menu` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role` (`role`),
  KEY `menu` (`menu`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `role_menu`
--

INSERT INTO `role_menu` (`id`, `role`, `menu`) VALUES
(14, 1, 4),
(13, 1, 3),
(12, 1, 2),
(11, 1, 1),
(15, 1, 5),
(79, 2, 18),
(78, 2, 17),
(77, 2, 16),
(76, 2, 15),
(75, 2, 14),
(74, 2, 13),
(73, 2, 12),
(72, 2, 11),
(71, 2, 10),
(70, 2, 9),
(69, 2, 8),
(68, 2, 7),
(67, 2, 6),
(80, 2, 19);

-- --------------------------------------------------------

--
-- Table structure for table `silabus`
--

CREATE TABLE IF NOT EXISTS `silabus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mata_pelajaran` int(11) NOT NULL,
  `standar_kompetensi` text NOT NULL,
  `kompetensi_dasar` text NOT NULL,
  `hasil_belajar` text NOT NULL,
  `indokator_hasil_belajar` text NOT NULL,
  `materi_pokok` text NOT NULL,
  `alokasi_waktu` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mata_pelajaran` (`mata_pelajaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE IF NOT EXISTS `siswa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_siswa` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `tanggal_lahir` varchar(100) NOT NULL,
  `status_keaktifan` int(11) NOT NULL,
  `status_kursus` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status_keaktifan` (`status_keaktifan`,`status_kursus`),
  KEY `status_kursus` (`status_kursus`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `id_siswa`, `nama_lengkap`, `alamat`, `tanggal_lahir`, `status_keaktifan`, `status_kursus`) VALUES
(1, '2103', 'Agus Prasetiyo', 'Mojokerto', '28-Mar-2017', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `status_aktifitas`
--

CREATE TABLE IF NOT EXISTS `status_aktifitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `status_keaktifan`
--

CREATE TABLE IF NOT EXISTS `status_keaktifan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `status_keaktifan`
--

INSERT INTO `status_keaktifan` (`id`, `status`) VALUES
(1, 'Aktif'),
(2, 'Tidak Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `status_kursus`
--

CREATE TABLE IF NOT EXISTS `status_kursus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_kursus` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `status_kursus`
--

INSERT INTO `status_kursus` (`id`, `status_kursus`) VALUES
(1, 'Reguler'),
(2, 'Privat');

-- --------------------------------------------------------

--
-- Table structure for table `surat`
--

CREATE TABLE IF NOT EXISTS `surat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_surat` varchar(100) NOT NULL,
  `perihal` text NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `status_surat` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `surat`
--

INSERT INTO `surat` (`id`, `no_surat`, `perihal`, `tanggal`, `status_surat`) VALUES
(1, 'SR001/AA', 'Undangan keluarga', '10-feb-2017', 'surat keluar');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`),
  KEY `role` (`role`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `role`) VALUES
(1, 'developer', 'hX2iXXGf-6-jNoAidotAxlhUBZxhgNdS', '$2y$13$0N.Ruj8ZRE8GifJVTvAhyefdXX2qHMYhhlGih1O5Avy/z5WJGugI.', NULL, 'developer@mail.com', 10, 1477197448, 1479234102, 1),
(2, 'admin', '1RnIfzw1x4lVoRBWnVyxKex7C2Yzz53j', '$2y$13$2Sv7RcTDAiEfhqtgkZvRA.4Am./iM3Pdt7djQsxMsklGSvwpxwm0u', NULL, 'admin@siakad.com', 10, 1480135793, 1480135793, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aktifitas`
--
ALTER TABLE `aktifitas`
  ADD CONSTRAINT `aktifitas_ibfk_1` FOREIGN KEY (`status_aktifitas`) REFERENCES `status_aktifitas` (`id`);

--
-- Constraints for table `jadwal_pelajaran`
--
ALTER TABLE `jadwal_pelajaran`
  ADD CONSTRAINT `jadwal_pelajaran_ibfk_1` FOREIGN KEY (`mata_pelajaran`) REFERENCES `mata_pelajaran` (`id`);

--
-- Constraints for table `pembayaran_siswa`
--
ALTER TABLE `pembayaran_siswa`
  ADD CONSTRAINT `pembayaran_siswa_ibfk_1` FOREIGN KEY (`siswa`) REFERENCES `siswa` (`id`);

--
-- Constraints for table `silabus`
--
ALTER TABLE `silabus`
  ADD CONSTRAINT `silabus_ibfk_1` FOREIGN KEY (`mata_pelajaran`) REFERENCES `mata_pelajaran` (`id`);

--
-- Constraints for table `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`status_keaktifan`) REFERENCES `status_keaktifan` (`id`),
  ADD CONSTRAINT `siswa_ibfk_2` FOREIGN KEY (`status_kursus`) REFERENCES `status_kursus` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

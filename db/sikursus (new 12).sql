-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 11, 2017 at 06:56 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikursus`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi_siswa`
--

CREATE TABLE `absensi_siswa` (
  `id` int(11) NOT NULL,
  `program` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `pertemuan_ke` int(11) NOT NULL,
  `bulan` varchar(100) NOT NULL,
  `tahun` int(11) NOT NULL,
  `tgl` varchar(2) NOT NULL,
  `keterangan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi_siswa`
--

INSERT INTO `absensi_siswa` (`id`, `program`, `siswa`, `tanggal`, `pertemuan_ke`, `bulan`, `tahun`, `tgl`, `keterangan`) VALUES
(1, 1, 7, '03-May-2017', 1, 'mei', 2017, '1', 'H'),
(2, 1, 8, '03-May-2017', 1, 'mei', 2017, '1', 'H');

-- --------------------------------------------------------

--
-- Table structure for table `absensi_tutor`
--

CREATE TABLE `absensi_tutor` (
  `id` int(11) NOT NULL,
  `tutor` int(11) NOT NULL,
  `kelas` int(11) NOT NULL,
  `waktu` varchar(100) NOT NULL,
  `tanggal` int(11) NOT NULL,
  `bulan` varchar(100) NOT NULL,
  `tahun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi_tutor`
--

INSERT INTO `absensi_tutor` (`id`, `tutor`, `kelas`, `waktu`, `tanggal`, `bulan`, `tahun`) VALUES
(1, 1, 1, '08:00 - 09.00', 3, 'mei', 2017);

-- --------------------------------------------------------

--
-- Table structure for table `action`
--

CREATE TABLE `action` (
  `id` int(11) NOT NULL,
  `menu` int(11) NOT NULL,
  `action` varchar(100) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `action`
--

INSERT INTO `action` (`id`, `menu`, `action`, `role`) VALUES
(1299, 5, 'create', 1),
(1298, 5, 'view', 1),
(1297, 5, 'index', 1),
(1296, 4, 'registeruser', 1),
(1295, 4, 'delete', 1),
(1294, 4, 'update', 1),
(1293, 4, 'create', 1),
(1292, 4, 'view', 1),
(1291, 4, 'index', 1),
(1290, 3, 'setting', 1),
(1289, 3, 'delete', 1),
(1288, 3, 'update', 1),
(1287, 3, 'create', 1),
(1286, 3, 'view', 1),
(1285, 3, 'index', 1),
(1284, 2, 'delete', 1),
(1283, 2, 'update', 1),
(1282, 2, 'create', 1),
(1281, 2, 'view', 1),
(1280, 2, 'index', 1),
(4055, 60, 'delete', 2),
(4249, 6, 'update', 8),
(4248, 6, 'create', 8),
(4247, 6, 'view', 8),
(4246, 6, 'baru', 8),
(4245, 6, 'index', 8),
(4244, 11, 'delete', 7),
(4243, 11, 'update', 7),
(4242, 11, 'create', 7),
(4241, 11, 'view', 7),
(4240, 11, 'index', 7),
(4239, 9, 'delete', 7),
(4238, 9, 'update', 7),
(4237, 9, 'create', 7),
(4236, 9, 'view', 7),
(4235, 9, 'index', 7),
(4234, 21, 'delete', 6),
(4233, 21, 'update', 6),
(4232, 21, 'create', 6),
(4231, 21, 'view', 6),
(4230, 21, 'index', 6),
(4229, 18, 'delete', 6),
(4228, 18, 'update', 6),
(4227, 18, 'create', 6),
(4226, 18, 'view', 6),
(4225, 18, 'index', 6),
(4224, 17, 'delete', 6),
(4223, 17, 'update', 6),
(4222, 17, 'create', 6),
(4054, 60, 'update', 2),
(4053, 60, 'create', 2),
(4052, 60, 'view', 2),
(4204, 50, 'delete', 5),
(4203, 50, 'update', 5),
(4202, 50, 'create', 5),
(4201, 50, 'view', 5),
(4200, 50, 'index', 5),
(4051, 60, 'index', 2),
(4050, 59, 'delete', 2),
(4049, 59, 'update', 2),
(4048, 59, 'create', 2),
(4199, 49, 'delete', 5),
(4198, 49, 'update', 5),
(4197, 49, 'create', 5),
(4196, 49, 'view', 5),
(4195, 49, 'index', 5),
(4194, 47, 'delete', 5),
(4193, 47, 'update', 5),
(4192, 47, 'create', 5),
(4191, 47, 'view', 5),
(4190, 47, 'index', 5),
(4189, 46, 'delete', 5),
(4188, 46, 'update', 5),
(4187, 46, 'create', 5),
(4186, 46, 'view', 5),
(4185, 46, 'index', 5),
(4184, 20, 'getgaji', 5),
(4183, 20, 'delete', 5),
(4182, 20, 'update', 5),
(4181, 20, 'create', 5),
(4180, 20, 'view', 5),
(4179, 20, 'index', 5),
(4178, 8, 'delete', 5),
(4177, 8, 'update', 5),
(4176, 8, 'create', 5),
(4175, 8, 'view', 5),
(4174, 8, 'index', 5),
(4221, 17, 'view', 6),
(4220, 17, 'index', 6),
(4219, 16, 'delete', 6),
(4218, 16, 'update', 6),
(4047, 59, 'view', 2),
(4046, 59, 'index', 2),
(4045, 58, 'delete', 2),
(4044, 58, 'update', 2),
(4043, 58, 'create', 2),
(4042, 58, 'view', 2),
(4041, 58, 'index', 2),
(4217, 16, 'create', 6),
(4216, 16, 'view', 6),
(4215, 16, 'index', 6),
(4214, 15, 'delete', 6),
(4213, 15, 'update', 6),
(4212, 15, 'create', 6),
(4211, 15, 'view', 6),
(4210, 15, 'index', 6),
(1300, 5, 'update', 1),
(1301, 5, 'delete', 1),
(4040, 57, 'delete', 2),
(4039, 57, 'update', 2),
(4038, 57, 'create', 2),
(4037, 57, 'view', 2),
(4036, 57, 'index', 2),
(4035, 56, 'delete', 2),
(4034, 56, 'update', 2),
(4033, 56, 'create', 2),
(4032, 56, 'view', 2),
(4031, 56, 'index', 2),
(4030, 55, 'delete', 2),
(4029, 55, 'update', 2),
(4028, 55, 'create', 2),
(4027, 55, 'view', 2),
(4026, 55, 'index', 2),
(4025, 54, 'delete', 2),
(4024, 54, 'update', 2),
(4023, 54, 'create', 2),
(4022, 54, 'view', 2),
(4021, 54, 'index', 2),
(4020, 53, 'delete', 2),
(4019, 53, 'update', 2),
(4018, 53, 'create', 2),
(4017, 53, 'view', 2),
(4016, 53, 'index', 2),
(4015, 51, 'delete', 2),
(4014, 51, 'update', 2),
(4013, 51, 'create', 2),
(4012, 51, 'view', 2),
(4011, 51, 'index', 2),
(4010, 50, 'delete', 2),
(4009, 50, 'update', 2),
(4008, 50, 'create', 2),
(4007, 50, 'view', 2),
(4006, 50, 'index', 2),
(4005, 49, 'delete', 2),
(4004, 49, 'update', 2),
(4003, 49, 'create', 2),
(4002, 49, 'view', 2),
(4001, 49, 'index', 2),
(4000, 47, 'delete', 2),
(3999, 47, 'update', 2),
(3998, 47, 'create', 2),
(3997, 47, 'view', 2),
(3996, 47, 'index', 2),
(3995, 46, 'delete', 2),
(3994, 46, 'update', 2),
(3993, 46, 'create', 2),
(3992, 46, 'view', 2),
(3991, 46, 'index', 2),
(3990, 45, 'delete', 2),
(3989, 45, 'update', 2),
(3988, 45, 'create', 2),
(3987, 45, 'view', 2),
(3986, 45, 'index', 2),
(3985, 44, 'delete', 2),
(3984, 44, 'update', 2),
(3983, 44, 'create', 2),
(3982, 44, 'view', 2),
(3981, 44, 'index', 2),
(3980, 43, 'index', 2),
(3979, 42, 'delete', 2),
(3978, 42, 'update', 2),
(3977, 42, 'create', 2),
(3976, 42, 'view', 2),
(3975, 42, 'index', 2),
(3974, 41, 'delete', 2),
(3973, 41, 'update', 2),
(3972, 41, 'create', 2),
(3971, 41, 'view', 2),
(3970, 41, 'index', 2),
(3969, 40, 'delete', 2),
(3968, 40, 'update', 2),
(3967, 40, 'create', 2),
(3966, 40, 'view', 2),
(3965, 40, 'index', 2),
(3964, 39, 'delete', 2),
(3963, 39, 'update', 2),
(3962, 39, 'create', 2),
(3961, 39, 'view', 2),
(3960, 39, 'index', 2),
(3959, 38, 'delete', 2),
(3958, 38, 'update', 2),
(3957, 38, 'create', 2),
(3956, 38, 'view', 2),
(3955, 38, 'index', 2),
(3954, 37, 'delete', 2),
(3953, 37, 'update', 2),
(3952, 37, 'create', 2),
(3951, 37, 'view', 2),
(3950, 37, 'index', 2),
(3949, 35, 'delete', 2),
(3948, 35, 'update', 2),
(3947, 35, 'create', 2),
(3946, 35, 'view', 2),
(3945, 35, 'index', 2),
(3944, 34, 'delete', 2),
(3943, 34, 'update', 2),
(3942, 34, 'create', 2),
(3941, 34, 'view', 2),
(3940, 34, 'index', 2),
(3939, 33, 'delete', 2),
(3938, 33, 'update', 2),
(3937, 33, 'create', 2),
(3936, 33, 'view', 2),
(3935, 33, 'index', 2),
(3934, 32, 'index', 2),
(3933, 30, 'delete', 2),
(3932, 30, 'update', 2),
(3931, 30, 'create', 2),
(3930, 30, 'view', 2),
(3929, 30, 'index', 2),
(3928, 29, 'delete', 2),
(3927, 29, 'update', 2),
(3926, 29, 'create', 2),
(3925, 29, 'view', 2),
(3924, 29, 'index', 2),
(3923, 28, 'delete', 2),
(3922, 28, 'update', 2),
(3921, 28, 'create', 2),
(3920, 28, 'view', 2),
(3919, 28, 'index', 2),
(3918, 31, 'delete', 2),
(3917, 31, 'update', 2),
(3916, 31, 'create', 2),
(3915, 31, 'view', 2),
(3914, 31, 'index', 2),
(3913, 25, 'delete', 2),
(3912, 25, 'update', 2),
(3911, 25, 'create', 2),
(3910, 25, 'view', 2),
(3909, 25, 'index', 2),
(3908, 23, 'delete', 2),
(3907, 23, 'update', 2),
(3906, 23, 'create', 2),
(3905, 23, 'view', 2),
(3904, 23, 'index', 2),
(3903, 22, 'delete', 2),
(3902, 22, 'update', 2),
(3901, 22, 'create', 2),
(3900, 22, 'view', 2),
(3899, 22, 'index', 2),
(3898, 21, 'delete', 2),
(3897, 21, 'update', 2),
(3896, 21, 'create', 2),
(3895, 21, 'view', 2),
(3894, 21, 'index', 2),
(3893, 20, 'delete', 2),
(3892, 20, 'update', 2),
(3891, 20, 'create', 2),
(3890, 20, 'view', 2),
(3889, 20, 'index', 2),
(3888, 18, 'delete', 2),
(3887, 18, 'update', 2),
(3886, 18, 'create', 2),
(3885, 18, 'view', 2),
(3884, 18, 'index', 2),
(3883, 17, 'delete', 2),
(3882, 17, 'update', 2),
(3881, 17, 'create', 2),
(3880, 17, 'view', 2),
(3879, 17, 'index', 2),
(3878, 16, 'delete', 2),
(3877, 16, 'update', 2),
(3876, 16, 'create', 2),
(3875, 16, 'view', 2),
(3874, 16, 'index', 2),
(3873, 15, 'delete', 2),
(3872, 15, 'update', 2),
(3871, 15, 'create', 2),
(3870, 15, 'view', 2),
(3869, 15, 'index', 2),
(3868, 14, 'delete', 2),
(3867, 14, 'update', 2),
(3866, 14, 'create', 2),
(3865, 14, 'view', 2),
(3864, 14, 'index', 2),
(3863, 13, 'delete', 2),
(3862, 13, 'update', 2),
(3861, 13, 'create', 2),
(3860, 13, 'view', 2),
(3859, 13, 'index', 2),
(3858, 12, 'delete', 2),
(3857, 12, 'update', 2),
(3856, 12, 'create', 2),
(3855, 12, 'view', 2),
(3854, 12, 'index', 2),
(3853, 11, 'delete', 2),
(3852, 11, 'update', 2),
(3851, 11, 'create', 2),
(3850, 11, 'view', 2),
(3849, 11, 'index', 2),
(3848, 9, 'delete', 2),
(3847, 9, 'update', 2),
(3846, 9, 'create', 2),
(3845, 9, 'view', 2),
(3844, 9, 'index', 2),
(3843, 8, 'delete', 2),
(3842, 8, 'update', 2),
(3841, 8, 'create', 2),
(3840, 8, 'view', 2),
(3839, 8, 'index', 2),
(3838, 7, 'delete', 2),
(3837, 7, 'update', 2),
(3836, 7, 'create', 2),
(3835, 7, 'view', 2),
(3834, 7, 'index', 2),
(3833, 6, 'delete', 2),
(3832, 6, 'update', 2),
(3831, 6, 'create', 2),
(3830, 6, 'view', 2),
(3829, 6, 'index', 2),
(4056, 61, 'index', 2),
(4057, 61, 'view', 2),
(4058, 61, 'create', 2),
(4059, 61, 'update', 2),
(4060, 61, 'delete', 2),
(4061, 62, 'index', 2),
(4062, 62, 'view', 2),
(4063, 62, 'create', 2),
(4064, 62, 'update', 2),
(4065, 62, 'delete', 2),
(4066, 7, 'index', 3),
(4067, 7, 'view', 3),
(4068, 7, 'create', 3),
(4069, 7, 'update', 3),
(4070, 7, 'delete', 3),
(4071, 7, 'getangsuran', 3),
(4072, 31, 'index', 3),
(4073, 31, 'view', 3),
(4074, 31, 'create', 3),
(4075, 31, 'update', 3),
(4076, 31, 'delete', 3),
(4077, 32, 'index', 3),
(4078, 32, 'getchartdata', 3),
(4079, 32, 'getchartdatamonth', 3),
(4080, 44, 'index', 3),
(4081, 44, 'view', 3),
(4082, 44, 'create', 3),
(4083, 44, 'update', 3),
(4084, 44, 'delete', 3),
(4085, 45, 'index', 3),
(4086, 45, 'view', 3),
(4087, 45, 'create', 3),
(4088, 45, 'update', 3),
(4089, 45, 'delete', 3),
(4090, 53, 'index', 3),
(4091, 53, 'tablekas', 3),
(4092, 53, 'view', 3),
(4093, 53, 'create', 3),
(4094, 53, 'update', 3),
(4095, 53, 'delete', 3),
(4096, 54, 'index', 3),
(4097, 54, 'tablejurnal', 3),
(4098, 54, 'view', 3),
(4099, 54, 'create', 3),
(4100, 54, 'update', 3),
(4101, 54, 'delete', 3),
(4102, 55, 'index', 3),
(4103, 55, 'list', 3),
(4104, 55, 'view', 3),
(4105, 55, 'create', 3),
(4106, 55, 'update', 3),
(4107, 55, 'delete', 3),
(4108, 56, 'index', 3),
(4109, 56, 'view', 3),
(4110, 56, 'create', 3),
(4111, 56, 'update', 3),
(4112, 56, 'delete', 3),
(4113, 57, 'index', 3),
(4114, 57, 'view', 3),
(4115, 57, 'create', 3),
(4116, 57, 'update', 3),
(4117, 57, 'delete', 3),
(4118, 58, 'index', 3),
(4119, 58, 'tabelmutasi', 3),
(4120, 58, 'view', 3),
(4121, 58, 'create', 3),
(4122, 58, 'update', 3),
(4123, 58, 'delete', 3),
(4124, 59, 'index', 3),
(4125, 59, 'view', 3),
(4126, 59, 'create', 3),
(4127, 59, 'update', 3),
(4128, 59, 'delete', 3),
(4129, 60, 'index', 3),
(4130, 60, 'view', 3),
(4131, 60, 'create', 3),
(4132, 60, 'update', 3),
(4133, 60, 'delete', 3),
(4134, 61, 'index', 3),
(4135, 61, 'view', 3),
(4136, 61, 'create', 3),
(4137, 61, 'update', 3),
(4138, 61, 'delete', 3),
(4139, 62, 'index', 3),
(4140, 62, 'view', 3),
(4141, 62, 'create', 3),
(4142, 62, 'update', 3),
(4143, 62, 'delete', 3),
(4144, 12, 'index', 4),
(4145, 12, 'view', 4),
(4146, 12, 'create', 4),
(4147, 12, 'update', 4),
(4148, 12, 'delete', 4),
(4149, 13, 'index', 4),
(4150, 13, 'view', 4),
(4151, 13, 'create', 4),
(4152, 13, 'update', 4),
(4153, 13, 'delete', 4),
(4154, 14, 'index', 4),
(4155, 14, 'view', 4),
(4156, 14, 'create', 4),
(4157, 14, 'update', 4),
(4158, 14, 'delete', 4),
(4159, 28, 'index', 4),
(4160, 28, 'view', 4),
(4161, 28, 'create', 4),
(4162, 28, 'update', 4),
(4163, 28, 'delete', 4),
(4164, 29, 'index', 4),
(4165, 29, 'view', 4),
(4166, 29, 'create', 4),
(4167, 29, 'update', 4),
(4168, 29, 'delete', 4),
(4169, 30, 'index', 4),
(4170, 30, 'view', 4),
(4171, 30, 'create', 4),
(4172, 30, 'update', 4),
(4173, 30, 'delete', 4),
(4205, 51, 'index', 5),
(4206, 51, 'view', 5),
(4207, 51, 'create', 5),
(4208, 51, 'update', 5),
(4209, 51, 'delete', 5),
(4250, 6, 'delete', 8),
(4251, 22, 'index', 8),
(4252, 22, 'lihat', 8),
(4253, 22, 'view', 8),
(4254, 22, 'create', 8),
(4255, 22, 'update', 8),
(4256, 22, 'delete', 8),
(4257, 23, 'index', 8),
(4258, 23, 'view', 8),
(4259, 23, 'create', 8),
(4260, 23, 'update', 8),
(4261, 23, 'delete', 8),
(4262, 25, 'index', 8),
(4263, 25, 'view', 8),
(4264, 25, 'create', 8),
(4265, 25, 'update', 8),
(4266, 25, 'delete', 8);

-- --------------------------------------------------------

--
-- Table structure for table `aktifitas`
--

CREATE TABLE `aktifitas` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `status_aktifitas` int(11) NOT NULL,
  `tanggal_mulai` varchar(100) NOT NULL,
  `tanggal_selesai` varchar(100) NOT NULL,
  `waktu_mulai` varchar(100) NOT NULL,
  `waktu_berakhir` varchar(100) NOT NULL,
  `pic` varchar(100) NOT NULL,
  `profil_market` varchar(100) NOT NULL,
  `target` int(11) NOT NULL,
  `kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aktifitas`
--

INSERT INTO `aktifitas` (`id`, `judul`, `keterangan`, `status_aktifitas`, `tanggal_mulai`, `tanggal_selesai`, `waktu_mulai`, `waktu_berakhir`, `pic`, `profil_market`, `target`, `kategori`) VALUES
(4, 'Visit SDN 5 Lamongan', 'Buka Booth Pameran, Sebar Brosur', 1, '21-May-2017', '23-May-2017', '08:00 AM', '04:00 PM', 'Latief', 'Siswa SD kelas 1-4', 75, 'Visit');

-- --------------------------------------------------------

--
-- Table structure for table `aktifitas_tim`
--

CREATE TABLE `aktifitas_tim` (
  `id` int(11) NOT NULL,
  `tim` int(11) NOT NULL,
  `aktifitas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aktifitas_tim`
--

INSERT INTO `aktifitas_tim` (`id`, `tim`, `aktifitas`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id` int(11) NOT NULL,
  `nomor_akun` int(11) NOT NULL,
  `akun` varchar(100) NOT NULL,
  `status_akun` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id`, `nomor_akun`, `akun`, `status_akun`) VALUES
(1, 1100, 'KAS', 'debet');

-- --------------------------------------------------------

--
-- Table structure for table `biaya_program`
--

CREATE TABLE `biaya_program` (
  `id` int(11) NOT NULL,
  `program` int(11) NOT NULL,
  `jumlah_siswa` int(11) NOT NULL,
  `durasi` varchar(100) NOT NULL,
  `jenis_kursus` varchar(100) NOT NULL,
  `banyak_angsuran` int(11) NOT NULL,
  `jumlah_biaya` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biaya_program`
--

INSERT INTO `biaya_program` (`id`, `program`, `jumlah_siswa`, `durasi`, `jenis_kursus`, `banyak_angsuran`, `jumlah_biaya`) VALUES
(1, 1, 10, '1 tahun', 'Reguler', 6, 2600000);

-- --------------------------------------------------------

--
-- Table structure for table `biaya_program_siswa`
--

CREATE TABLE `biaya_program_siswa` (
  `id` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `biaya_program` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biaya_program_siswa`
--

INSERT INTO `biaya_program_siswa` (`id`, `siswa`, `biaya_program`) VALUES
(1, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `buku_besar`
--

CREATE TABLE `buku_besar` (
  `id` int(11) NOT NULL,
  `nama_rek` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `debet` int(11) NOT NULL,
  `kredit` int(11) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `ref` varchar(100) NOT NULL,
  `nomor_rek` int(11) NOT NULL,
  `saldo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `buku_kas`
--

CREATE TABLE `buku_kas` (
  `id` int(11) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `saldo` int(11) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buku_kas`
--

INSERT INTO `buku_kas` (`id`, `tanggal`, `keterangan`, `saldo`, `status`) VALUES
(1, '11-May-2017', 'Materai 2 @6000', 12000, 'kredit');

-- --------------------------------------------------------

--
-- Table structure for table `buku_tamu`
--

CREATE TABLE `buku_tamu` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(200) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `perihal` text NOT NULL,
  `orang_tujuan` varchar(100) NOT NULL,
  `jam_kedatangan` varchar(100) NOT NULL,
  `tanggal` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_report`
--

CREATE TABLE `campaign_report` (
  `id` int(11) NOT NULL,
  `aktifitas` int(11) NOT NULL,
  `evaluasi` text NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `total_registran` int(11) NOT NULL,
  `total_revenue` int(11) NOT NULL,
  `roas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campaign_report`
--

INSERT INTO `campaign_report` (`id`, `aktifitas`, `evaluasi`, `tanggal`, `total_registran`, `total_revenue`, `roas`) VALUES
(2, 4, 'Pelaksanaan kurang kondusif, listrik tidak ada jadi tidak bisa preview', '05-Oct-2017', 23, 27600000, 11);

-- --------------------------------------------------------

--
-- Table structure for table `daftar_akun`
--

CREATE TABLE `daftar_akun` (
  `id` int(11) NOT NULL,
  `nomor_akun` int(11) NOT NULL,
  `saldo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daftar_akun`
--

INSERT INTO `daftar_akun` (`id`, `nomor_akun`, `saldo`) VALUES
(1, 1, 415767116);

-- --------------------------------------------------------

--
-- Table structure for table `daily_tes`
--

CREATE TABLE `daily_tes` (
  `id` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `pelajaran` int(11) NOT NULL,
  `nilai_1` int(11) NOT NULL,
  `nilai_2` int(11) NOT NULL,
  `nilai_3` int(11) NOT NULL,
  `nilai_4` int(11) NOT NULL,
  `nilai_5` int(11) NOT NULL,
  `nilai_6` int(11) NOT NULL,
  `nilai_7` int(11) NOT NULL,
  `nilai_8` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_tes`
--

INSERT INTO `daily_tes` (`id`, `siswa`, `pelajaran`, `nilai_1`, `nilai_2`, `nilai_3`, `nilai_4`, `nilai_5`, `nilai_6`, `nilai_7`, `nilai_8`) VALUES
(1, 7, 1, 80, 80, 80, 80, 80, 80, 80, 80);

-- --------------------------------------------------------

--
-- Table structure for table `data_kelas`
--

CREATE TABLE `data_kelas` (
  `id` int(11) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL,
  `program` int(11) NOT NULL,
  `tingkat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_kelas`
--

INSERT INTO `data_kelas` (`id`, `nama_kelas`, `program`, `tingkat`) VALUES
(1, 'Kelas Mangga', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_tim_maketing`
--

CREATE TABLE `data_tim_maketing` (
  `id` int(11) NOT NULL,
  `tim` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_tim_marketing`
--

CREATE TABLE `data_tim_marketing` (
  `id` int(11) NOT NULL,
  `tim` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_tim_marketing`
--

INSERT INTO `data_tim_marketing` (`id`, `tim`, `pegawai`) VALUES
(1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `final_tes`
--

CREATE TABLE `final_tes` (
  `id` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `pelajaran` int(11) NOT NULL,
  `nilai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `final_tes`
--

INSERT INTO `final_tes` (`id`, `siswa`, `pelajaran`, `nilai`) VALUES
(1, 7, 1, 80);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_pelajaran`
--

CREATE TABLE `jadwal_pelajaran` (
  `id` int(11) NOT NULL,
  `mata_pelajaran` int(11) NOT NULL,
  `kelas` varchar(100) NOT NULL,
  `hari` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_kelamin`
--

CREATE TABLE `jenis_kelamin` (
  `id` int(11) NOT NULL,
  `jenis_kelamin` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_kelamin`
--

INSERT INTO `jenis_kelamin` (`id`, `jenis_kelamin`) VALUES
(1, 'Laki-Laki'),
(2, 'Perempuan');

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_harian`
--

CREATE TABLE `jurnal_harian` (
  `id` int(11) NOT NULL,
  `nama_pengajar` varchar(100) NOT NULL,
  `pukul` varchar(100) NOT NULL,
  `program` varchar(100) NOT NULL,
  `level` int(11) NOT NULL,
  `pertemuan_ke` int(11) NOT NULL,
  `jumlah_siswa` int(11) NOT NULL,
  `jumlah_hadir` int(11) NOT NULL,
  `jumlah_absen` int(11) NOT NULL,
  `materi` text NOT NULL,
  `uraian` text NOT NULL,
  `media` text NOT NULL,
  `tambahan` text NOT NULL,
  `pembukaan` text NOT NULL,
  `inti` text NOT NULL,
  `penutupan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurnal_harian`
--

INSERT INTO `jurnal_harian` (`id`, `nama_pengajar`, `pukul`, `program`, `level`, `pertemuan_ke`, `jumlah_siswa`, `jumlah_hadir`, `jumlah_absen`, `materi`, `uraian`, `media`, `tambahan`, `pembukaan`, `inti`, `penutupan`) VALUES
(1, 'irfan', '9.00 - 10.00', 'EFC', 2, 1, 6, 6, 0, 'Writing and tenses', 'Writing and tenses', 'Laptop dan monitor', '-', 'Mempelajari Writing dan tenses pada bahasa inggris', 'Mengetahui semua jenis Tenses  dalam bahasa inggris', 'Siswa dapat menuliskan tenses dengan benar');

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_penyesuaian`
--

CREATE TABLE `jurnal_penyesuaian` (
  `id` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `nomor_akun` int(11) NOT NULL,
  `saldo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurnal_penyesuaian`
--

INSERT INTO `jurnal_penyesuaian` (`id`, `keterangan`, `tanggal`, `nomor_akun`, `saldo`) VALUES
(1, 'Biaya Perlengkapam', '100', 1, 250000);

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_umum`
--

CREATE TABLE `jurnal_umum` (
  `id` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `nomor_akun` int(11) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `status_akun` varchar(100) NOT NULL,
  `saldo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurnal_umum`
--

INSERT INTO `jurnal_umum` (`id`, `keterangan`, `nomor_akun`, `tanggal`, `status_akun`, `saldo`) VALUES
(1, 'Materai 2 @6000 ', 1, '10-May-2017', 'kredit', 12000);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_aktifitas`
--

CREATE TABLE `kategori_aktifitas` (
  `id` int(11) NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `target` int(11) NOT NULL,
  `bobot` int(11) NOT NULL,
  `target_tahun` int(11) NOT NULL,
  `target_quartal` int(11) NOT NULL,
  `target_bulan` int(11) NOT NULL,
  `bobot_akhir` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_aktifitas`
--

INSERT INTO `kategori_aktifitas` (`id`, `kategori`, `target`, `bobot`, `target_tahun`, `target_quartal`, `target_bulan`, `bobot_akhir`) VALUES
(1, 'Visit', 49, 15, 960, 240, 80, 15),
(2, 'Goal Visit', 1, 30, 24, 6, 2, 30),
(3, 'New Customer', 15, 50, 360, 90, 30, 45),
(4, 'Awareness BTL', 4, 5, 48, 12, 4, 5),
(5, 'Awareness ATL', 52, 100, 0, 0, 52, 5);

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `kelas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id`, `siswa`, `kelas`) VALUES
(1, 7, 1),
(2, 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `laporan_harian_siswa`
--

CREATE TABLE `laporan_harian_siswa` (
  `id` int(11) NOT NULL,
  `jurnal` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `catatan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan_harian_siswa`
--

INSERT INTO `laporan_harian_siswa` (`id`, `jurnal`, `siswa`, `catatan`) VALUES
(1, 1, 1, 'good attitude');

-- --------------------------------------------------------

--
-- Table structure for table `laporan_laba_rugi`
--

CREATE TABLE `laporan_laba_rugi` (
  `id` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `laporan_perubahan_ekuitas`
--

CREATE TABLE `laporan_perubahan_ekuitas` (
  `id` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `marketer_report`
--

CREATE TABLE `marketer_report` (
  `id` int(11) NOT NULL,
  `judul` varchar(1000) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `marketing_tools`
--

CREATE TABLE `marketing_tools` (
  `id` int(11) NOT NULL,
  `aktifitas` int(11) NOT NULL,
  `tool` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marketing_tools`
--

INSERT INTO `marketing_tools` (`id`, `aktifitas`, `tool`, `jumlah`, `harga`) VALUES
(2, 4, 'Booth Pameran', 1, 2500000),
(3, 4, 'Brosur', 2, 1500000);

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran`
--

CREATE TABLE `mata_pelajaran` (
  `id` int(11) NOT NULL,
  `mata_pelajaran` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`id`, `mata_pelajaran`) VALUES
(1, 'grammar');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `parent` varchar(100) NOT NULL,
  `module` varchar(100) NOT NULL,
  `controller` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu`, `icon`, `parent`, `module`, `controller`) VALUES
(1, 'Setting', 'fa fa-gear', '', '2', ''),
(2, 'Menu Backend', 'fa fa-align-justify', '1', '2', 'menu'),
(3, 'Role', 'fa fa-check-circle', '1', '2', 'role'),
(4, 'User', 'fa fa-users', '', '2', 'user'),
(5, 'Module Aplikasi', 'fa fa-asterisk', '', '2', 'module'),
(6, 'Data Siswa', 'fa fa-users', '', '6', 'siswa'),
(7, 'Pembayaran Siswa', 'fa fa-money', '', '7', 'pembayaran-siswa'),
(8, 'Pegawai', 'fa fa-user', '', '4', 'pegawai'),
(9, 'Buku Tamu', 'fa fa-book', '', '8', 'buku-tamu'),
(10, 'Kearsipan', 'fa fa-folder-open', '', '8', ''),
(11, 'Surat', 'fa fa-mail-forward', '10', '8', 'surat'),
(12, 'Campaign Report', 'fa fa-sticky-note', '', '3', 'campaign-report'),
(13, 'Marketer Report', 'fa fa-bookmark-o', '', '3', 'marketer-report'),
(14, 'Aktifitas', 'fa fa-briefcase', '', '3', 'aktifitas'),
(15, 'Jadwal Pelajaran', 'fa fa-calendar', '', '5', 'jadwal-pelajaran'),
(16, 'Jurnal Harian', 'fa fa-book', '', '5', 'jurnal-harian'),
(17, 'Mata Pelajaran', 'fa fa-flag-checkered', '', '5', 'mata-pelajaran'),
(18, 'Silabus ', 'fa fa-align-justify', '', '5', 'silabus'),
(20, 'Penggajian', 'fa fa-money', '', '4', 'penggajian'),
(21, 'Raport Siswa', 'fa fa-file-text-o', '', '5', 'raport'),
(22, 'Absensi Siswa', 'fa fa-edit', '', '6', 'absensi-siswa'),
(23, 'Kelas', 'fa fa-check-square', '24', '6', 'kelas'),
(24, 'Kelas', 'fa fa-align-justify', '', '6', ''),
(25, 'Data-Kelas', 'fa fa-align-left', '24', '6', 'data-kelas'),
(26, 'Tim Marketing', 'fa fa-bullhorn', '', '3', ''),
(31, 'Pengeluaran', 'fa fa-sign-out', '', '7', 'pengeluaran'),
(28, 'Tim', 'fa fa-users', '26', '3', 'tim-marketing'),
(29, 'Data Tim', 'fa fa-user', '26', '3', 'data-tim-marketing'),
(30, 'Aktifitas Tim', 'fa fa-list-alt', '26', '3', 'aktifitas-tim'),
(32, 'Kas', 'fa fa-dollar', '', '7', 'kas'),
(33, 'Jurnal Siswa', 'fa fa-files-o', '', '9', 'jurnal-harian'),
(34, 'Laporan Harian', 'fa fa-edit', '', '9', 'laporan-harian-siswa'),
(35, 'RPP', 'fa fa-align-justify', '', '9', 'silabus'),
(36, 'Tes', 'fa fa-book', '', '9', ''),
(37, 'Pre Tes', 'fa fa-adjust', '36', '9', 'pretes'),
(38, 'Daily Tes', 'fa fa-adjust', '36', '9', 'daily-tes'),
(39, 'Speaking Tes', 'fa fa-adjust', '36', '9', 'speaking-tes'),
(40, 'Unit Tes', 'fa fa-adjust', '36', '9', 'unit-tes'),
(41, 'Mid Tes', 'fa fa-adjust', '36', '9', 'mid-tes'),
(42, 'Final Tes', 'fa fa-adjust', '36', '9', 'final-tes'),
(43, 'Tes Report', 'fa fa-file', '', '9', 'tes-report'),
(44, 'Biaya Program', 'fa fa-star-o', '', '7', 'biaya-program'),
(45, 'Biaya Program Siswa', 'fa fa-user', '', '7', 'biaya-program-siswa'),
(46, 'Tutor', 'fa fa-user', '', '4', 'tutor'),
(47, 'Absensi Tutor', 'fa fa-calendar-check-o', '', '4', 'absensi-tutor'),
(48, 'Supervisi', 'fa fa-file-text', '', '4', ''),
(49, 'Pengamatan Pembelajaran', 'fa fa-adjust', '48', '4', 'supervisi'),
(50, 'Perencanaan pembelajaran', 'fa fa-adjust', '48', '4', 'pengamatan-pembelajarang'),
(51, 'Observasi Kelas', 'fa fa-adjust', '48', '4', 'observasi-kelas'),
(52, 'Laporan Laporan', 'fa fa-paperclip', '', '7', ''),
(53, 'Buku Kas', 'fa fa-book', '52', '7', 'buku-kas'),
(54, 'Jurnal Penyesuaian', 'fa fa-bookmark', '52', '7', 'jurnal-penyesuaian'),
(55, 'Jurnal Umum', 'fa fa-bookmark-o', '52', '7', 'jurnal-umum'),
(56, 'Laporan Laba Rugi', 'fa fa-edit', '52', '7', 'laporan-laba-rugi'),
(57, 'LPE', 'fa fa-align-justify', '52', '7', 'laporan-perubahan-ekuitas'),
(58, 'Mutasi Kas Bulanan', 'fa fa-calendar', '52', '7', 'mutasi-kas-bulanan'),
(59, 'Neraca', 'fa fa-adn', '52', '7', 'neraca'),
(60, 'Neraca Lajur', 'fa fa-arrow-circle-right', '52', '7', 'neraca-lajur'),
(61, 'Akun', 'fa fa-list-alt', '52', '7', 'akun'),
(62, 'Daftar Akun', 'fa fa-th-list', '52', '7', 'daftar-akun');

-- --------------------------------------------------------

--
-- Table structure for table `mid_tes`
--

CREATE TABLE `mid_tes` (
  `id` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `pelajaran` int(11) NOT NULL,
  `nilai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mid_tes`
--

INSERT INTO `mid_tes` (`id`, `siswa`, `pelajaran`, `nilai`) VALUES
(1, 7, 1, 80);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1479231545),
('m140506_102106_rbac_init', 1479231864);

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `module` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `module`) VALUES
(1, 'frontend'),
(2, 'backend'),
(3, 'marketing'),
(4, 'HRD'),
(5, 'akademik'),
(6, 'Kesiswaan'),
(7, 'Keuangan'),
(8, 'administrasi'),
(9, 'Tutor');

-- --------------------------------------------------------

--
-- Table structure for table `mutasi_kas_bulanan`
--

CREATE TABLE `mutasi_kas_bulanan` (
  `id` int(11) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `saldo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mutasi_kas_bulanan`
--

INSERT INTO `mutasi_kas_bulanan` (`id`, `tanggal`, `keterangan`, `status`, `saldo`) VALUES
(1, '11-May-2017', 'materai (2) @6000', 'kredit', 12000),
(2, '11-May-2017', 'Ang. a/n Mayza Ayudhia', 'debet', 165000);

-- --------------------------------------------------------

--
-- Table structure for table `neraca`
--

CREATE TABLE `neraca` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `neraca`
--

INSERT INTO `neraca` (`id`, `keterangan`, `status`, `jumlah`) VALUES
(1, 'KAS', 'Aktiva', 372393677);

-- --------------------------------------------------------

--
-- Table structure for table `neraca_lajur`
--

CREATE TABLE `neraca_lajur` (
  `id` int(11) NOT NULL,
  `no_akun` int(11) NOT NULL,
  `nama_akun` varchar(100) NOT NULL,
  `neraca_saldo_debet` int(11) NOT NULL,
  `neraca_saldo_kredit` int(11) NOT NULL,
  `penyesuaian_debet` int(11) NOT NULL,
  `penyesuaian_kredit` int(11) NOT NULL,
  `ns_disesuaikan_debet` int(11) NOT NULL,
  `ns_disesuaikan_kredit` int(11) NOT NULL,
  `laba_rugi_debet` int(11) NOT NULL,
  `laba_rugi_kredit` int(11) NOT NULL,
  `neraca_debet` int(11) NOT NULL,
  `neraca_kredit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `observasi_kelas`
--

CREATE TABLE `observasi_kelas` (
  `id` int(11) NOT NULL,
  `tutor` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `jam_ke` int(11) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `hari` varchar(100) NOT NULL,
  `p1` int(11) NOT NULL,
  `p2` int(11) NOT NULL,
  `p3` int(11) NOT NULL,
  `p4` int(11) NOT NULL,
  `p5` int(11) NOT NULL,
  `p6` int(11) NOT NULL,
  `p7` int(11) NOT NULL,
  `p8` int(11) NOT NULL,
  `p9` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `tanggal_lahir` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `email` varchar(100) NOT NULL,
  `hobi` varchar(100) NOT NULL,
  `makanan_favorit` varchar(100) NOT NULL,
  `warna_favorit` varchar(100) NOT NULL,
  `motto` text NOT NULL,
  `gaji_pokok` int(11) NOT NULL,
  `tunjangan` int(11) NOT NULL,
  `tunjangan_mengajar` int(11) NOT NULL,
  `tunjangan_lain` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `nama_lengkap`, `tanggal_lahir`, `alamat`, `no_telp`, `email`, `hobi`, `makanan_favorit`, `warna_favorit`, `motto`, `gaji_pokok`, `tunjangan`, `tunjangan_mengajar`, `tunjangan_lain`) VALUES
(2, 'M. Tholib Hasan', '06-Jun-1985', 'Pasar Sore', '085806313588', '-', 'Bermain sepak bola', 'Bakso', 'Putih', '-', 1500000, 300000, 0, 0),
(3, 'Eny yuliana', '04-Jul-1992', 'Deket Agung Sugio Lamongan', '085733308256', '3niyulian4@gmail.com', 'Nonton TV, Belanja', 'Bakso, Nasi Goreng, Sate, Rawon, Gule, Krengsengan', 'Warna Soft', 'Belajar, Berjuan, Dan Berdoa\r\n', 1700000, 350000, 0, 0),
(4, 'Nisfatul Laila', '09-Dec-1991', 'Sawo Dukun Gresik', '085755394827', 'nisfatulaila@gmail.com', 'Nonton Film', 'Bakso, Sate, Buah-Buahan', 'Hitam', 'Jadi Diri sendiri Dan Hidup Mandiri', 1300000, 200000, 0, 0),
(5, 'Yoesi Roesita Dewi', '14-Mar-1998', 'Jl. Sunan Drajad No. 249 Lamongan', '08121745685', 'yosi_dewi39@yahoo.co.id', 'Nonton Film', 'Soto', 'Biru, Hitam', 'Everything will be okay in the end', 2100000, 400000, 0, 0),
(6, 'Iwan Suheriono', '08-Sep-1989', 'Sugio, Lamongan', '085735415754', 'iherio@ymail.com', 'Nonton Film', 'Rujak Buah/Rujak Lontong/Lontong Sayur/Boranan', 'Merah & Putih', 'Give more get more\r\n', 1800000, 3000000, 0, 0),
(7, 'Rahmiyati Fauziyah ', '30-Jun-1988', 'Dsn. Klating RT 01 RW 01 Tikung Lamongan', '085733266469', 'mikudori@rocketmail.com', 'Membaca', 'Bakso, Rujak', 'Orange', 'Sedekah, Sedekah, Sedekah, Nabung, Hemat\r\n', 1500000, 300000, 0, 0),
(8, 'Sandrina Rosi', '09-Apr-1988', 'Banjar Mendalan, GG Lele No 3 Lamongan', '085843086443', 'belle_dabeauty@yahoo.com', 'Nonton Film', 'Tahu Campur', 'Merah', 'Shine like star wherever you are\r\n', 1700000, 350000, 0, 0),
(9, 'Diana Anggraini', '07-Jul-1992', 'Dsn Plalangan Ds Plosowahyu RT 2 RW 3 Lamongan', '085733366556', 'kotaksurat.dianna@gmail.com', 'Membaca', 'Bakso', 'Merah, Biru', 'Selalu berdoa dan bersyukur', 1800000, 300000, 0, 0),
(10, 'Viki Sevila Sani', '12-Sep-1990', 'Tikung, Lamongan', '082134416312', 'vikisevila@gmail.com', 'Nonton Film & Mendengarkan musik', 'Semua makanan (Yang penting enak)', 'Semua warna soft', 'Eat for life not life for eat\r\n', 1500000, 300000, 0, 0),
(11, 'Dian Eka Lestari', '15-Dec-1992', 'Ds Siman Kecamatan Sekaran Kabupaten Lamongan', '085791132626', 'dianeka27@gmail.com', 'Mendengarkan musik', 'Sate', 'Tosca', 'Life is choice', 1700000, 200000, 0, 0),
(12, 'Faridatus Saadah', '08-Jan-1889', 'Dsn Pongangan Tikung Lamongan', '085773647135', 'faridatussaadah79@yahoo.co.id', 'Nonton TV', 'Nasi Goreng', 'Pink', '-', 1500000, 300000, 0, 0),
(13, 'Intan Novita Sari', '30-Nov-1990', 'Pemgangsalan, Kalitengah, Lamongan', '085853111513', 'intannera30@gmail.com', 'Membaca/Menulis', 'Rujak', 'Putih', 'Whatever doesn’t kill you, Makes you stronger\r\n', 1500000, 300000, 0, 0),
(14, 'Eni Rahmawati', '16-Feb-1992', 'Dsn Mayong Ds Sidomlangean Kedungpring Lamongan', '085708189692', '-', 'Nonton Film ', 'Mie', 'Abu-abu, Merah, Pink', 'Hidup itu perjuangan, Perjuangkanlah mana yang benar\r\n', 1700000, 300000, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_siswa`
--

CREATE TABLE `pembayaran_siswa` (
  `id` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `bulan` varchar(100) NOT NULL,
  `angsuran_ke` int(11) NOT NULL,
  `biaya_program` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran_siswa`
--

INSERT INTO `pembayaran_siswa` (`id`, `siswa`, `keterangan`, `tanggal`, `jumlah`, `tahun`, `bulan`, `angsuran_ke`, `biaya_program`) VALUES
(1, 7, 'Pembayaran SPP Bulanan', '09-Apr-2017', 450000, 2017, 'April', 1, 0),
(2, 7, 'Pembayaran A', '24-Apr-2017', 360000, 2017, 'April', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pengamatan_pembelajaran`
--

CREATE TABLE `pengamatan_pembelajaran` (
  `id` int(11) NOT NULL,
  `tutor` int(11) NOT NULL,
  `jam_ke` int(11) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `hari` varchar(100) NOT NULL,
  `p1` int(11) NOT NULL,
  `p2` int(11) NOT NULL,
  `p3` int(11) NOT NULL,
  `p4` int(11) NOT NULL,
  `p5` int(11) NOT NULL,
  `p6` int(11) NOT NULL,
  `p7` int(11) NOT NULL,
  `p8` int(11) NOT NULL,
  `p9` int(11) NOT NULL,
  `p10` int(11) NOT NULL,
  `p11` int(11) NOT NULL,
  `p12` int(11) NOT NULL,
  `p13` int(11) NOT NULL,
  `p14` int(11) NOT NULL,
  `p15` int(11) NOT NULL,
  `p16` int(11) NOT NULL,
  `p17` int(11) NOT NULL,
  `p18` int(11) NOT NULL,
  `p19` int(11) NOT NULL,
  `p20` int(11) NOT NULL,
  `p21` int(11) NOT NULL,
  `p22` int(11) NOT NULL,
  `p23` int(11) NOT NULL,
  `p24` int(11) NOT NULL,
  `p25` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengamatan_pembelajaran`
--

INSERT INTO `pengamatan_pembelajaran` (`id`, `tutor`, `jam_ke`, `tanggal`, `hari`, `p1`, `p2`, `p3`, `p4`, `p5`, `p6`, `p7`, `p8`, `p9`, `p10`, `p11`, `p12`, `p13`, `p14`, `p15`, `p16`, `p17`, `p18`, `p19`, `p20`, `p21`, `p22`, `p23`, `p24`, `p25`) VALUES
(1, 1, 1, '09-May-2017', 'selasa', 3, 2, 2, 2, 3, 2, 4, 4, 3, 2, 3, 1, 3, 2, 4, 2, 1, 4, 2, 2, 4, 4, 3, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran`
--

CREATE TABLE `pengeluaran` (
  `id` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `penganggung_jawab` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `bulan` varchar(100) NOT NULL,
  `tahun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran`
--

INSERT INTO `pengeluaran` (`id`, `keterangan`, `tanggal`, `penganggung_jawab`, `jumlah`, `bulan`, `tahun`) VALUES
(1, 'Pembelian buku ajar', '09-Apr-2017', 'Efan Supardi', 200000, 'April', 2017);

-- --------------------------------------------------------

--
-- Table structure for table `penggajian`
--

CREATE TABLE `penggajian` (
  `id` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `bulan` varchar(100) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `tahun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penggajian`
--

INSERT INTO `penggajian` (`id`, `pegawai`, `jumlah`, `bulan`, `tanggal`, `tahun`) VALUES
(1, 2, 1800000, 'Januari', '06-Apr-2017:11.00.31 am', 2017);

-- --------------------------------------------------------

--
-- Table structure for table `pretes`
--

CREATE TABLE `pretes` (
  `id` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `nilai` varchar(100) NOT NULL,
  `pelajaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pretes`
--

INSERT INTO `pretes` (`id`, `siswa`, `nilai`, `pelajaran`) VALUES
(1, 7, '80', 1);

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(11) NOT NULL,
  `program` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `program`) VALUES
(1, 'English Golden Star');

-- --------------------------------------------------------

--
-- Table structure for table `raport`
--

CREATE TABLE `raport` (
  `id` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `vocabulary` int(11) NOT NULL,
  `fluency` int(11) NOT NULL,
  `accuracy` int(11) NOT NULL,
  `pronunciation` int(11) NOT NULL,
  `intonation` int(11) NOT NULL,
  `understanding` int(11) NOT NULL,
  `diction` int(11) NOT NULL,
  `respect` int(11) NOT NULL,
  `honest` int(11) NOT NULL,
  `care` int(11) NOT NULL,
  `brave` int(11) NOT NULL,
  `convidence` int(11) NOT NULL,
  `communicative` int(11) NOT NULL,
  `sosial_awarnes` int(11) NOT NULL,
  `curiousity` int(11) NOT NULL,
  `team_work` int(11) NOT NULL,
  `mengulang_materi` varchar(10) NOT NULL,
  `mempelajari_materi` varchar(10) NOT NULL,
  `mengerjakan_tugas` varchar(10) NOT NULL,
  `memutar_audio` varchar(10) NOT NULL,
  `bermain_game` varchar(10) NOT NULL,
  `belajar_online` varchar(10) NOT NULL,
  `praktek_materi` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `raport`
--

INSERT INTO `raport` (`id`, `siswa`, `vocabulary`, `fluency`, `accuracy`, `pronunciation`, `intonation`, `understanding`, `diction`, `respect`, `honest`, `care`, `brave`, `convidence`, `communicative`, `sosial_awarnes`, `curiousity`, `team_work`, `mengulang_materi`, `mempelajari_materi`, `mengerjakan_tugas`, `memutar_audio`, `bermain_game`, `belajar_online`, `praktek_materi`) VALUES
(1, 7, 3, 3, 3, 2, 3, 4, 2, 2, 3, 3, 4, 4, 3, 4, 3, 1, 'ya', 'tidak', 'ya', 'ya', 'tidak', 'ya', 'ya');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'developer'),
(2, 'administrator'),
(3, 'keuangan'),
(4, 'marketing'),
(5, 'hrd'),
(6, 'akademik'),
(7, 'administrasi'),
(8, 'kesiswaan');

-- --------------------------------------------------------

--
-- Table structure for table `role_menu`
--

CREATE TABLE `role_menu` (
  `id` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_menu`
--

INSERT INTO `role_menu` (`id`, `role`, `menu`) VALUES
(14, 1, 4),
(13, 1, 3),
(12, 1, 2),
(11, 1, 1),
(15, 1, 5),
(651, 2, 60),
(650, 2, 59),
(649, 2, 58),
(648, 2, 57),
(647, 2, 56),
(646, 2, 55),
(645, 2, 54),
(644, 2, 53),
(643, 2, 52),
(642, 2, 51),
(641, 2, 50),
(640, 2, 49),
(639, 2, 48),
(638, 2, 47),
(637, 2, 46),
(636, 2, 45),
(635, 2, 44),
(634, 2, 43),
(633, 2, 42),
(632, 2, 41),
(631, 2, 40),
(630, 2, 39),
(629, 2, 38),
(628, 2, 37),
(627, 2, 36),
(626, 2, 35),
(625, 2, 34),
(624, 2, 33),
(623, 2, 32),
(622, 2, 30),
(621, 2, 29),
(620, 2, 28),
(619, 2, 31),
(618, 2, 26),
(617, 2, 25),
(616, 2, 24),
(615, 2, 23),
(614, 2, 22),
(613, 2, 21),
(612, 2, 20),
(611, 2, 18),
(610, 2, 17),
(609, 2, 16),
(608, 2, 15),
(607, 2, 14),
(606, 2, 13),
(605, 2, 12),
(604, 2, 11),
(603, 2, 10),
(602, 2, 9),
(601, 2, 8),
(600, 2, 7),
(599, 2, 6),
(652, 2, 61),
(653, 2, 62),
(654, 3, 7),
(655, 3, 31),
(656, 3, 32),
(657, 3, 44),
(658, 3, 45),
(659, 3, 52),
(660, 3, 53),
(661, 3, 54),
(662, 3, 55),
(663, 3, 56),
(664, 3, 57),
(665, 3, 58),
(666, 3, 59),
(667, 3, 60),
(668, 3, 61),
(669, 3, 62),
(670, 4, 12),
(671, 4, 13),
(672, 4, 14),
(673, 4, 26),
(674, 4, 28),
(675, 4, 29),
(676, 4, 30),
(677, 5, 8),
(678, 5, 20),
(679, 5, 46),
(680, 5, 47),
(681, 5, 48),
(682, 5, 49),
(683, 5, 50),
(684, 5, 51),
(685, 6, 15),
(686, 6, 16),
(687, 6, 17),
(688, 6, 18),
(689, 6, 21),
(690, 7, 9),
(691, 7, 10),
(692, 7, 11),
(693, 8, 6),
(694, 8, 22),
(695, 8, 23),
(696, 8, 24),
(697, 8, 25);

-- --------------------------------------------------------

--
-- Table structure for table `silabus`
--

CREATE TABLE `silabus` (
  `id` int(11) NOT NULL,
  `mata_pelajaran` int(11) NOT NULL,
  `standar_kompetensi` text NOT NULL,
  `kompetensi_dasar` text NOT NULL,
  `hasil_belajar` text NOT NULL,
  `indokator_hasil_belajar` text NOT NULL,
  `materi_pokok` text NOT NULL,
  `alokasi_waktu` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id` int(11) NOT NULL,
  `id_siswa` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `tanggal_lahir` varchar(100) NOT NULL,
  `status_keaktifan` int(11) NOT NULL,
  `status_kursus` int(11) NOT NULL,
  `catatan_siswa` varchar(200) NOT NULL,
  `program` int(11) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `status` varchar(100) NOT NULL,
  `nama_ayah` varchar(100) NOT NULL,
  `nama_ibu` varchar(100) NOT NULL,
  `alamat_ayah` varchar(100) NOT NULL,
  `alamat_ibu` varchar(100) NOT NULL,
  `pekerjaan_ayah` varchar(100) NOT NULL,
  `pekerjaan_ibu` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `tahun_akademik` int(11) NOT NULL,
  `jenis_kelamin` varchar(100) NOT NULL,
  `sekolah` varchar(100) NOT NULL,
  `status_siswa` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `id_siswa`, `nama_lengkap`, `alamat`, `tanggal_lahir`, `status_keaktifan`, `status_kursus`, `catatan_siswa`, `program`, `tempat_lahir`, `no_hp`, `status`, `nama_ayah`, `nama_ibu`, `alamat_ayah`, `alamat_ibu`, `pekerjaan_ayah`, `pekerjaan_ibu`, `foto`, `tahun_akademik`, `jenis_kelamin`, `sekolah`, `status_siswa`) VALUES
(7, '0001239', 'Damara Suci Hanania Arfa', 'Jl. Kusuma Bangsa Regency E / 17 Lamongan', '11-Jul-2009', 1, 1, '0', 1, 'lamongan', '087856329111', 'Pelajar', 'Zammiah Wicahyo', 'Halimatus Sa’diyah', 'Jl. Kusuma Bangsa Regency E / 17 Lamongan', 'Jl. Kusuma Bangsa Regency E / 17 Lamongan', 'Pegawai Swasta', '-', '12289507_1175576945799483_5912750962013069275_n.jpg', 2013, '2', '', 'lama'),
(8, '0001240', 'Danendra Muhammad Raharto', 'Kusuma Bangsa Regency F 8 Lamongan', '03-Sep-2008', 1, 1, '0', 1, 'Jember, 03September 2008', '081216377612', 'Pelajar', 'Joko Raharto', 'Noni Amilda', 'Kusuma Bangsa Regency F 8 Lamongan', 'Kusuma Bangsa Regency F 8 Lamongan', 'PNS', '-', '13151439_1119489954769993_7202456452794871378_n.jpg', 2015, '1', '', 'lama'),
(9, '0001241', 'Alfiyan Hamsyah', 'Kusuma Bangsa Regency F 8 Lamongan', '25-Apr-2007', 1, 1, '-', 1, 'lamongan', '087856329111', 'Pelajar', 'Zammiah Wicahyo', 'Halimatus Sa’diyah', 'Jl. Kusuma Bangsa Regency E / 17 Lamongan', 'Jl. Kusuma Bangsa Regency E / 17 Lamongan', 'Pegawai Swasta', '-', '1031-200.png', 2013, '1', 'TK Rabasya', 'baru'),
(10, '0001242', 'Mega Ayu', 'Kusuma Bangsa Regency F 8 Lamongan', '03-Apr-2008', 1, 1, '-', 1, 'lamongan', '081216377612', 'Pelajar', 'Zammiah Wicahyo', 'Halimatus Sa’diyah', 'Jl. Kusuma Bangsa Regency E / 17 Lamongan', 'Jl. Kusuma Bangsa Regency E / 17 Lamongan', 'Pegawai Swasta', '-', '1031-200.png', 2014, '2', 'TK Rabasya', 'baru'),
(11, '0001243', 'Amanda Manopo', 'Jl. Kusuma Bangsa Regency E / 17 Lamongan', '03-Apr-2009', 1, 1, '-', 1, 'lamongan', '087856329111', 'Pelajar', 'Zammiah Wicahyo', 'Halimatus Sa’diyah', 'Kusuma Bangsa Regency F 8 Lamongan', 'Kusuma Bangsa Regency F 8 Lamongan', 'PNS', '-', '12289507_1175576945799483_5912750962013069275_n.jpg', 2014, '2', 'TK Rabasya', 'baru');

-- --------------------------------------------------------

--
-- Table structure for table `speaking_tes`
--

CREATE TABLE `speaking_tes` (
  `id` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `nilai_1` int(11) NOT NULL,
  `nilai_2` int(11) NOT NULL,
  `nilai_3` int(11) NOT NULL,
  `nilai_4` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `speaking_tes`
--

INSERT INTO `speaking_tes` (`id`, `siswa`, `nilai_1`, `nilai_2`, `nilai_3`, `nilai_4`) VALUES
(1, 7, 80, 80, 80, 80);

-- --------------------------------------------------------

--
-- Table structure for table `status_aktifitas`
--

CREATE TABLE `status_aktifitas` (
  `id` int(11) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_aktifitas`
--

INSERT INTO `status_aktifitas` (`id`, `status`) VALUES
(1, 'berjalan'),
(2, 'gagal'),
(3, 'slesai');

-- --------------------------------------------------------

--
-- Table structure for table `status_keaktifan`
--

CREATE TABLE `status_keaktifan` (
  `id` int(11) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_keaktifan`
--

INSERT INTO `status_keaktifan` (`id`, `status`) VALUES
(1, 'Aktif'),
(2, 'Tidak Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `status_kursus`
--

CREATE TABLE `status_kursus` (
  `id` int(11) NOT NULL,
  `status_kursus` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_kursus`
--

INSERT INTO `status_kursus` (`id`, `status_kursus`) VALUES
(1, 'Reguler'),
(2, 'Privat');

-- --------------------------------------------------------

--
-- Table structure for table `status_siswa`
--

CREATE TABLE `status_siswa` (
  `id` int(11) NOT NULL,
  `status_siswa` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_siswa`
--

INSERT INTO `status_siswa` (`id`, `status_siswa`) VALUES
(1, 'Berprestasi'),
(2, 'Bermasalah'),
(3, 'regular'),
(4, 'instansi');

-- --------------------------------------------------------

--
-- Table structure for table `supervisi`
--

CREATE TABLE `supervisi` (
  `id` int(11) NOT NULL,
  `tutor` int(11) NOT NULL,
  `supervisi` varchar(100) NOT NULL,
  `level` varchar(100) NOT NULL,
  `hari` varchar(100) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `jam_ke` int(11) NOT NULL,
  `p1` int(11) NOT NULL,
  `p2` int(11) NOT NULL,
  `p3` int(11) NOT NULL,
  `p4` int(11) NOT NULL,
  `p5` int(11) NOT NULL,
  `p6` int(11) NOT NULL,
  `p7` int(11) NOT NULL,
  `p8` int(11) NOT NULL,
  `p9` int(11) NOT NULL,
  `p10` int(11) NOT NULL,
  `p11` int(11) NOT NULL,
  `p12` int(11) NOT NULL,
  `p13` int(11) NOT NULL,
  `p14` int(11) NOT NULL,
  `p15` int(11) NOT NULL,
  `p16` int(11) NOT NULL,
  `p17` int(11) NOT NULL,
  `p18` int(11) NOT NULL,
  `p19` int(11) NOT NULL,
  `p20` int(11) NOT NULL,
  `p21` int(11) NOT NULL,
  `p22` int(11) NOT NULL,
  `p23` int(11) NOT NULL,
  `p24` int(11) NOT NULL,
  `p25` int(11) NOT NULL,
  `p26` int(11) NOT NULL,
  `p27` int(11) NOT NULL,
  `p28` int(11) NOT NULL,
  `p29` int(11) NOT NULL,
  `p30` int(11) NOT NULL,
  `p31` int(11) NOT NULL,
  `p32` int(11) NOT NULL,
  `p33` int(11) NOT NULL,
  `p34` int(11) NOT NULL,
  `p35` int(11) NOT NULL,
  `p36` int(11) NOT NULL,
  `p37` int(11) NOT NULL,
  `p38` int(11) NOT NULL,
  `p39` int(11) NOT NULL,
  `saran` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supervisi`
--

INSERT INTO `supervisi` (`id`, `tutor`, `supervisi`, `level`, `hari`, `tanggal`, `jam_ke`, `p1`, `p2`, `p3`, `p4`, `p5`, `p6`, `p7`, `p8`, `p9`, `p10`, `p11`, `p12`, `p13`, `p14`, `p15`, `p16`, `p17`, `p18`, `p19`, `p20`, `p21`, `p22`, `p23`, `p24`, `p25`, `p26`, `p27`, `p28`, `p29`, `p30`, `p31`, `p32`, `p33`, `p34`, `p35`, `p36`, `p37`, `p38`, `p39`, `saran`) VALUES
(1, 1, 'irfan', '1', 'senin', '08-May-2017', 1, 3, 4, 2, 5, 2, 4, 2, 3, 2, 2, 4, 3, 3, 2, 1, 4, 4, 3, 2, 2, 2, 2, 4, 2, 2, 4, 2, 4, 2, 4, 3, 2, 4, 2, 3, 4, 3, 2, 2, 'semangat mas');

-- --------------------------------------------------------

--
-- Table structure for table `surat`
--

CREATE TABLE `surat` (
  `id` int(11) NOT NULL,
  `no_surat` varchar(100) NOT NULL,
  `perihal` text NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `status_surat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat`
--

INSERT INTO `surat` (`id`, `no_surat`, `perihal`, `tanggal`, `status_surat`) VALUES
(1, 'SR001/AA', 'Undangan keluarga', '10-feb-2017', 'surat keluar');

-- --------------------------------------------------------

--
-- Table structure for table `tim_marketing`
--

CREATE TABLE `tim_marketing` (
  `id` int(11) NOT NULL,
  `tim` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tim_marketing`
--

INSERT INTO `tim_marketing` (`id`, `tim`) VALUES
(1, 'Tim garuda');

-- --------------------------------------------------------

--
-- Table structure for table `tools_report`
--

CREATE TABLE `tools_report` (
  `id` int(11) NOT NULL,
  `aktifitas` int(11) NOT NULL,
  `tool` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tools_report`
--

INSERT INTO `tools_report` (`id`, `aktifitas`, `tool`, `jumlah`, `harga`) VALUES
(1, 1, 'a', 1, 2000000),
(2, 2, 'Booth Pameran', 1, 2500000),
(3, 2, 'Brosur', 3, 750000);

-- --------------------------------------------------------

--
-- Table structure for table `tutor`
--

CREATE TABLE `tutor` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tutor`
--

INSERT INTO `tutor` (`id`, `nama_lengkap`, `alamat`) VALUES
(1, 'Andi Heraya ', 'Lamongan');

-- --------------------------------------------------------

--
-- Table structure for table `unit_tes`
--

CREATE TABLE `unit_tes` (
  `id` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `pelajaran` int(11) NOT NULL,
  `nilai_1` int(11) NOT NULL,
  `nilai_2` int(11) NOT NULL,
  `nilai_3` int(11) NOT NULL,
  `nilai_4` int(11) NOT NULL,
  `nilai_5` int(11) NOT NULL,
  `nilai_6` int(11) NOT NULL,
  `nilai_7` int(11) NOT NULL,
  `nilai_8` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit_tes`
--

INSERT INTO `unit_tes` (`id`, `siswa`, `pelajaran`, `nilai_1`, `nilai_2`, `nilai_3`, `nilai_4`, `nilai_5`, `nilai_6`, `nilai_7`, `nilai_8`) VALUES
(1, 7, 1, 80, 80, 80, 80, 80, 80, 80, 80);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `role`) VALUES
(1, 'developer', 'hX2iXXGf-6-jNoAidotAxlhUBZxhgNdS', '$2y$13$0N.Ruj8ZRE8GifJVTvAhyefdXX2qHMYhhlGih1O5Avy/z5WJGugI.', NULL, 'developer@mail.com', 10, 1477197448, 1479234102, 1),
(2, 'admin', '1RnIfzw1x4lVoRBWnVyxKex7C2Yzz53j', '$2y$13$2Sv7RcTDAiEfhqtgkZvRA.4Am./iM3Pdt7djQsxMsklGSvwpxwm0u', NULL, 'admin@siakad.com', 10, 1480135793, 1480135793, 2),
(13, 'keuangan', 'zfMFlqhy45Y6wWi6pkSzSpf50_4qqQni', '$2y$13$GbxjYowlEOPDBZJUj4GTSuFjuVTeHVP6skT5ChY7YJvQ6Xmd6TV1O', NULL, NULL, 10, 1494519476, 1494519476, 3),
(14, 'marketing', 'ozDsqE5bOVxderr8Yu-ZiPk3MELUkGfK', '$2y$13$ECgJNH34kCI54P7YTqQRU.pnyEfHSBEkhF65cVN9m1WNYFBOuhunG', NULL, NULL, 10, 1494521604, 1494521604, 4),
(19, 'kesiswaan', 'mzLMndDyYLd_TM3xjOBXyF7hr6-GPssX', '$2y$13$kH1k/vvym5PmvWosBvT9SOEFZl09NcGBhAjEHuky4yAeSM//g8.T2', NULL, NULL, 10, 1494521741, 1494521741, 8),
(16, 'hrd', 'OeZOJaXxpwuOMEM2JGZJfR5q6ftLWY-u', '$2y$13$OiU6jQULc1l8C0iXnbUZgOfyR8MSzUAW0EFEWRTdoa6xDGR.HGUpm', NULL, NULL, 10, 1494521679, 1494521679, 5),
(17, 'akademik', '71vhBkPvsoykgZv1zr7E-3xiVIl7vlA7', '$2y$13$uh5sWxiWhm0mDLrIuykFXeZkXeS7xLvJ4sTGM4dbWUTheu7P39xQi', NULL, NULL, 10, 1494521692, 1494521692, 6),
(18, 'administrasi', 'nkfY0ZHuR6RDD8vSNodSC8NnDtMHQ0vv', '$2y$13$nmY3EqxRbTOOfYT7NbctgeYuxfMQimPaQ8ShtKFHjUIchjvnN.R5y', NULL, NULL, 10, 1494521722, 1494521722, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi_siswa`
--
ALTER TABLE `absensi_siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `program` (`program`,`siswa`),
  ADD KEY `siswa` (`siswa`);

--
-- Indexes for table `absensi_tutor`
--
ALTER TABLE `absensi_tutor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tutor` (`tutor`),
  ADD KEY `kelas` (`kelas`);

--
-- Indexes for table `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role` (`role`),
  ADD KEY `menu` (`menu`);

--
-- Indexes for table `aktifitas`
--
ALTER TABLE `aktifitas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_aktifitas` (`status_aktifitas`);

--
-- Indexes for table `aktifitas_tim`
--
ALTER TABLE `aktifitas_tim`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tim` (`tim`),
  ADD KEY `aktifitas` (`aktifitas`);

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `biaya_program`
--
ALTER TABLE `biaya_program`
  ADD PRIMARY KEY (`id`),
  ADD KEY `program` (`program`);

--
-- Indexes for table `biaya_program_siswa`
--
ALTER TABLE `biaya_program_siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswa` (`siswa`),
  ADD KEY `biaya_program` (`biaya_program`);

--
-- Indexes for table `buku_besar`
--
ALTER TABLE `buku_besar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buku_kas`
--
ALTER TABLE `buku_kas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buku_tamu`
--
ALTER TABLE `buku_tamu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campaign_report`
--
ALTER TABLE `campaign_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daftar_akun`
--
ALTER TABLE `daftar_akun`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nomor_akun` (`nomor_akun`);

--
-- Indexes for table `daily_tes`
--
ALTER TABLE `daily_tes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswa` (`siswa`),
  ADD KEY `pelajaran` (`pelajaran`);

--
-- Indexes for table `data_kelas`
--
ALTER TABLE `data_kelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `program` (`program`);

--
-- Indexes for table `data_tim_maketing`
--
ALTER TABLE `data_tim_maketing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pegawai` (`pegawai`),
  ADD KEY `tim` (`tim`);

--
-- Indexes for table `data_tim_marketing`
--
ALTER TABLE `data_tim_marketing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tim` (`tim`),
  ADD KEY `pegawai` (`pegawai`);

--
-- Indexes for table `final_tes`
--
ALTER TABLE `final_tes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswa` (`siswa`),
  ADD KEY `pelajaran` (`pelajaran`);

--
-- Indexes for table `jadwal_pelajaran`
--
ALTER TABLE `jadwal_pelajaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mata_pelajaran` (`mata_pelajaran`);

--
-- Indexes for table `jenis_kelamin`
--
ALTER TABLE `jenis_kelamin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnal_harian`
--
ALTER TABLE `jurnal_harian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnal_penyesuaian`
--
ALTER TABLE `jurnal_penyesuaian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnal_umum`
--
ALTER TABLE `jurnal_umum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_aktifitas`
--
ALTER TABLE `kategori_aktifitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `program` (`siswa`),
  ADD KEY `siswa` (`siswa`),
  ADD KEY `kelas` (`kelas`);

--
-- Indexes for table `laporan_harian_siswa`
--
ALTER TABLE `laporan_harian_siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jurnal` (`jurnal`),
  ADD KEY `siswa` (`siswa`);

--
-- Indexes for table `laporan_laba_rugi`
--
ALTER TABLE `laporan_laba_rugi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan_perubahan_ekuitas`
--
ALTER TABLE `laporan_perubahan_ekuitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marketer_report`
--
ALTER TABLE `marketer_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marketing_tools`
--
ALTER TABLE `marketing_tools`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aktifitas` (`aktifitas`);

--
-- Indexes for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module` (`module`);

--
-- Indexes for table `mid_tes`
--
ALTER TABLE `mid_tes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswa` (`siswa`),
  ADD KEY `pelajaran` (`pelajaran`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mutasi_kas_bulanan`
--
ALTER TABLE `mutasi_kas_bulanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neraca`
--
ALTER TABLE `neraca`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neraca_lajur`
--
ALTER TABLE `neraca_lajur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `observasi_kelas`
--
ALTER TABLE `observasi_kelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tutor` (`tutor`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembayaran_siswa`
--
ALTER TABLE `pembayaran_siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswa` (`siswa`),
  ADD KEY `biaya_program` (`biaya_program`);

--
-- Indexes for table `pengamatan_pembelajaran`
--
ALTER TABLE `pengamatan_pembelajaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tutor` (`tutor`);

--
-- Indexes for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penggajian`
--
ALTER TABLE `penggajian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pegawai` (`pegawai`);

--
-- Indexes for table `pretes`
--
ALTER TABLE `pretes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswa` (`siswa`),
  ADD KEY `pelajaran` (`pelajaran`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raport`
--
ALTER TABLE `raport`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswa` (`siswa`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_menu`
--
ALTER TABLE `role_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role` (`role`),
  ADD KEY `menu` (`menu`);

--
-- Indexes for table `silabus`
--
ALTER TABLE `silabus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mata_pelajaran` (`mata_pelajaran`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_keaktifan` (`status_keaktifan`,`status_kursus`),
  ADD KEY `status_kursus` (`status_kursus`),
  ADD KEY `catatan_siswa` (`catatan_siswa`),
  ADD KEY `catatan_siswa_2` (`catatan_siswa`);

--
-- Indexes for table `speaking_tes`
--
ALTER TABLE `speaking_tes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswa` (`siswa`);

--
-- Indexes for table `status_aktifitas`
--
ALTER TABLE `status_aktifitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_keaktifan`
--
ALTER TABLE `status_keaktifan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_kursus`
--
ALTER TABLE `status_kursus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_siswa`
--
ALTER TABLE `status_siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supervisi`
--
ALTER TABLE `supervisi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tutor` (`tutor`);

--
-- Indexes for table `surat`
--
ALTER TABLE `surat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tim_marketing`
--
ALTER TABLE `tim_marketing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tools_report`
--
ALTER TABLE `tools_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aktifitas` (`aktifitas`);

--
-- Indexes for table `tutor`
--
ALTER TABLE `tutor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit_tes`
--
ALTER TABLE `unit_tes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswa` (`siswa`),
  ADD KEY `pelajaran` (`pelajaran`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD KEY `role` (`role`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi_siswa`
--
ALTER TABLE `absensi_siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `absensi_tutor`
--
ALTER TABLE `absensi_tutor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `action`
--
ALTER TABLE `action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4267;
--
-- AUTO_INCREMENT for table `aktifitas`
--
ALTER TABLE `aktifitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `aktifitas_tim`
--
ALTER TABLE `aktifitas_tim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `biaya_program`
--
ALTER TABLE `biaya_program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `biaya_program_siswa`
--
ALTER TABLE `biaya_program_siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `buku_besar`
--
ALTER TABLE `buku_besar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `buku_kas`
--
ALTER TABLE `buku_kas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `buku_tamu`
--
ALTER TABLE `buku_tamu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `campaign_report`
--
ALTER TABLE `campaign_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `daftar_akun`
--
ALTER TABLE `daftar_akun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `daily_tes`
--
ALTER TABLE `daily_tes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `data_kelas`
--
ALTER TABLE `data_kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `data_tim_maketing`
--
ALTER TABLE `data_tim_maketing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `data_tim_marketing`
--
ALTER TABLE `data_tim_marketing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `final_tes`
--
ALTER TABLE `final_tes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `jadwal_pelajaran`
--
ALTER TABLE `jadwal_pelajaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jenis_kelamin`
--
ALTER TABLE `jenis_kelamin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jurnal_harian`
--
ALTER TABLE `jurnal_harian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `jurnal_penyesuaian`
--
ALTER TABLE `jurnal_penyesuaian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `jurnal_umum`
--
ALTER TABLE `jurnal_umum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kategori_aktifitas`
--
ALTER TABLE `kategori_aktifitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `laporan_harian_siswa`
--
ALTER TABLE `laporan_harian_siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `laporan_laba_rugi`
--
ALTER TABLE `laporan_laba_rugi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `laporan_perubahan_ekuitas`
--
ALTER TABLE `laporan_perubahan_ekuitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `marketer_report`
--
ALTER TABLE `marketer_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `marketing_tools`
--
ALTER TABLE `marketing_tools`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `mid_tes`
--
ALTER TABLE `mid_tes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `mutasi_kas_bulanan`
--
ALTER TABLE `mutasi_kas_bulanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `neraca`
--
ALTER TABLE `neraca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `neraca_lajur`
--
ALTER TABLE `neraca_lajur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `observasi_kelas`
--
ALTER TABLE `observasi_kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `pembayaran_siswa`
--
ALTER TABLE `pembayaran_siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pengamatan_pembelajaran`
--
ALTER TABLE `pengamatan_pembelajaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `penggajian`
--
ALTER TABLE `penggajian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pretes`
--
ALTER TABLE `pretes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `raport`
--
ALTER TABLE `raport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `role_menu`
--
ALTER TABLE `role_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=698;
--
-- AUTO_INCREMENT for table `silabus`
--
ALTER TABLE `silabus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `speaking_tes`
--
ALTER TABLE `speaking_tes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `status_aktifitas`
--
ALTER TABLE `status_aktifitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `status_keaktifan`
--
ALTER TABLE `status_keaktifan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `status_kursus`
--
ALTER TABLE `status_kursus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `status_siswa`
--
ALTER TABLE `status_siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `supervisi`
--
ALTER TABLE `supervisi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `surat`
--
ALTER TABLE `surat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tim_marketing`
--
ALTER TABLE `tim_marketing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tools_report`
--
ALTER TABLE `tools_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tutor`
--
ALTER TABLE `tutor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `unit_tes`
--
ALTER TABLE `unit_tes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `absensi_siswa`
--
ALTER TABLE `absensi_siswa`
  ADD CONSTRAINT `absensi_siswa_ibfk_1` FOREIGN KEY (`program`) REFERENCES `program` (`id`),
  ADD CONSTRAINT `absensi_siswa_ibfk_2` FOREIGN KEY (`siswa`) REFERENCES `siswa` (`id`);

--
-- Constraints for table `aktifitas`
--
ALTER TABLE `aktifitas`
  ADD CONSTRAINT `aktifitas_ibfk_1` FOREIGN KEY (`status_aktifitas`) REFERENCES `status_aktifitas` (`id`);

--
-- Constraints for table `jadwal_pelajaran`
--
ALTER TABLE `jadwal_pelajaran`
  ADD CONSTRAINT `jadwal_pelajaran_ibfk_1` FOREIGN KEY (`mata_pelajaran`) REFERENCES `mata_pelajaran` (`id`);

--
-- Constraints for table `kelas`
--
ALTER TABLE `kelas`
  ADD CONSTRAINT `kelas_ibfk_2` FOREIGN KEY (`siswa`) REFERENCES `siswa` (`id`);

--
-- Constraints for table `pembayaran_siswa`
--
ALTER TABLE `pembayaran_siswa`
  ADD CONSTRAINT `pembayaran_siswa_ibfk_1` FOREIGN KEY (`siswa`) REFERENCES `siswa` (`id`);

--
-- Constraints for table `penggajian`
--
ALTER TABLE `penggajian`
  ADD CONSTRAINT `penggajian_ibfk_1` FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`);

--
-- Constraints for table `silabus`
--
ALTER TABLE `silabus`
  ADD CONSTRAINT `silabus_ibfk_1` FOREIGN KEY (`mata_pelajaran`) REFERENCES `mata_pelajaran` (`id`);

--
-- Constraints for table `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`status_keaktifan`) REFERENCES `status_keaktifan` (`id`),
  ADD CONSTRAINT `siswa_ibfk_2` FOREIGN KEY (`status_kursus`) REFERENCES `status_kursus` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

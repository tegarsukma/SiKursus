<?php

/* @var $this yii\web\View */

$this->title = 'Beranda';
?>
<div class="site-index">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Jumlah Siswa</span>
                    <span class="info-box-number">
                        <?=\app\models\Siswa::find()->count()?>
                        <small>siswa</small></span>
                    <?=\yii\helpers\Html::a('Lihat',['kesiswaan/siswa/'])?>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa fa-user-plus"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Siswa baru</span>
                    <span class="info-box-number">
                        <?=\app\models\Siswa::find()->where(['status_siswa'=>'baru'])->count()?>
                    </span>
                    <?=\yii\helpers\Html::a('Lihat',['kesiswaan/siswa/baru'])?>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-user-plus"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">New Members</span>
                    <span class="info-box-number">
                        <?=\app\models\Siswa::find()->count()?>
                    </span>

                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>


        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="fa fa-edit"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Activity</span>
                    <span class="info-box-number">
                        <?=\app\models\Aktifitas::find()->count()?>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Chart</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <?php
                        use app\models\Pengeluaran;
                        use miloschuman\highcharts\Highcharts;
                        $month=['January','February','March','April','May','June','July','June','August','September','October','November','December'];
                        $data = [];
                        for($i=0;$i<12;$i++){
                            $dat = Pengeluaran::find()->where(['bulan'=>$month[$i],'tahun'=>date('Y')])->count();
                            if($dat == 1){
                                $a = Pengeluaran::find()->where(['bulan'=>$month[$i],'tahun'=>date('Y')])->one();
                                $data[] = $a->jumlah;
                            } else {
                                $data[] = 0;
                            }
                            //echo $month[$i]."-".$dat."<br>";
                        }

                        echo Highcharts::widget([
                            'options' => [
                                'title' => ['text' => 'Pengeluaran'],
                                'xAxis' => [
                                    'categories' => $month
                                ],
                                'yAxis' => [
                                    'title' => ['text' => 'Jumlah Pengeluaran']
                                ],
                                'series' => [
                                    ['data' => $data],
                                ]
                            ]
                        ]);
                        ?>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>Administrasi</h3>

                    <p>Moduls Administrasi</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="./administrasi" class="small-box-footer">Mulai <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>Akademik</h3>

                    <p>Modul Akademik</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="./akademik" class="small-box-footer">Mulai <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>HRD</h3>

                    <p>Moduls HRD</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="./hrd" class="small-box-footer">Mulai <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>Kesiswaan</h3>

                    <p>Modul Kesiswaan</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="./kesiswaan" class="small-box-footer">Mulai <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-purple">
                <div class="inner">
                    <h3>Keuangan</h3>

                    <p>Modul Keuangan</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="./keuangan" class="small-box-footer">Mulai <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-maroon">
                <div class="inner">
                    <h3>Marketing</h3>

                    <p>Modul Pemasaran</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="./marketing" class="small-box-footer">Mulai <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>



</div>

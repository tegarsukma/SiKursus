<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\CampaignReportSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="campaign-report-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'aktifitas') ?>

		<?= $form->field($model, 'evaluasi') ?>

		<?= $form->field($model, 'used_tool') ?>

		<?= $form->field($model, 'jumlah') ?>

		<?php // echo $form->field($model, 'harga') ?>

		<?php // echo $form->field($model, 'tanggal') ?>

		<?php // echo $form->field($model, 'total_registran') ?>

		<?php // echo $form->field($model, 'total_revenue') ?>

		<?php // echo $form->field($model, 'roas') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

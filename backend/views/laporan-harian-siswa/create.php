<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\LaporanHarianSiswa $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'LaporanHarianSiswas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud laporan-harian-siswa-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

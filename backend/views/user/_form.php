<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var app\models\User $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
    'id' => 'User',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options'=>['enctype'=>'multipart/form-data'],
    ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
        </div>
        <?php
        $role = \app\models\Role::find()->where(['!=','role','developer'])->all();
        ?>

        <div class="box-body">
			<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'password_hash')->passwordInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'role')->widget(\kartik\widgets\Select2::className(),[
                'data'=>\yii\helpers\ArrayHelper::map($role,'id','role'),
                'options'=>[
                    'prompt'=>'Pilih Lab'
                ]
            ]) ?>
          
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
            '<span class="fa fa-check"></span> ' .
            ($model->isNewRecord ? 'Create' : 'Save'),
            [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success'
            ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>

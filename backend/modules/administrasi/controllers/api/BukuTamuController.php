<?php

namespace backend\modules\administrasi\controllers\api;

/**
* This is the class for REST controller "BukuTamuController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class BukuTamuController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\BukuTamu';
}

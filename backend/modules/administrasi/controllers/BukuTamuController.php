<?php

namespace backend\modules\administrasi\controllers;

/**
* This is the class for controller "BukuTamuController".
*/
class BukuTamuController extends \backend\modules\administrasi\controllers\base\BukuTamuController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

<?php

namespace backend\modules\administrasi\controllers;

/**
* This is the class for controller "SuratController".
*/
class SuratController extends \backend\modules\administrasi\base\SuratController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

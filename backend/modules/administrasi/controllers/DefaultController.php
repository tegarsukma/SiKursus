<?php

namespace backend\modules\administrasi\controllers;

use yii\web\Controller;

/**
 * Default controller for the `administrasi` module
 */
class DefaultController extends Controller
{
  public function beforeAction($action)
{
   $this->layout = 'main'; //your layout name
   return parent::beforeAction($action);
}
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}

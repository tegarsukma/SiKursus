<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var app\models\Surat $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="surat-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Surat',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options'=>['enctype'=>'multipart/form-data'],
    ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
        </div>

        <div class="box-body">
			<?= $form->field($model, 'no_surat')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'perihal')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'tanggal')->widget(\kartik\date\DatePicker::className(),[
			        'pluginOptions'=>[
			                'format'=>'dd-M-yyyy'
                    ]

            ]) ?>
			<?= $form->field($model, 'status_surat')->widget(\kartik\select2\Select2::className(),[
			        'data'=>['surat masuk'=>'Surat Masuk','surat keluar'=>'Surat Keluar'],
                    'options'=>[
                           'prompt'=>'Pilih Status Surat',
                    ]
            ]) ?>
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
            '<span class="fa fa-check"></span> ' .
            ($model->isNewRecord ? 'Create' : 'Save'),
            [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success'
            ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>


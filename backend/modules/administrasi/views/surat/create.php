<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Surat $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Surats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud surat-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

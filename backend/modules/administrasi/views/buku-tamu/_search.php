<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\BukuTamuSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="buku-tamu-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'nama_lengkap') ?>

		<?= $form->field($model, 'alamat') ?>

		<?= $form->field($model, 'perihal') ?>

		<?= $form->field($model, 'orang_tujuan') ?>

		<?php // echo $form->field($model, 'jam_kedatangan') ?>

		<?php // echo $form->field($model, 'tanggal') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\BukuTamu $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'BukuTamus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud buku-tamu-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

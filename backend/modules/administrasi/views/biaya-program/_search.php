<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\BiayaProgramSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="biaya-program-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'program') ?>

		<?= $form->field($model, 'jumlah_siswa') ?>

		<?= $form->field($model, 'durasi') ?>

		<?= $form->field($model, 'jenis_kursus') ?>

		<?php // echo $form->field($model, 'banyak_angsuran') ?>

		<?php // echo $form->field($model, 'jumlah_biaya') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

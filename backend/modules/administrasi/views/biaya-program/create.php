<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\BiayaProgram $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'BiayaPrograms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud biaya-program-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

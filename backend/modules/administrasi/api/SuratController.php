<?php

namespace backend\modules\administrasi\api;

/**
* This is the class for REST controller "SuratController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class SuratController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Surat';
}

<?php

namespace backend\modules\tutor\controllers;

/**
* This is the class for controller "SilabusController".
*/
class SilabusController extends \backend\modules\tutor\controllers\base\SilabusController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

<?php

namespace backend\modules\tutor\controllers;

class TesReportController extends \backend\modules\tutor\controllers\base\TesReportController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }

}

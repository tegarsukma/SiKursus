<?php

namespace backend\modules\tutor\controllers;

/**
* This is the class for controller "JurnalHarianController".
*/
class JurnalHarianController extends \backend\modules\tutor\controllers\base\JurnalHarianController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

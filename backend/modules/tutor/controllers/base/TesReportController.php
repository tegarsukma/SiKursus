<?php

namespace backend\modules\tutor\controllers\base;

use app\models\SiswaSearch;
use dmstr\bootstrap\Tabs;
use yii\helpers\Url;

class TesReportController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel  = new SiswaSearch();
        $dataProvider = $searchModel->search($_GET);

        Tabs::clearLocalStorage();

        Url::remember();
        \Yii::$app->session['__crudReturnUrl'] = null;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionView($id)
    {
        \Yii::$app->session['__crudReturnUrl'] = Url::previous();
        Url::remember();
        Tabs::rememberActiveState();

        return $this->render('view',['id'=>$id]);
    }



}

<?php

namespace backend\modules\tutor\controllers;

/**
* This is the class for controller "MidTesController".
*/
class MidTesController extends \backend\modules\tutor\controllers\base\MidTesController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

<?php

namespace backend\modules\tutor\controllers;

/**
* This is the class for controller "UnitTesController".
*/
class UnitTesController extends \backend\modules\tutor\controllers\base\UnitTesController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

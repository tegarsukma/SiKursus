<?php

namespace backend\modules\tutor\controllers;

/**
* This is the class for controller "RaportController".
*/
class RaportController extends \backend\modules\tutor\controllers\base\RaportController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

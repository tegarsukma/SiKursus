<?php

namespace backend\modules\tutor\controllers;

/**
* This is the class for controller "MataPelajaranController".
*/
class MataPelajaranController extends \backend\modules\akademik\controllers\base\MataPelajaranController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

<?php

namespace backend\modules\tutor\controllers;

/**
* This is the class for controller "FinalTesController".
*/
class FinalTesController extends \backend\modules\tutor\controllers\base\FinalTesController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

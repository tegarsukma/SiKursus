<?php

namespace backend\modules\tutor\controllers;

/**
* This is the class for controller "DailyTesController".
*/
class DailyTesController extends \backend\modules\tutor\controllers\base\DailyTesController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

<?php

namespace backend\modules\tutor\controllers;

/**
* This is the class for controller "SpeakingTesController".
*/
class SpeakingTesController extends \backend\modules\tutor\controllers\base\SpeakingTesController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

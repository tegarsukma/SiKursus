<?php

namespace backend\modules\tutor\controllers;

/**
* This is the class for controller "PretesController".
*/
class PretesController extends \backend\modules\tutor\controllers\base\PretesController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

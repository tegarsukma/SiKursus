<?php

namespace backend\modules\tutor\controllers;

/**
* This is the class for controller "LaporanHarianSiswaController".
*/
class LaporanHarianSiswaController extends \backend\modules\tutor\controllers\base\LaporanHarianSiswaController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

<?php

namespace backend\modules\akademik\controllers\api;

/**
* This is the class for REST controller "JadwalPelajaranController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class JadwalPelajaranController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\JadwalPelajaran';
}

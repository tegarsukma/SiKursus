<?php

namespace backend\modules\akademik\controllers\api;

/**
* This is the class for REST controller "LaporanHarianSiswaController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class LaporanHarianSiswaController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\LaporanHarianSiswa';
}

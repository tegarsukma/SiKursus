<?php

namespace backend\modules\akademik\controllers\api;

/**
* This is the class for REST controller "MataPelajaranController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class MataPelajaranController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\MataPelajaran';
}

<?php

namespace backend\modules\akademik\controllers\api;

/**
* This is the class for REST controller "RaportController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class RaportController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Raport';
}

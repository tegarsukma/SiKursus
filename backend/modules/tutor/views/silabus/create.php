<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Silabus $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Silabuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud silabus-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

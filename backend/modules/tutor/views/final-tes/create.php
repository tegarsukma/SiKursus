<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\FinalTes $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'FinalTes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud final-tes-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

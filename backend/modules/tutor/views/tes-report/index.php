<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = "Tes Report"
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Data <?= Yii::t('app', 'UnitTes') ?></h3>
    </div>

    <div class="box-body">
        <div class="table-responsive">
            <?= GridView::widget([
                'layout' => '{summary}{pager}{items}{pager}',
                'dataProvider' => $dataProvider,
                'pager' => [
                    'class' => yii\widgets\LinkPager::className(),
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'                ],
                'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                'headerRowOptions' => ['class'=>'x'],
                'columns' => [

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' =>'{view}',
                        'urlCreator' => function($action, $model, $key, $index) {
                            // using the column name as key, not mapping to 'id' like the standard generator
                            $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                            $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                            return Url::toRoute($params);
                        },
                        'contentOptions' => ['nowrap'=>'nowrap']
                    ],
                    'id_siswa',
                    'nama_lengkap',
                    'tahun_akademik',
                ],
            ]); ?>
        </div>
    </div>
</div>
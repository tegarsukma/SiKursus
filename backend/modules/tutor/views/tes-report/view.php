<?php

use dmstr\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var app\models\SpeakingTes $model
 */
//$copyParams = $model->attributes;

$this->title = Yii::t('app', 'View Tes Report');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'SpeakingTes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'View', 'url' => ['view', 'id' => $id]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="giiant-crud speaking-tes-view">

    <!-- flash message -->
    <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
        <span class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <?= \Yii::$app->session->getFlash('deleteError') ?>
        </span>
    <?php endif; ?>
    <div class="clearfix crud-navigation">

        <!-- menu buttons -->
        <div class='pull-left'>
            <?= Html::a(
                '<span class="fa fa-pencil"></span> ' . 'Edit',
                ['update', 'id' => $id],
                ['class' => 'btn btn-info']) ?>

            <?= Html::a(
                '<span class="fa fa-plus"></span> ' . 'New',
                ['create'],
                ['class' => 'btn btn-success']) ?>
        </div>

        <div class="pull-right">
            <?= Html::a('<span class="fa fa-list"></span> '
                . 'Full list', ['index'], ['class' => 'btn btn-default']) ?>
        </div>

    </div>
    <br clear="all">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Detail </h3>
        </div>

        <div class="box-body">
            <?php
                $pretes = \app\models\Pretes::find()->where(['siswa'=>$id])->one();
                $dailytes = \app\models\DailyTes::find()->where(['siswa'=>$id])->one();
                $unittes = \app\models\UnitTes::find()->where(['siswa'=>$id])->one();
                $speakingtes = \app\models\SpeakingTes::find()->where(['siswa'=>$id])->one();
                $midtes = \app\models\MidTes::find()->where(['siswa'=>$id])->one();
                $finaltes = \app\models\FinalTes::find()->where(['siswa'=>$id])->one();
            ?>
            <?php $this->beginBlock('app\models\SpeakingTes'); ?>

            <table class="table table-bordered">
                <tr>
                    <th>Nama Lengkap</th>
                    <th colspan="8">Daily Tes</th>
                </tr>

                <tr>
                    <td><?=\app\models\Siswa::find($id)->one()->nama_lengkap?></td>
                    <td><?=$dailytes->nilai_1?></td>
                    <td><?=$dailytes->nilai_2?></td>
                    <td><?=$dailytes->nilai_3?></td>
                    <td><?=$dailytes->nilai_4?></td>
                    <td><?=$dailytes->nilai_5?></td>
                    <td><?=$dailytes->nilai_6?></td>
                    <td><?=$dailytes->nilai_7?></td>
                    <td><?=$dailytes->nilai_8?></td>

                </tr>
            </table>
            <br>
            <table class="table table-bordered">
                <tr>
                    <th>Nama Lengkap</th>
                    <th colspan="8">Unit Tes</th>
                </tr>

                <tr>
                    <td><?=\app\models\Siswa::find($id)->one()->nama_lengkap?></td>
                    <td><?=$unittes->nilai_1?></td>
                    <td><?=$unittes->nilai_2?></td>
                    <td><?=$unittes->nilai_3?></td>
                    <td><?=$unittes->nilai_4?></td>
                    <td><?=$unittes->nilai_5?></td>
                    <td><?=$unittes->nilai_6?></td>
                    <td><?=$unittes->nilai_7?></td>
                    <td><?=$unittes->nilai_8?></td>

                </tr>
            </table>
            <br>
            <table class="table table-bordered">
                <tr>
                    <th>Nama Lengkap</th>
                    <th>Pre tes</th>
                    <th colspan="4">Speaking Tes</th>
                    <th>mid Tes</th>
                    <th>Final Tes</th>
                </tr>

                <tr>
                    <td><?=\app\models\Siswa::find($id)->one()->nama_lengkap?></td>
                    <td><?=$pretes->nilai?></td>
                    <td><?=$speakingtes->nilai_1?></td>
                    <td><?=$speakingtes->nilai_2?></td>
                    <td><?=$speakingtes->nilai_3?></td>
                    <td><?=$speakingtes->nilai_4?></td>
                    <td><?=$midtes->nilai?></td>
                    <td><?=$finaltes->nilai?></td>
                </tr>
            </table>
            <hr/>
            <?php $this->endBlock(); ?>



            <?= Tabs::widget(
                [
                    'id' => 'relation-tabs',
                    'encodeLabels' => false,
                    'items' => [[
                        'label' => '<b class=""># ' . $id . '</b>',
                        'content' => $this->blocks['app\models\SpeakingTes'],
                        'active' => true,
                    ],]
                ]
            );
            ?>
        </div>

    </div>


</div>

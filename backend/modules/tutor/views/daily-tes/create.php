<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\DailyTes $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'DailyTes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud daily-tes-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

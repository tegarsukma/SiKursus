<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var app\models\SpeakingTes $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="speaking-tes-form">

    <?php $form = ActiveForm::begin([
    'id' => 'SpeakingTes',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options'=>['enctype'=>'multipart/form-data'],
    ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
        </div>

        <div class="box-body">

            <?= $form->field($model, 'siswa')->widget(\kartik\select2\Select2::className(),[
                'data'=>\yii\helpers\ArrayHelper::map(\app\models\Siswa::find()->all(),'id','nama_lengkap'),
                'options'=>[
                    'prompt'=>'Pilih Siswa'
                ]
            ]) ?>
			<?= $form->field($model, 'nilai_1')->textInput() ?>
			<?= $form->field($model, 'nilai_2')->textInput() ?>
			<?= $form->field($model, 'nilai_3')->textInput() ?>
			<?= $form->field($model, 'nilai_4')->textInput() ?>
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
            '<span class="fa fa-check"></span> ' .
            ($model->isNewRecord ? 'Create' : 'Save'),
            [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success'
            ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>


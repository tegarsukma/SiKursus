<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\SpeakingTes $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'SpeakingTes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud speaking-tes-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

<?php

use dmstr\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
* @var yii\web\View $this
* @var app\models\MataPelajaran $model
*/
$copyParams = $model->attributes;

$this->title = Yii::t('app', 'MataPelajaran');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MataPelajarans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="giiant-crud mata-pelajaran-view">

    <!-- flash message -->
    <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
        <span class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <?= \Yii::$app->session->getFlash('deleteError') ?>
        </span>
    <?php endif; ?>
    <div class="clearfix crud-navigation">

        <!-- menu buttons -->
        <div class='pull-left'>
            <?= Html::a(
            '<span class="fa fa-pencil"></span> ' . 'Edit',
            [ 'update', 'id' => $model->id],
            ['class' => 'btn btn-info']) ?>

            <?= Html::a(
            '<span class="fa fa-plus"></span> ' . 'New',
            ['create'],
            ['class' => 'btn btn-success']) ?>
        </div>

        <div class="pull-right">
            <?= Html::a('<span class="fa fa-list"></span> '
            . 'Full list', ['index'], ['class'=>'btn btn-default']) ?>
        </div>

    </div><br  clear="all">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Detail </h3>
        </div>

        <div class="box-body">
            <?php $this->beginBlock('app\models\MataPelajaran'); ?>

            
            <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                    'id',
        'mata_pelajaran',
            ],
            ]); ?>

            
            <hr/>

            <?= Html::a('<span class="fa fa-trash"></span> ' . 'Delete', ['delete', 'id' => $model->id],
            [
            'class' => 'btn btn-danger',
            'data-confirm' => '' . 'Are you sure to delete this item?' . '',
            'data-method' => 'post',
            ]); ?>
            <?php $this->endBlock(); ?>


            
<?php $this->beginBlock('JadwalPelajarans'); ?>
<div style='position: relative'><div style='position:absolute; right: 0px; top: 0px;'>
  <?= Html::a(
            '<span class="fa fa-list"></span> ' . 'List All' . ' Jadwal Pelajarans',
            ['jadwal-pelajaran/index'],
            ['class'=>'btn text-muted btn-xs']
        ) ?>
  <?= Html::a(
            '<span class="fa fa-plus"></span> ' . 'New' . ' Jadwal Pelajaran',
            ['jadwal-pelajaran/create', 'JadwalPelajaran' => ['mata_pelajaran' => $model->id]],
            ['class'=>'btn btn-success btn-xs']
        ); ?>
</div></div><?php Pjax::begin(['id'=>'pjax-JadwalPelajarans', 'enableReplaceState'=> false, 'linkSelector'=>'#pjax-JadwalPelajarans ul.pagination a, th a', 'clientOptions' => ['pjax:success'=>'function(){alert("yo")}']]) ?>
<?= '<div class="table-responsive">' . \yii\grid\GridView::widget([
    'layout' => '{summary}{pager}<br/>{items}{pager}',
    'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getJadwalPelajarans(), 'pagination' => ['pageSize' => 20, 'pageParam'=>'page-jadwalpelajarans']]),
    'pager'        => [
        'class'          => yii\widgets\LinkPager::className(),
        'firstPageLabel' => 'First',
        'lastPageLabel'  => 'Last'
    ],
    'columns' => [[
    'class'      => 'yii\grid\ActionColumn',
    'template'   => '{view} {update}',
    'contentOptions' => ['nowrap'=>'nowrap'],
    'urlCreator' => function ($action, $model, $key, $index) {
        // using the column name as key, not mapping to 'id' like the standard generator
        $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
        $params[0] = 'jadwal-pelajaran' . '/' . $action;
        return $params;
    },
    'buttons'    => [
        
    ],
    'controller' => 'jadwal-pelajaran'
],
        'id',
        'kelas',
        'hari',
]
]) . '</div>' ?>
<?php Pjax::end() ?>
<?php $this->endBlock() ?>


<?php $this->beginBlock('Silabuses'); ?>
<div style='position: relative'><div style='position:absolute; right: 0px; top: 0px;'>
  <?= Html::a(
            '<span class="fa fa-list"></span> ' . 'List All' . ' Silabuses',
            ['silabus/index'],
            ['class'=>'btn text-muted btn-xs']
        ) ?>
  <?= Html::a(
            '<span class="fa fa-plus"></span> ' . 'New' . ' Silabus',
            ['silabus/create', 'Silabus' => ['mata_pelajaran' => $model->id]],
            ['class'=>'btn btn-success btn-xs']
        ); ?>
</div></div><?php Pjax::begin(['id'=>'pjax-Silabuses', 'enableReplaceState'=> false, 'linkSelector'=>'#pjax-Silabuses ul.pagination a, th a', 'clientOptions' => ['pjax:success'=>'function(){alert("yo")}']]) ?>
<?= '<div class="table-responsive">' . \yii\grid\GridView::widget([
    'layout' => '{summary}{pager}<br/>{items}{pager}',
    'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getSilabuses(), 'pagination' => ['pageSize' => 20, 'pageParam'=>'page-silabuses']]),
    'pager'        => [
        'class'          => yii\widgets\LinkPager::className(),
        'firstPageLabel' => 'First',
        'lastPageLabel'  => 'Last'
    ],
    'columns' => [[
    'class'      => 'yii\grid\ActionColumn',
    'template'   => '{view} {update}',
    'contentOptions' => ['nowrap'=>'nowrap'],
    'urlCreator' => function ($action, $model, $key, $index) {
        // using the column name as key, not mapping to 'id' like the standard generator
        $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
        $params[0] = 'silabus' . '/' . $action;
        return $params;
    },
    'buttons'    => [
        
    ],
    'controller' => 'silabus'
],
        'id',
        'standar_kompetensi:ntext',
        'kompetensi_dasar:ntext',
        'hasil_belajar:ntext',
        'indokator_hasil_belajar:ntext',
        'materi_pokok:ntext',
        'alokasi_waktu',
]
]) . '</div>' ?>
<?php Pjax::end() ?>
<?php $this->endBlock() ?>


            <?= Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<b class=""># '.$model->id.'</b>',
    'content' => $this->blocks['app\models\MataPelajaran'],
    'active'  => true,
],[
    'content' => $this->blocks['JadwalPelajarans'],
    'label'   => '<small>Jadwal Pelajarans <span class="badge badge-default">'.count($model->getJadwalPelajarans()->asArray()->all()).'</span></small>',
    'active'  => false,
],[
    'content' => $this->blocks['Silabuses'],
    'label'   => '<small>Silabuses <span class="badge badge-default">'.count($model->getSilabuses()->asArray()->all()).'</span></small>',
    'active'  => false,
], ]
                 ]
    );
    ?>        </div>

    </div>


</div>

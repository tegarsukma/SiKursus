<?php

use dmstr\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var app\models\JurnalHarian $model
 */
$copyParams = $model->attributes;

$this->title = Yii::t('app', 'JurnalHarian');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'JurnalHarians'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="giiant-crud jurnal-harian-view">

    <!-- flash message -->
    <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
        <span class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <?= \Yii::$app->session->getFlash('deleteError') ?>
        </span>
    <?php endif; ?>
    <div class="clearfix crud-navigation">

        <!-- menu buttons -->
        <div class='pull-left'>
            <?= Html::a(
                '<span class="fa fa-pencil"></span> ' . 'Edit',
                ['update', 'id' => $model->id],
                ['class' => 'btn btn-info']) ?>

            <?= Html::a(
                '<span class="fa fa-plus"></span> ' . 'New',
                ['create'],
                ['class' => 'btn btn-success']) ?>
        </div>

        <div class="pull-right">
            <?= Html::a('<span class="fa fa-list"></span> '
                . 'Full list', ['index'], ['class' => 'btn btn-default']) ?>
        </div>

    </div>
    <br clear="all">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Detail </h3>
        </div>

        <div class="box-body">
            <?php $this->beginBlock('app\models\JurnalHarian'); ?>


            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'nama_pengajar',
                    'pukul',
                    'program',
                    'level',
                    'pertemuan_ke',
                    'jumlah_siswa',
                    'jumlah_hadir',
                    'jumlah_absen',
                    'materi:ntext',
                    'uraian:ntext',
                    'media:ntext',
                    'tambahan:ntext',
                    'pembukaan:ntext',
                    'inti:ntext',
                    'penutupan:ntext',
                ],
            ]); ?>


            <hr/>

            <?= Html::a('<span class="fa fa-trash"></span> ' . 'Delete', ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'data-confirm' => '' . 'Are you sure to delete this item?' . '',
                    'data-method' => 'post',
                ]); ?>
            <?php $this->endBlock(); ?>

            <?php $this->beginBlock("siswa") ?>
            <?= GridView::widget([
                'layout' => '{summary}{pager}{items}{pager}',
                'dataProvider' => $dataProvider,
                'pager' => [
                    'class' => yii\widgets\LinkPager::className(),
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'],
                'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                'headerRowOptions' => ['class' => 'x'],
                'columns' => [
                    [
                        'attribute'=>'siswa',
                        'value'=>function($data){
                            return \app\models\Siswa::find($data->id)->one()->nama_lengkap;
                        }
                    ],
                    'catatan:ntext',
                ],
            ]); ?>
            <?php $this->endBlock() ?>

            <?= Tabs::widget(
                [
                    'id' => 'relation-tabs',
                    'encodeLabels' => false,
                    'items' => [
                        [
                            'label' => '<b class=""># ' . $model->id . '</b>',
                            'content' => $this->blocks['app\models\JurnalHarian'],
                            'active' => true,
                        ],
                        [
                            'label' => '<b class=""> Catatan Siswa</b>',
                            'content' => $this->blocks['siswa'],
                        ]
                    ]
                ]
            );
            ?>        </div>

    </div>


</div>

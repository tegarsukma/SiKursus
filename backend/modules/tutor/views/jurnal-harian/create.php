<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\JurnalHarian $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'JurnalHarians'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud jurnal-harian-create">

    <?= $this->render('_form', [
    'model' => $model,
        'modelSiswa'=>$modelSiswa

    ]); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\UnitTesSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="unit-tes-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'siswa') ?>

		<?= $form->field($model, 'pelajaran') ?>

		<?= $form->field($model, 'nilai_1') ?>

		<?= $form->field($model, 'nilai_2') ?>

		<?php // echo $form->field($model, 'nilai_3') ?>

		<?php // echo $form->field($model, 'nilai_4') ?>

		<?php // echo $form->field($model, 'nilai_5') ?>

		<?php // echo $form->field($model, 'nilai_6') ?>

		<?php // echo $form->field($model, 'nilai_7') ?>

		<?php // echo $form->field($model, 'nilai_8') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\UnitTes $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'UnitTes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud unit-tes-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

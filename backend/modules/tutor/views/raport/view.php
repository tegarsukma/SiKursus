<?php

use dmstr\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var app\models\Raport $model
 */
$copyParams = $model->attributes;

$this->title = Yii::t('app', 'Raport');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Raports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="giiant-crud raport-view">

    <!-- flash message -->
    <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
        <span class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <?= \Yii::$app->session->getFlash('deleteError') ?>
        </span>
    <?php endif; ?>
    <div class="clearfix crud-navigation">

        <!-- menu buttons -->
        <div class='pull-left'>
            <?= Html::a(
                '<span class="fa fa-pencil"></span> ' . 'Edit',
                ['update', 'id' => $model->id],
                ['class' => 'btn btn-info']) ?>

            <?= Html::a(
                '<span class="fa fa-plus"></span> ' . 'New',
                ['create'],
                ['class' => 'btn btn-success']) ?>
        </div>

        <div class="pull-right">
            <?= Html::a('<span class="fa fa-list"></span> '
                . 'Full list', ['index'], ['class' => 'btn btn-default']) ?>
        </div>

    </div>
    <br clear="all">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Detail </h3>
        </div>

        <div class="box-body">
            <?php $this->beginBlock('app\models\Raport'); ?>

            <h3>ACTIVITIES AT ILF COURSE</h3>
            <p>
                Name : <?= \app\models\Siswa::find($model->siswa)->one()->nama_lengkap ?>
            </p>
            <table class="table table-bordered table-hover" align="">
                <tr align="">
                    <th rowspan="2" align="center" valign="center">No</th>
                    <th rowspan="2" align="center" valign="center">Aspek</th>
                    <th colspan="5">Value</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                </tr>

                <tr>
                    <td></td>
                    <td><b>Kenowledge (Pengetahuan) </b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <?php
                $field_a = ['vocabulary', 'fluency', 'accuracy', 'pronunciation', 'intonation', 'understanding', 'diction'];
                foreach ($field_a as $k => $f) {
                    echo "<tr>";
                    $d = $k + 1;
                    echo "<td>" . str_replace("_"," ",$d) . "</td>";
                    echo "<td>$f</td>";
                    $a = \app\models\Raport::find()->asArray()->where(['id' => $model->id])->one();
                    for ($i = 1; $i <= 5; $i++) {
                        if ($a[$f] == $i) {
                            echo "<td>√</td>";
                        } else {
                            echo "<td></td>";
                        }

                    }
                    echo "</tr>";
                }
                ?>

                <tr>
                    <td></td>
                    <td><b>Attitude (Sikap) </b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <?php
                $field_a = ['respect', 'honest', 'care', 'brave', 'convidence', 'communicative', 'sosial_awarnes', 'curiousity', 'team_work'];
                foreach ($field_a as $k => $f) {
                    echo "<tr>";
                    $d = $k + 1;
                    echo "<td>" . str_replace("_"," ",$d) . "</td>";
                    echo "<td>$f</td>";
                    $a = \app\models\Raport::find()->asArray()->where(['id' => $model->id])->one();
                    for ($i = 1; $i <= 5; $i++) {
                        if ($a[$f] == $i) {
                            echo "<td>√</td>";
                        } else {
                            echo "<td></td>";
                        }

                    }
                    echo "</tr>";
                }
                ?>
            </table>

            <hr/>

            <?= Html::a('<span class="fa fa-trash"></span> ' . 'Delete', ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'data-confirm' => '' . 'Are you sure to delete this item?' . '',
                    'data-method' => 'post',
                ]); ?>
            <?php $this->endBlock(); ?>


            <?php $this->beginBlock('home'); ?>

            <table class="table table-bordered table-hover" align="">
                <tr align="">
                    <th rowspan="2" align="center" valign="center">No</th>
                    <th rowspan="2" align="center" valign="center">Aspek</th>
                    <th colspan="2">Value</th>
                </tr>
                <tr>
                    <td>ya</td>
                    <td>tidak</td>
                </tr>

                <?php
                $field_a = ['mengulang_materi', 'mempelajari_materi', 'mengerjakan_tugas', 'memutar_audio', 'bermain_game', 'belajar_online', 'praktek_materi'];
                foreach ($field_a as $k => $f) {
                    echo "<tr>";
                    $d = $k + 1;
                    echo "<td>" . str_replace("_"," ",$d) . "</td>";
                    echo "<td>$f</td>";
                    $a = \app\models\Raport::find()->asArray()->where(['id' => $model->id])->one();
                    for ($i = 1; $i <= 2; $i++) {
                        if($i==1){
                            if ($a[$f] == "ya") {
                                echo "<td>√</td>";
                            } else {
                                echo "<td></td>";
                            }
                        } else {
                            if ($a[$f] == "tidak") {
                                echo "<td>√</td>";
                            } else {
                                echo "<td></td>";
                            }
                        }


                    }
                    echo "</tr>";
                }
                ?>

            </table>
            <?php $this->endBlock(); ?>

            <?= Tabs::widget(
                [
                    'id' => 'relation-tabs',
                    'encodeLabels' => false,
                    'items' =>
                        [
                            [
                                'label' => '<b class="">#1</b>',
                                'content' => $this->blocks['app\models\Raport'],
                                'active' => true,
                            ],
                            [
                                'label' => '<b class="">#2</b>',
                                'content' => $this->blocks['home'],

                            ],
                    ]
                ]
            );
            ?>        </div>

    </div>


</div>

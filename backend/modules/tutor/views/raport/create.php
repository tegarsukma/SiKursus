<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Raport $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Raports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud raport-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

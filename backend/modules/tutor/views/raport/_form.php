<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var app\models\Raport $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="raport-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Raport',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options'=>['enctype'=>'multipart/form-data'],
    ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
        </div>

        <div class="box-body">
            <div class="col-md-6">
                <?php
                    $dataNilai = [
                        "1"=>'1',"2"=>'2',"3"=>'3',"4"=>'4','5'=>'5'
                    ];

                    $dataNilai2 = [
                            'ya'=>'ya',
                            'tidak'=>'tidak'
                    ]
                ?>
                <?= $form->field($model, 'siswa')->widget(kartik\select2\Select2::className(),[
                        'data'=>\yii\helpers\ArrayHelper::map(\app\models\Siswa::find()->all(),'id','nama_lengkap'),
                        'options'=>[
                                'prompt'=>'Pilih Siswa'
                        ]
                ]) ?>
                <?= $form->field($model, 'vocabulary')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
                <?= $form->field($model, 'fluency')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
                <?= $form->field($model, 'accuracy')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
                <?= $form->field($model, 'pronunciation')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
                <?= $form->field($model, 'intonation')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
                <?= $form->field($model, 'understanding')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>

            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'diction')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
                <?= $form->field($model, 'respect')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
                <?= $form->field($model, 'honest')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
                <?= $form->field($model, 'care')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
                <?= $form->field($model, 'brave')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
                <?= $form->field($model, 'convidence')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
                <?= $form->field($model, 'communicative')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
                <?= $form->field($model, 'sosial_awarnes')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
                <?= $form->field($model, 'curiousity')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
                <?= $form->field($model, 'team_work')->dropDownList($dataNilai,['prompt'=>'Pilih Nilai']) ?>
            </div>

			<?= $form->field($model, 'mengulang_materi')->dropDownList($dataNilai2,['prompt'=>'Pilih Nilai']) ?>
			<?= $form->field($model, 'mempelajari_materi')->dropDownList($dataNilai2,['prompt'=>'Pilih Nilai']) ?>
			<?= $form->field($model, 'mengerjakan_tugas')->dropDownList($dataNilai2,['prompt'=>'Pilih Nilai']) ?>
			<?= $form->field($model, 'memutar_audio')->dropDownList($dataNilai2,['prompt'=>'Pilih Nilai']) ?>
			<?= $form->field($model, 'bermain_game')->dropDownList($dataNilai2,['prompt'=>'Pilih Nilai']) ?>
			<?= $form->field($model, 'belajar_online')->dropDownList($dataNilai2,['prompt'=>'Pilih Nilai']) ?>
			<?= $form->field($model, 'praktek_materi')->dropDownList($dataNilai2,['prompt'=>'Pilih Nilai']) ?>
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
            '<span class="fa fa-check"></span> ' .
            ($model->isNewRecord ? 'Create' : 'Save'),
            [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success'
            ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>


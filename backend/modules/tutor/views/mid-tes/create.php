<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\MidTes $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MidTes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud mid-tes-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\AktifitasTim $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'AktifitasTims'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud aktifitas-tim-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

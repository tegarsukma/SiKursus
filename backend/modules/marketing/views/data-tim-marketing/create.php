<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\DataTimMarketing $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'DataTimMarketings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud data-tim-marketing-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var app\models\DataTimMarketing $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="data-tim-marketing-form">

    <?php $form = ActiveForm::begin([
    'id' => 'DataTimMarketing',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options'=>['enctype'=>'multipart/form-data'],
    ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
        </div>

        <div class="box-body">
			<?= $form->field($model, 'tim')->dropDownList(
			        \yii\helpers\ArrayHelper::map(\app\models\TimMarketing::find()->all(),'id','tim'),
                ['prompt'=>'pilih nama tim']
            ) ?>
			<?= $form->field($model, 'pegawai')->widget(\kartik\select2\Select2::className(),[
			        'data'=>\yii\helpers\ArrayHelper::map(\app\models\Pegawai::find()->all(),'id','nama_lengkap'),
                'options'=>[
                        'prompt'=>'pilih pegawai'
                ]
            ]) ?>
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
            '<span class="fa fa-check"></span> ' .
            ($model->isNewRecord ? 'Create' : 'Save'),
            [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success'
            ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>


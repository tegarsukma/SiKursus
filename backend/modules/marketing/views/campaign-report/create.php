<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\CampaignReport $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'CampaignReports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud campaign-report-create">

    <?= $this->render('_form', [
    'model' => $model,
        'tool'=>$tool
    ]); ?>

</div>

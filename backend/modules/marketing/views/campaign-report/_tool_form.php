<?php
use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;
?>
    <style type="text/css">
        .form-horizontal .form-group{
            margin: 0px 0px!important;
        }
    </style>
<?php DynamicFormWidget::begin([
    'id' => 'dynamic-form' ,
    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
    'widgetBody' => '.container-items', // required: css class selector
    'widgetItem' => '.item', // required: css class
    'limit' => 15, // the maximum times, an element can be cloned (default 999)
    'min' => 1, // 0 or 1 (default 1)
    'insertButton' => '.add-item', // css class
    'deleteButton' => '.remove-item', // css class
    'model' => $tool[0],
    'formId' => 'CampaignReport',
    'formFields' => [
        'tool',
        'jumlah',
        'harga',
    ],
]); ?>
    <h4>Tools</h4>
    <button type="button" class=" add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i>Tambah Data</button>
    <table class="table custom-table red" width="100%">
        <thead>
        <tr>
            <th>Tool</th>
            <th>Jumlah</th>
            <th>Harga</th>
            <th>#</th>
        </tr>
        </thead>

        <tbody class="container-items"><!-- widgetContainer -->
        <?php foreach ($tool as $i => $ba): ?>
            <tr class="item"><!-- widgetBody -->
                <td style="display: none">
                    <?php
                    $blank = [
                        'template' => '<div class=\"\">{input}</div><div class=\"\">{error}</div>'
                    ];
                    if (!$ba->isNewRecord) {
                        echo Html::activeHiddenInput($ba, "[{$i}]id");
                    }
                    echo $i;
                    ?>
                </td>
                <td>
                    <?= $form->field($ba, "[{$i}]tool", $blank)->textInput() ?>
                </td>
                <td>
                    <?= $form->field($ba, "[{$i}]jumlah", $blank)->textInput() ?>
                </td>
                <td>
                    <?= $form->field($ba, "[{$i}]harga", $blank)->textInput() ?>
                </td>
                <td>
                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php DynamicFormWidget::end(); ?>
<?php
$this->registerJs('
    $(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
        console.log("beforeInsert");
    });

    $(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        console.log("afterInsert");
    });

    $(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
        if (! confirm("Are you sure you want to delete this item?")) {
            return false;
        }
        return true;
    });

    $(".dynamicform_wrapper").on("afterDelete", function(e) {
        console.log("Deleted item!");
    });

    $(".dynamicform_wrapper").on("limitReached", function(e, item) {
        alert("Limit reached");
    });

    ');
?>
<div class="site-index">

    <div>
        <div class="jumbotron" style="color: #484848">

            <h1 >Modul <?=Yii::$app->controller->module->id?></h1>
            <p class="lead">SI Kursus Bahasa Inggris</p><br>

        </div>
    </div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data Presentase</h3>
        </div>

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered">

                    <tr>
                        <th>Kategori</th>
                        <th>Target</th>
                        <th>Bobot</th>
                        <th>Realisasi</th>
                        <th>Prosentasi Realisasi</th>
                        <th>Prosentasi  Target</th>

                    </tr>
                    <?php
                        $sum_pr = 0;
                        foreach (\app\models\KategoriAktifitas::find()->all() as $kategori):
                            $goal = \app\models\Aktifitas::find()->where(['kategori'=>$kategori->kategori])->count();
                            $pres = round($goal / $kategori->target * 100);
                            $pr = ceil($pres * $kategori->bobot / 1 / 100);
                            $sum_pr +=$pr;
                    ?>
                        <tr>
                            <td class="text-aqua"><?=$kategori->kategori?></td>
                            <td><?=$kategori->target?></td>
                            <td><?=$kategori->bobot?>%</td>
                            <td><?=$goal?></td>
                            <td><?=$pres?>%</td>
                            <td><?=$pr?>%</td>
                        </tr>
                    <?php endforeach;?>
                    <tr class="text-aqua">
                        <td colspan="5" ><b>Total Presentasi</b></td>
                        <td><b><?=$sum_pr?>%</b></td>
                    </tr>
                </table><br>

                <table class="table table-bordered">

                    <tr>
                        <th>Kategori</th>
                        <th>Target / Tahun</th>
                        <th>Target / Quartal</th>
                        <th>Target / Bulan</th>
                        <th>Bobot</th>
                        <th>Realisasi</th>
                        <th>Prosentase Penyelasaian</th>

                    </tr>
                    <?php
                    $sum_pr = 0;
                    foreach (\app\models\KategoriAktifitas::find()->all() as $kategori):
                        $goal = \app\models\Aktifitas::find()->where(['kategori'=>$kategori->kategori])->count()+4;
                        //$pres = round($goal / $kategori->target * 100);
                        $pr = ceil($goal / $kategori->target_bulan * 100 / 100 * 15 / 100);
                        $sum_pr +=$pr;
                        ?>
                        <tr>
                            <td class="text-aqua"><?=$kategori->kategori?></td>
                            <td><?=$kategori->target_tahun?></td>
                            <td><?=$kategori->target_quartal?></td>
                            <td><?=$kategori->target_bulan?></td>
                            <td><?=$kategori->bobot_akhir?></td>
                            <td><?=$goal?></td>
                            <td><?=$pr?>%</td>
                        </tr>
                    <?php endforeach;?>
                    <tr class="text-aqua">
                        <td colspan="4" ><b>Total Presentasi</b></td>
                        <td>100%</td>
                        <td></td>
                        <td><b><?=$sum_pr?>%</b></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</div>
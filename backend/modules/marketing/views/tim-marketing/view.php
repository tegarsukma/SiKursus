<?php

use dmstr\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var app\models\TimMarketing $model
 */
$copyParams = $model->attributes;

$this->title = Yii::t('app', 'TimMarketing');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'TimMarketings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="giiant-crud tim-marketing-view">

    <!-- flash message -->
    <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
        <span class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <?= \Yii::$app->session->getFlash('deleteError') ?>
        </span>
    <?php endif; ?>
    <div class="clearfix crud-navigation">

        <!-- menu buttons -->
        <div class='pull-left'>
            <?= Html::a(
                '<span class="fa fa-pencil"></span> ' . 'Edit',
                ['update', 'id' => $model->id],
                ['class' => 'btn btn-info']) ?>

            <?= Html::a(
                '<span class="fa fa-plus"></span> ' . 'New',
                ['create'],
                ['class' => 'btn btn-success']) ?>
        </div>

        <div class="pull-right">
            <?= Html::a('<span class="fa fa-list"></span> '
                . 'Full list', ['index'], ['class' => 'btn btn-default']) ?>
        </div>

    </div>
    <br clear="all">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Detail </h3>
        </div>

        <div class="box-body">
            <?php $this->beginBlock('app\models\TimMarketing'); ?>


            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'tim',
                ],
            ]); ?>


            <hr/>

            <?= Html::a('<span class="fa fa-trash"></span> ' . 'Delete', ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'data-confirm' => '' . 'Are you sure to delete this item?' . '',
                    'data-method' => 'post',
                ]); ?>
            <?php $this->endBlock(); ?>

            <?php $this->beginBlock("data-tim")?>
            <?php
                $dataProvider_dt = new \yii\data\ActiveDataProvider([
                        'query'=>\app\models\DataTimMarketing::find()->where(['tim'=>$model->tim])
                    ]);
            ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider_dt,
                'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                'headerRowOptions' => ['class' => 'x'],
                'columns' => [
                    [
                        'attribute'=>'tim',
                        'value'=>function($data){
                            return \app\models\TimMarketing::findOne($data->tim)->tim;
                        }
                    ],
                    [
                        'attribute'=>'pegawai',
                        'value'=>function($data){
                            return \app\models\Pegawai::findOne($data->pegawai)->nama_lengkap;
                        }
                    ],
                ],
            ]); ?>
            <?php $this->endBlock()?>

            <?php $this->beginBlock("aktifitas-tim")?>
            <?php
            $dataProvider = new \yii\data\ActiveDataProvider([
                'query'=>\app\models\AktifitasTim::find()->where(['tim'=>$model->tim])
            ]);
            ?>
            <?= GridView::widget([
                'layout' => '{summary}{pager}{items}{pager}',
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                'headerRowOptions' => ['class' => 'x'],
                'columns' => [
                    [
                        'attribute' => 'tim',
                        'value' => function ($data) {
                            return \app\models\TimMarketing::findOne($data->tim)->tim;
                        }
                    ],
                    [
                        'attribute' => 'aktifitas',
                        'value' => function ($data) {
                            return \app\models\Aktifitas::findOne($data->aktifitas)->judul;
                        }
                    ],
                ],
            ]); ?>
            <?php $this->endBlock()?>

            <?= Tabs::widget(
                [
                    'id' => 'relation-tabs',
                    'encodeLabels' => false,
                    'items' => [[
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['app\models\TimMarketing'],
                        'active' => true,
                    ],[
                        'label' => '<b class="">Data Tim</b>',
                        'content' => $this->blocks['data-tim'],
                    ],[
                        'label' => '<b class="">Aktifitas Tim</b>',
                        'content' => $this->blocks['aktifitas-tim'],
                    ],]
                ]
            );
            ?>        </div>

    </div>


</div>

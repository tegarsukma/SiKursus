<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\TimMarketing $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'TimMarketings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud tim-marketing-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

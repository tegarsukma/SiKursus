<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Aktifitas $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Aktifitas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud aktifitas-create">

    <?= $this->render('_form', [
    'model' => $model,
        'tool'=>$tool
    ]); ?>

</div>

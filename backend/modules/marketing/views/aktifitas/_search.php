<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\AktifitasSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="aktifitas-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'judul') ?>

		<?= $form->field($model, 'keterangan') ?>

		<?= $form->field($model, 'status_aktifitas') ?>

		<?= $form->field($model, 'tanggal_mulai') ?>

		<?php // echo $form->field($model, 'tanggal_selesai') ?>

		<?php // echo $form->field($model, 'waktu_mulai') ?>

		<?php // echo $form->field($model, 'waktu_berakhir') ?>

		<?php // echo $form->field($model, 'pic') ?>

		<?php // echo $form->field($model, 'profil_market') ?>

		<?php // echo $form->field($model, 'target') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

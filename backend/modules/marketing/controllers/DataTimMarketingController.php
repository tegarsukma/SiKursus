<?php

namespace backend\modules\marketing\controllers;

/**
* This is the class for controller "DataTimMarketingController".
*/
class DataTimMarketingController extends \backend\modules\marketing\controllers\base\DataTimMarketingController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

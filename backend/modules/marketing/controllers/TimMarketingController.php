<?php

namespace backend\modules\marketing\controllers;

/**
* This is the class for controller "TimMarketingController".
*/
class TimMarketingController extends \backend\modules\marketing\controllers\base\TimMarketingController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

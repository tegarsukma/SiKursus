<?php

namespace backend\modules\marketing\controllers;

use yii\web\Controller;

/**
 * Default controller for the `marketing` module
 */
class DefaultController extends Controller
{
  public function beforeAction($action)
{
   $this->layout = 'main'; //your layout name
   return parent::beforeAction($action);
}
  //$layout = "main.php";
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
    //  $this->layout = "main";
        return $this->render('index');
    }
}

<?php

namespace backend\modules\marketing\controllers;

/**
* This is the class for controller "MarketerReportController".
*/
class MarketerReportController extends \backend\modules\marketing\controllers\base\MarketerReportController
{   
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

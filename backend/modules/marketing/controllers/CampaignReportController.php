<?php

namespace backend\modules\marketing\controllers;

/**
* This is the class for controller "CampaignReportController".
*/
class CampaignReportController extends \backend\modules\marketing\controllers\base\CampaignReportController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

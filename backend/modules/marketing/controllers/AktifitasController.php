<?php

namespace backend\modules\marketing\controllers;

/**
* This is the class for controller "AktifitasController".
*/
class AktifitasController extends \backend\modules\marketing\controllers\base\AktifitasController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

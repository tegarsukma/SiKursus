<?php

namespace backend\modules\marketing\controllers\api;

/**
* This is the class for REST controller "DataTimMarketingController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class DataTimMarketingController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\DataTimMarketing';
}

<?php

namespace backend\modules\marketing\controllers\api;

/**
* This is the class for REST controller "MarketerReportController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class MarketerReportController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\MarketerReport';
}

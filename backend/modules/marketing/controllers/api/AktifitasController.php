<?php

namespace backend\modules\marketing\controllers\api;

/**
* This is the class for REST controller "AktifitasController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class AktifitasController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Aktifitas';
}

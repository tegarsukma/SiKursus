<?php

namespace backend\modules\marketing\controllers\api;

/**
* This is the class for REST controller "TimMarketingController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class TimMarketingController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\TimMarketing';
}

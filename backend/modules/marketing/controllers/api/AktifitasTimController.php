<?php

namespace backend\modules\marketing\controllers\api;

/**
* This is the class for REST controller "AktifitasTimController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class AktifitasTimController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\AktifitasTim';
}

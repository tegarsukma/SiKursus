<?php

namespace backend\modules\marketing\controllers\api;

/**
* This is the class for REST controller "CampaignReportController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class CampaignReportController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\CampaignReport';
}

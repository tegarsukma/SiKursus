<?php

namespace backend\modules\marketing\controllers;

/**
* This is the class for controller "AktifitasTimController".
*/
class AktifitasTimController extends \backend\modules\marketing\controllers\base\AktifitasTimController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

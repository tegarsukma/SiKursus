<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\MataPelajaran $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MataPelajarans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud mata-pelajaran-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

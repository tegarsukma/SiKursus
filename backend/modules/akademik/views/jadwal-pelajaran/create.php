<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\JadwalPelajaran $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'JadwalPelajarans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud jadwal-pelajaran-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

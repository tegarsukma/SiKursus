<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var app\models\JurnalHarian $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="jurnal-harian-form">

    <?php $form = ActiveForm::begin([
    'id' => 'JurnalHarian',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options'=>['enctype'=>'multipart/form-data'],
    ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
        </div>

        <div class="box-body">
			<?= $form->field($model, 'nama_pengajar')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'pukul')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'program')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'level')->textInput() ?>
			<?= $form->field($model, 'pertemuan_ke')->textInput() ?>
			<?= $form->field($model, 'jumlah_siswa')->textInput() ?>
			<?= $form->field($model, 'jumlah_hadir')->textInput() ?>
			<?= $form->field($model, 'jumlah_absen')->textInput() ?>
			<?= $form->field($model, 'materi')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'uraian')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'media')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'tambahan')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'pembukaan')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'inti')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'penutupan')->textarea(['rows' => 6]) ?>

            <?php if(isset($modelSiswa)):?>
            <?=$this->render("_siswa_form", [ 'modelSiswa'=>$modelSiswa, 'form'=>$form]);?>
            <?php endif;?>
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
            '<span class="fa fa-check"></span> ' .
            ($model->isNewRecord ? 'Create' : 'Save'),
            [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success'
            ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>


<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\JurnalHarianSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="jurnal-harian-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'nama_pengajar') ?>

		<?= $form->field($model, 'pukul') ?>

		<?= $form->field($model, 'program') ?>

		<?= $form->field($model, 'level') ?>

		<?php // echo $form->field($model, 'pertemuan_ke') ?>

		<?php // echo $form->field($model, 'jumlah_siswa') ?>

		<?php // echo $form->field($model, 'jumlah_hadir') ?>

		<?php // echo $form->field($model, 'jumlah_absen') ?>

		<?php // echo $form->field($model, 'materi') ?>

		<?php // echo $form->field($model, 'uraian') ?>

		<?php // echo $form->field($model, 'media') ?>

		<?php // echo $form->field($model, 'tambahan') ?>

		<?php // echo $form->field($model, 'pembukaan') ?>

		<?php // echo $form->field($model, 'inti') ?>

		<?php // echo $form->field($model, 'penutupan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\RaportSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="raport-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'siswa') ?>

		<?= $form->field($model, 'vocabulary') ?>

		<?= $form->field($model, 'fluency') ?>

		<?= $form->field($model, 'accuracy') ?>

		<?php // echo $form->field($model, 'pronunciation') ?>

		<?php // echo $form->field($model, 'intonation') ?>

		<?php // echo $form->field($model, 'understanding') ?>

		<?php // echo $form->field($model, 'diction') ?>

		<?php // echo $form->field($model, 'respect') ?>

		<?php // echo $form->field($model, 'honest') ?>

		<?php // echo $form->field($model, 'care') ?>

		<?php // echo $form->field($model, 'brave') ?>

		<?php // echo $form->field($model, 'convidence') ?>

		<?php // echo $form->field($model, 'communicative') ?>

		<?php // echo $form->field($model, 'sosial_awarnes') ?>

		<?php // echo $form->field($model, 'curiousity') ?>

		<?php // echo $form->field($model, 'team_work') ?>

		<?php // echo $form->field($model, 'mengulang_materi') ?>

		<?php // echo $form->field($model, 'mempelajari_materi') ?>

		<?php // echo $form->field($model, 'mengerjakan_tugas') ?>

		<?php // echo $form->field($model, 'memutar_audio') ?>

		<?php // echo $form->field($model, 'bermain_game') ?>

		<?php // echo $form->field($model, 'belajar_online') ?>

		<?php // echo $form->field($model, 'praktek_materi') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\SilabusSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="silabus-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'mata_pelajaran') ?>

		<?= $form->field($model, 'standar_kompetensi') ?>

		<?= $form->field($model, 'kompetensi_dasar') ?>

		<?= $form->field($model, 'hasil_belajar') ?>

		<?php // echo $form->field($model, 'indokator_hasil_belajar') ?>

		<?php // echo $form->field($model, 'materi_pokok') ?>

		<?php // echo $form->field($model, 'alokasi_waktu') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=Yii::getAlias('@web')?>/img/user.png" width="20px" height="auto" class="img-circle" alt="User Image"/>

            </div>
            <div class="pull-left info">
                <p><?=Yii::$app->user->identity->username?></p>

                <?php
                $role = \app\models\User::findOne(Yii::$app->user->id)->role
                ?>

                <a href="#"><i class="fa fa-circle text-success"></i> Online (<?=\app\models\Role::findOne($role)->role?>)</a>

            </div>
        </div>

        <ul class="sidebar-menu">
            <li class="header">MENU UTAMA</li>
        </ul>
        <?php

        use hscstudio\mimin\components\Mimin;
        $menuItems = backend\components\SidebarMenu::getMenu(5);

        //$menuItems = Mimin::filterMenu($menuItems);

        ?>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => $menuItems,
            ]
        ) ?>

    </section>

</aside>

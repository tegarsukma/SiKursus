<?php

namespace backend\modules\akademik\controllers;

/**
* This is the class for controller "JadwalPelajaranController".
*/
class JadwalPelajaranController extends \backend\modules\akademik\controllers\base\JadwalPelajaranController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

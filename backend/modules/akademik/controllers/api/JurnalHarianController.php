<?php

namespace backend\modules\akademik\controllers\api;

/**
* This is the class for REST controller "JurnalHarianController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class JurnalHarianController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\JurnalHarian';
}

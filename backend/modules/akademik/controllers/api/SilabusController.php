<?php

namespace backend\modules\akademik\controllers\api;

/**
* This is the class for REST controller "SilabusController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class SilabusController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Silabus';
}

<?php

namespace backend\modules\akademik\controllers;

/**
* This is the class for controller "RaportController".
*/
class RaportController extends \backend\modules\akademik\controllers\base\RaportController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

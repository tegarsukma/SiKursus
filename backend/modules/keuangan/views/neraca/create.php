<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Neraca $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Neracas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud neraca-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

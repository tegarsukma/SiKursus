<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var app\models\Neraca $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="neraca-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Neraca',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options'=>['enctype'=>'multipart/form-data'],
    ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
        </div>

        <div class="box-body">
			<?= $form->field($model, 'keterangan')->widget(\kartik\select2\Select2::className(),[
			        'data'=>\yii\helpers\ArrayHelper::map(\app\models\Akun::find()->all(),'akun','akun'),
                'options'=>['prompt'=>'Pilih Akun']

            ]) ?>
			<?= $form->field($model, 'status')->dropDownList(
			        ['Aktiva'=>'Aktifa','Pasiva'=>'Pasiva'],
                ['prompt'=>'Pilih Status']) ?>
			<?= $form->field($model, 'jumlah')->textInput() ?>
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
            '<span class="fa fa-check"></span> ' .
            ($model->isNewRecord ? 'Create' : 'Save'),
            [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success'
            ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>


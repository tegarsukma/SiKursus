<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\PembayaranSiswa $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'PembayaranSiswas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud pembayaran-siswa-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var app\models\PembayaranSiswa $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="pembayaran-siswa-form">

    <?php $form = ActiveForm::begin([
    'id' => 'PembayaranSiswa',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options'=>['enctype'=>'multipart/form-data'],
    ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
        </div>

        <div class="box-body">

            <?= $form->field($model, 'siswa')->widget(\kartik\select2\Select2::className(),[
                'data'=>\yii\helpers\ArrayHelper::map(app\models\Siswa::find()->all(), 'id', 'nama_lengkap'),
                'options'=>[
                    'prompt'=>'Pilih Siswa',
                ]
            ]) ?>
			<?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'angsuran_ke')->textInput() ?>
			<?= $form->field($model, 'jumlah')->textInput() ?>
            <?= $form->field($model, 'tahun')->textInput(['readonly'=>true,'value'=>date('Y')]) ?>
            <?= $form->field($model, 'bulan')->textInput(['readonly'=>true,'value'=>date('F')]) ?>
            <?= $form->field($model, 'tanggal')->textInput(['readonly'=>true,'value'=>date('d-M-Y')]) ?>
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
            '<span class="fa fa-check"></span> ' .
            ($model->isNewRecord ? 'Create' : 'Save'),
            [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success'
            ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>
<script>
    $("#pembayaransiswa-siswa").change(function () {
        var idsiswa = $("#pembayaransiswa-siswa").val();
        //alert('haha');

        $.ajax({
            url:'<?=\yii\helpers\Url::to(['get-angsuran'])?>',
            data:{
                siswa:idsiswa
            },
            type:'GET',
            success:function (data) {
                $("#pembayaransiswa-jumlah").val(data);
            }
        })
    })
</script>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\JurnalUmumSearch $searchModel
 */

$this->title = 'JurnalUmums';
$this->params['breadcrumbs'][] = $this->title;

if (isset($actionColumnTemplates)) {
    $actionColumnTemplate = implode(' ', $actionColumnTemplates);
    $actionColumnTemplateString = $actionColumnTemplate;
} else {
    Yii::$app->view->params['pageButtons'] = Html::a('<span
    class="fa fa-plus"></span> ' . 'New', ['create'], ['class' => 'btn btn-success']);
    $actionColumnTemplateString = "{view} {update} {delete}";
}

?>
<div class="giiant-crud jurnal-umum-index">

    <?php //             echo $this->render('_search', ['model' =>$searchModel]);
    ?>


    <?php \yii\widgets\Pjax::begin(['id' => 'pjax-main', 'enableReplaceState' => false, 'linkSelector' => '#pjax-main ul.pagination a, th a', 'clientOptions' => ['pjax:success' => 'function(){alert("yo")}']]) ?>


    <div class="clearfix"></div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data <?= Yii::t('app', 'JurnalUmums') ?></h3>
        </div>

        <div class="box-body">
            <div class="table-responsive">
                <?php foreach (\app\models\Akun::find()->all() as $akun): ?>
                    <table class="table table-bordered">
                        <tr>
                            <th colspan="2">
                                Nama REK : <?=$akun->akun?>
                            </th>
                            <th>
                                <?=$akun->status_akun?>
                            </th>
                            <th colspan="4">
                                Nomor REK : <?=$akun->nomor_akun?>
                            </th>
                        </tr>
                        <tr>
                            <th rowspan="2">TANGGAL</th>
                            <th rowspan="2">KETERANGAN</th>
                            <th rowspan="2">REF</th>
                            <th rowspan="2">DEBET</th>
                            <th rowspan="2">KREDIT</th>
                            <th rowspan="1" colspan="2">SALDO</th>
                        </tr>

                        <tr>
                            <th>Debet</th>
                            <th>Kredit</th>
                        </tr>

                        <tr>
                            <td>Keterangan</td>
                            <td>SALDO AWAL</td>
                            <td>JU-1</td>
                            <td></td>
                            <td></td>
                            <td><?php $sa = \app\models\DaftarAkun::findOne($akun->id)->saldo;
                                    echo "Rp.". number_format($sa,0,"",".");
                                ?></td>
                            <td></td>
                        </tr>
                        <?php
                            $kredit_ju =  \app\models\JurnalUmum::find()->where(['nomor_akun'=>$akun->id,'status_akun'=>'debet'])->sum('saldo');
                            $debet_ju = \app\models\JurnalUmum::find()->where(['nomor_akun'=>$akun->id,'status_akun'=>'kredit'])->sum('saldo');
                        ?>
                        <tr>
                            <td></td>
                            <td>POSTING JURNAL UMUM</td>
                            <td>JU-1</td>
                            <td><?= number_format($debet_ju,0,"",".") ?></td>
                            <td><?= number_format($kredit_ju,0,"",".") ?></td>
                            <td><?= "Rp. ".number_format($sa - $kredit_ju + $debet_ju,0,"",".")?></td>
                            <td>Kredit</td>
                        </tr>


                    </table>
                <?php endforeach; ?>
                <br>
            </div>
        </div>
    </div>


    <?php \yii\widgets\Pjax::end() ?>



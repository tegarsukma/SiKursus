<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Akun $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akuns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud akun-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

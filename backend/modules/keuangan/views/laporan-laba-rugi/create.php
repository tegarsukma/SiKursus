<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\LaporanLabaRugi $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'LaporanLabaRugis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud laporan-laba-rugi-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

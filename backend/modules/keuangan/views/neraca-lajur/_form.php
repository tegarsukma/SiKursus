<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var app\models\NeracaLajur $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="neraca-lajur-form">

    <?php $form = ActiveForm::begin([
    'id' => 'NeracaLajur',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options'=>['enctype'=>'multipart/form-data'],
    ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
        </div>

        <div class="box-body">

			<?= $form->field($model, 'no_akun')->textInput() ?>
			<?= $form->field($model, 'nama_akun')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'neraca_saldo_debet')->textInput() ?>
			<?= $form->field($model, 'neraca_saldo_kredit')->textInput() ?>
			<?= $form->field($model, 'penyesuaian_debet')->textInput() ?>
			<?= $form->field($model, 'penyesuaian_kredit')->textInput() ?>
			<?= $form->field($model, 'ns_disesuaikan_debet')->textInput() ?>
			<?= $form->field($model, 'ns_disesuaikan_kredit')->textInput() ?>
			<?= $form->field($model, 'laba_rugi_debet')->textInput() ?>
			<?= $form->field($model, 'laba_rugi_kredit')->textInput() ?>
			<?= $form->field($model, 'neraca_debet')->textInput() ?>
			<?= $form->field($model, 'neraca_kredit')->textInput() ?>
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
            '<span class="fa fa-check"></span> ' .
            ($model->isNewRecord ? 'Create' : 'Save'),
            [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success'
            ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>


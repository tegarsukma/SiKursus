<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\JurnalUmumSearch $searchModel
 */

$this->title = 'JurnalUmums';
$this->params['breadcrumbs'][] = $this->title;

if (isset($actionColumnTemplates)) {
    $actionColumnTemplate = implode(' ', $actionColumnTemplates);
    $actionColumnTemplateString = $actionColumnTemplate;
} else {
    Yii::$app->view->params['pageButtons'] = Html::a('<span
    class="fa fa-plus"></span> ' . 'New', ['create'], ['class' => 'btn btn-success']);
    $actionColumnTemplateString = "{view} {update} {delete}";
}

?>
<div class="giiant-crud jurnal-umum-index">

    <?php //             echo $this->render('_search', ['model' =>$searchModel]);
    ?>


    <?php \yii\widgets\Pjax::begin(['id'=>'pjax-main', 'enableReplaceState'=> false, 'linkSelector'=>'#pjax-main ul.pagination a, th a', 'clientOptions' => ['pjax:success'=>'function(){alert("yo")}']]) ?>

    <div class="pull-left">
         </div>

    <div class="pull-right">


        <?=
        \yii\bootstrap\ButtonDropdown::widget(
            [
                'id' => 'giiant-relations',
                'encodeLabel' => false,
                'label' => '<span class="fa fa-paperclip"></span> ' . 'Relations',
                'dropdown' => [
                    'options' => [
                        'class' => 'dropdown-menu-right'
                    ],
                    'encodeLabels' => false,
                    'items' => []
                ],
                'options' => [
                    'class' => 'btn-default'
                ]
            ]
        );
        ?>    </div>
</div><br clear="all"><br>

<div class="clearfix"></div>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Data <?= Yii::t('app', 'JurnalUmums') ?></h3>
    </div>

    <div class="box-body">
        <div class="table-responsive">
           <table class="table table-bordered">
               <tr>
                   <th rowspan="2">NO AKUN</th>
                   <th rowspan="2">NAMA AKUN</th>
                   <th colspan="2">NERACA SALDO</th>
                   <th colspan="2">PENYESUAIAN</th>
                   <th colspan="2">N.S DISESUAIKAN</th>
                   <th colspan="2">LABA RUGI</th>
                   <th colspan="2">NERACA</th>
               </tr>
               <tr>
                   <td>DEBET</td>
                   <td>KREDIT</td>
                   <td>DEBET</td>
                   <td>KREDIT</td>
                   <td>DEBET</td>
                   <td>KREDIT</td>
                   <td>DEBET</td>
                   <td>KREDIT</td>
                   <td>DEBET</td>
                   <td>KREDIT</td>

               </tr>

               <?php
                foreach (\app\models\Akun::find()->all() as $akun):

                    $kredit_ju =  \app\models\JurnalUmum::find()->where(['nomor_akun'=>$akun->id,'status_akun'=>'debet'])->sum('saldo');
                    $debet_ju = \app\models\JurnalUmum::find()->where(['nomor_akun'=>$akun->id,'status_akun'=>'kredit'])->sum('saldo');
                    $sa = \app\models\DaftarAkun::findOne($akun->id)->saldo;

                    $jp_saldo = \app\models\JurnalPenyesuaian::find()->where(['nomor_akun'=>$akun->id])->sum('saldo');
                    ?>
                    <tr>
                        <td><?=$akun->nomor_akun?></td>
                        <td><?=$akun->akun?></td>
                        <td>
                            <?php
                            if($akun->status_akun == 'debet'){
                                $n_saldo_debet = $sa + $kredit_ju - $debet_ju;
                                echo "Rp. ".number_format($n_saldo_debet,0,"",".");

                            } else {
                                $n_saldo_debet = 0;
                            }
                            ?>
                        </td>
                        <td><?php
                            if($akun->status_akun == 'kredit'){
                                $n_saldo_kredit = $sa + $kredit_ju - $debet_ju;
                                echo "Rp".number_format($n_saldo_kredit + $kredit_ju - $debet_ju,0,"",".");
                            } else {
                                $n_saldo_kredit = 0;
                            }
                            ?></td>
                        <td>
                            <?php
                            if($akun->status_akun == 'debet'){
                                echo "Rp. ". number_format($jp_saldo,0,"",".");
                            }
                            $n_jp_debet = $jp_saldo;
                            ?>
                        </td>
                        <td><?php
                            if($akun->status_akun == 'kredit'){

                                echo "Rp. ". number_format($jp_saldo,0,"",".");
                            }
                            $n_jp_kredit = $jp_saldo;
                            ?>
                        </td>
                        <td><?php
                            if($akun->status_akun == 'debet'){
                                 $ns = $n_saldo_debet + $n_jp_debet - $n_saldo_kredit + $n_jp_kredit;
                                echo "Rp. ". number_format($ns,0,"",".");
                            }
                            ?>
                        </td>
                        <td><?php
                            if($akun->status_akun == 'kredit'){
                                $ns = $n_saldo_debet + $n_jp_debet - $n_saldo_kredit + $n_jp_kredit;
                                echo "Rp. ". number_format($ns,0,"",".");
                            }
                            ?>
                        </td>
                        <td><?php
                            if($akun->status_akun == 'debet'){
                                echo "Rp. ". number_format($ns,0,"",".");
                            }
                            ?></td>
                        <td>
                            <?php
                            if($akun->status_akun == 'kredit'){

                                echo "Rp. ". number_format($ns,0,"",".");
                            }
                            ?>
                        </td>
                        <td><?php
                            if($akun->status_akun == 'debet'){
                                echo "Rp. ". number_format($ns,0,"",".");
                            }
                            ?></td>
                        <td>
                            <?php
                            if($akun->status_akun == 'kredit'){

                                echo "Rp. ". number_format($ns,0,"",".");
                            }
                            ?>
                        </td>

                    </tr>

               <?php endforeach; ?>
           </table>
        </div>
    </div>
</div>


<?php \yii\widgets\Pjax::end() ?>



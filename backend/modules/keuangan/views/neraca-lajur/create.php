<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\NeracaLajur $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NeracaLajurs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud neraca-lajur-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

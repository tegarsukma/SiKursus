<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\NeracaLajurSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="neraca-lajur-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'no_akun') ?>

		<?= $form->field($model, 'nama_akun') ?>

		<?= $form->field($model, 'neraca_saldo_debet') ?>

		<?= $form->field($model, 'neraca_saldo_kredit') ?>

		<?php // echo $form->field($model, 'penyesuaian_debet') ?>

		<?php // echo $form->field($model, 'penyesuaian_kredit') ?>

		<?php // echo $form->field($model, 'ns_disesuaikan_debet') ?>

		<?php // echo $form->field($model, 'ns_disesuaikan_kredit') ?>

		<?php // echo $form->field($model, 'laba_rugi_debet') ?>

		<?php // echo $form->field($model, 'laba_rugi_kredit') ?>

		<?php // echo $form->field($model, 'neraca_debet') ?>

		<?php // echo $form->field($model, 'neraca_kredit') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

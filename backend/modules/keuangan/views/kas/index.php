<?php
/* @var $this yii\web\View */
?>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-sign-in"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Pemasukan</span>
                <span class="info-box-number">
                        Rp.<?=number_format(\app\models\PembayaranSiswa::find()->sum('jumlah'),0,"",".")?><br>
                    <small>Total Pemasukan</small></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-paper-plane"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Pengeluaran</span>
                <span class="info-box-number">
                        Rp.<?=number_format(\app\models\Pengeluaran::find()->sum('jumlah'),0,"",".")?><br>
                    <small>Total Pengeluaran</small>
                    </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-sign-out"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Keuangan</span>
                <span class="info-box-number">
                    <?php
                    $a = \app\models\PembayaranSiswa::find()->sum('jumlah');
                    $b = \app\models\Pengeluaran::find()->sum('jumlah');
                    ?>
                        <?=number_format($a-$b,0,'','.')?><br>
                    <small>Jumlah Keuangan</small>
                </span>

            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>


    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-edit"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Activity</span>
                <span class="info-box-number">
                        <?=\app\models\Aktifitas::find()->count()?>
                    </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Chart</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="chart">
                    <?php
                    use app\models\Pengeluaran;
                    use miloschuman\highcharts\Highcharts;
                    $month=['January','February','March','April','May','June','July','June','August','September','October','November','December'];
                    $data = [];
                    for($i=0;$i<12;$i++){
                        $dat = Pengeluaran::find()->where(['bulan'=>$month[$i],'tahun'=>date('Y')])->count();
                        if($dat == 1){
                            $a = Pengeluaran::find()->where(['bulan'=>$month[$i],'tahun'=>date('Y')])->one();
                            $data[] = $a->jumlah;
                        } else {
                            $data[] = 0;
                        }
                        //echo $month[$i]."-".$dat."<br>";
                    }

                    echo Highcharts::widget([
                        'options' => [
                            'title' => ['text' => 'Pengeluaran'],
                            'xAxis' => [
                                'categories' => $month
                            ],
                            'yAxis' => [
                                'title' => ['text' => 'Jumlah Pengeluaran']
                            ],
                            'series' => [
                                ['data' => $data],
                            ]
                        ]
                    ]);
                    ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
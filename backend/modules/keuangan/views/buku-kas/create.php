<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\BukuKas $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'BukuKas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud buku-kas-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

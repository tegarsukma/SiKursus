<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\BiayaProgramSiswa $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'BiayaProgramSiswas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud biaya-program-siswa-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

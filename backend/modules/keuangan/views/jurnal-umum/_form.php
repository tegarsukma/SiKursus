<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var app\models\JurnalUmum $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="jurnal-umum-form">

    <?php $form = ActiveForm::begin([
    'id' => 'JurnalUmum',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options'=>['enctype'=>'multipart/form-data'],
    ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
        </div>

        <div class="box-body">
			<?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'nomor_akun')->widget(\kartik\select2\Select2::className(),[
			        'data'=>\yii\helpers\ArrayHelper::map(\app\models\Akun::find()->all(),'id',function($model){
			            return $model->nomor_akun . " - " . $model->akun;
                    })
            ]) ?>
			<?= $form->field($model, 'tanggal')->textInput(['maxlength' => true ,'value'=>date('d-F-Y')]) ?>
            <?= $form->field($model, 'status_akun')->dropDownList(
                ['debet'=>'Debet','kredit'=>'Kredit'],
                ['prompt'=>'Status Akun']
            ) ?>
			<?= $form->field($model, 'saldo')->textInput() ?>
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
            '<span class="fa fa-check"></span> ' .
            ($model->isNewRecord ? 'Create' : 'Save'),
            [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success'
            ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>


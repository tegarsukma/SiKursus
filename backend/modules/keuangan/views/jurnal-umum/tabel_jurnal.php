<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\JurnalUmumSearch $searchModel
 */

$this->title = 'JurnalUmums';
$this->params['breadcrumbs'][] = $this->title;

if (isset($actionColumnTemplates)) {
    $actionColumnTemplate = implode(' ', $actionColumnTemplates);
    $actionColumnTemplateString = $actionColumnTemplate;
} else {
    Yii::$app->view->params['pageButtons'] = Html::a('<span
    class="fa fa-plus"></span> ' . 'New', ['create'], ['class' => 'btn btn-success']);
    $actionColumnTemplateString = "{view} {update} {delete}";
}

?>
<div class="giiant-crud jurnal-umum-index">

    <?php //             echo $this->render('_search', ['model' =>$searchModel]);
    ?>


    <?php \yii\widgets\Pjax::begin(['id'=>'pjax-main', 'enableReplaceState'=> false, 'linkSelector'=>'#pjax-main ul.pagination a, th a', 'clientOptions' => ['pjax:success'=>'function(){alert("yo")}']]) ?>

    <div class="pull-left">
        <?= Html::a('<span class="fa fa-plus"></span> ' . 'New', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('<span class="fa fa-eye"></span> ' . 'Daftar Jurnal', ['list'], ['class' => 'btn btn-primary']) ?>

    </div>

    <div class="pull-right">


        <?=
        \yii\bootstrap\ButtonDropdown::widget(
            [
                'id' => 'giiant-relations',
                'encodeLabel' => false,
                'label' => '<span class="fa fa-paperclip"></span> ' . 'Relations',
                'dropdown' => [
                    'options' => [
                        'class' => 'dropdown-menu-right'
                    ],
                    'encodeLabels' => false,
                    'items' => []
                ],
                'options' => [
                    'class' => 'btn-default'
                ]
            ]
        );
        ?>    </div>
</div><br clear="all"><br>

<div class="clearfix"></div>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Data <?= Yii::t('app', 'JurnalUmums') ?></h3>
    </div>

    <div class="box-body">
        <div class="table-responsive">
           <table class="table table-bordered">
               <tr>
                   <th>Tanggal</th>
                   <th>Keterangan</th>
                   <th>Nomor Akun</th>
                   <th>Debet</th>
                   <th>Kredit</th>
               </tr>

               <?php
                foreach (\app\models\JurnalUmum::find()->all() as $jurnal):
               ?>
               <tr>
                   <td><?=$jurnal->tanggal?></td>
                   <td><?=$jurnal->keterangan?></td>
                   <td><?=\app\models\Akun::findOne($jurnal->nomor_akun)->nomor_akun?></td>
                   <td><?php
                          if($jurnal->status_akun == "debet"){
                              echo "Rp.".number_format($jurnal->saldo,0,"",",");
                          }
                       ?>
                   </td>
                   <td><?php
                       if($jurnal->status_akun == "kredit"){
                           echo "Rp.".number_format($jurnal->saldo,0,"",",");
                       }
                       ?></td>
               </tr>

               <?php endforeach; ?>
           </table>
        </div>
    </div>
</div>


<?php \yii\widgets\Pjax::end() ?>



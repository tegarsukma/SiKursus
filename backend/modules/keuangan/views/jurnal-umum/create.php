<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\JurnalUmum $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'JurnalUmums'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud jurnal-umum-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\JurnalPenyesuaian $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'JurnalPenyesuaians'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud jurnal-penyesuaian-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

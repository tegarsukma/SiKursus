<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\LaporanPerubahanEkuitas $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'LaporanPerubahanEkuitas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud laporan-perubahan-ekuitas-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

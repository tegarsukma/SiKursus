<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\DaftarAkun $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'DaftarAkuns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud daftar-akun-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Pengeluaran $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengeluarans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud pengeluaran-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

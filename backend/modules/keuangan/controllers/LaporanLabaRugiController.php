<?php

namespace backend\modules\keuangan\controllers;

/**
* This is the class for controller "LaporanLabaRugiController".
*/
class LaporanLabaRugiController extends \backend\modules\keuangan\controllers\base\LaporanLabaRugiController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

<?php

namespace backend\modules\keuangan\controllers;

/**
* This is the class for controller "BukuKasController".
*/
class BukuKasController extends \backend\modules\keuangan\controllers\base\BukuKasController
{

    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

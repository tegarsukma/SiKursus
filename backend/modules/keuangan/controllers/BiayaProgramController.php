<?php

namespace backend\modules\keuangan\controllers;

/**
* This is the class for controller "BiayaProgramController".
*/
class BiayaProgramController extends \backend\modules\keuangan\controllers\base\BiayaProgramController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }

}

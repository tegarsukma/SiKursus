<?php

namespace backend\modules\keuangan\controllers\base;

use app\models\Pengeluaran;
use yii\helpers\Json;

class KasController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetChartData(){
        $bln = ['January','February','March','April','May','June','July','August','September','October','November',"December"];
        $json = [];
        for($i=0;$i<12;$i++){
            $b = Pengeluaran::findOne(['bulan'=>$bln[$i]]);
            $json[$i] = [
                    'a'=>$b->jumlah
                ];
        }

        return Json::encode($json);
    }

    public function actionGetChartDataMonth($bulan){
        foreach (Pengeluaran::find()->where(['bulan'=>$bulan])->all() as $k=>$val){
            if(empty($val->jumlah)){
                return 0;
            } else {
                return $val->jumlah;
            }

        }

    }

}

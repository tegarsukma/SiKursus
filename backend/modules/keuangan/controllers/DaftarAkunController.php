<?php

namespace backend\modules\keuangan\controllers;

/**
* This is the class for controller "DaftarAkunController".
*/
class DaftarAkunController extends \backend\modules\keuangan\controllers\base\DaftarAkunController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

<?php

namespace backend\modules\keuangan\controllers;

/**
* This is the class for controller "PembayaranSiswaController".
*/
class PembayaranSiswaController extends \backend\modules\keuangan\controllers\base\PembayaranSiswaController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

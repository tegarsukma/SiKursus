<?php

namespace backend\modules\keuangan\controllers\api;

/**
* This is the class for REST controller "PengeluaranController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class PengeluaranController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Pengeluaran';
}

<?php

namespace backend\modules\keuangan\controllers\api;

/**
* This is the class for REST controller "AkunController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class AkunController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Akun';
}

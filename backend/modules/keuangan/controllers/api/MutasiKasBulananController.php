<?php

namespace backend\modules\keuangan\controllers\api;

/**
* This is the class for REST controller "MutasiKasBulananController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class MutasiKasBulananController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\MutasiKasBulanan';
}

<?php

namespace backend\modules\keuangan\controllers\api;

/**
* This is the class for REST controller "LaporanLabaRugiController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class LaporanLabaRugiController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\LaporanLabaRugi';
}

<?php

namespace backend\modules\keuangan\controllers\api;

/**
* This is the class for REST controller "JurnalPenyesuaianController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class JurnalPenyesuaianController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\JurnalPenyesuaian';
}

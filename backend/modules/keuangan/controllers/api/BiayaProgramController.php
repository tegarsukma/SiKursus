<?php

namespace backend\modules\keuangan\controllers\api;

/**
* This is the class for REST controller "BiayaProgramController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class BiayaProgramController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\BiayaProgram';
}

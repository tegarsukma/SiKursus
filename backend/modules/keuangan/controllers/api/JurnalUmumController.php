<?php

namespace backend\modules\keuangan\controllers\api;

/**
* This is the class for REST controller "JurnalUmumController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class JurnalUmumController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\JurnalUmum';
}

<?php

namespace backend\modules\keuangan\controllers\api;

/**
* This is the class for REST controller "PembayaranSiswaController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class PembayaranSiswaController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\PembayaranSiswa';
}

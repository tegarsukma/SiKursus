<?php

namespace backend\modules\keuangan\controllers\api;

/**
* This is the class for REST controller "LaporanPerubahanEkuitasController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class LaporanPerubahanEkuitasController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\LaporanPerubahanEkuitas';
}

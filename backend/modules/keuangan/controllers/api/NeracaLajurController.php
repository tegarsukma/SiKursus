<?php

namespace backend\modules\keuangan\controllers\api;

/**
* This is the class for REST controller "NeracaLajurController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class NeracaLajurController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\NeracaLajur';
}

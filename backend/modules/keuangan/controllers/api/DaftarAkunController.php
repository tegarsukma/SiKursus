<?php

namespace backend\modules\keuangan\controllers\api;

/**
* This is the class for REST controller "DaftarAkunController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class DaftarAkunController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\DaftarAkun';
}

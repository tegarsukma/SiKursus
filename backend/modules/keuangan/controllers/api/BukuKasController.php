<?php

namespace backend\modules\keuangan\controllers\api;

/**
* This is the class for REST controller "BukuKasController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class BukuKasController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\BukuKas';
}

<?php

namespace backend\modules\keuangan\controllers\api;

/**
* This is the class for REST controller "NeracaController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class NeracaController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Neraca';
}

<?php

namespace backend\modules\keuangan\controllers;

/**
* This is the class for controller "BiayaProgramSiswaController".
*/
class BiayaProgramSiswaController extends \backend\modules\keuangan\controllers\base\BiayaProgramSiswaController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

<?php

namespace backend\modules\keuangan\controllers;

/**
* This is the class for controller "LaporanPerubahanEkuitasController".
*/
class LaporanPerubahanEkuitasController extends \backend\modules\keuangan\controllers\base\LaporanPerubahanEkuitasController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

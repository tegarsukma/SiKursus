<?php

namespace backend\modules\keuangan\controllers;

/**
* This is the class for controller "PengeluaranController".
*/
class PengeluaranController extends \backend\modules\keuangan\controllers\base\PengeluaranController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

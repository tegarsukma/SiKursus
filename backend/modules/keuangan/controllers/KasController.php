<?php

namespace backend\modules\keuangan\controllers;

class KasController extends \backend\modules\keuangan\controllers\base\KasController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

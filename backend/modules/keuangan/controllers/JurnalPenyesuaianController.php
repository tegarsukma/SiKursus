<?php

namespace backend\modules\keuangan\controllers;

/**
* This is the class for controller "JurnalPenyesuaianController".
*/
class JurnalPenyesuaianController extends \backend\modules\keuangan\controllers\base\JurnalPenyesuaianController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

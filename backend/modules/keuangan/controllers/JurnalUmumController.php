<?php

namespace backend\modules\keuangan\controllers;

/**
* This is the class for controller "JurnalUmumController".
*/
class JurnalUmumController extends \backend\modules\keuangan\controllers\base\JurnalUmumController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

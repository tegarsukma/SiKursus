<?php

namespace backend\modules\keuangan\controllers;

/**
* This is the class for controller "NeracaController".
*/
class NeracaController extends \backend\modules\keuangan\controllers\base\NeracaController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

<?php

namespace backend\modules\keuangan\controllers;

/**
* This is the class for controller "AkunController".
*/
class AkunController extends \backend\modules\keuangan\controllers\base\AkunController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

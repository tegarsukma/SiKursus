<?php

namespace backend\modules\keuangan\controllers;

/**
* This is the class for controller "MutasiKasBulananController".
*/
class MutasiKasBulananController extends \backend\modules\keuangan\controllers\base\MutasiKasBulananController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

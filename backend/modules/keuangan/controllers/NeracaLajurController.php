<?php

namespace backend\modules\keuangan\controllers;

/**
* This is the class for controller "NeracaLajurController".
*/
class NeracaLajurController extends \backend\modules\keuangan\controllers\base\NeracaLajurController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var app\models\PengamatanPembelajaran $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="pengamatan-pembelajaran-form">

    <?php $form = ActiveForm::begin([
    'id' => 'PengamatanPembelajaran',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options'=>['enctype'=>'multipart/form-data'],
    ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
        </div>

        <div class="box-body">

            <?php $arr_nilai = [1=>'1',2=>'2',3=>'3',4=>'4']?>
            <?= $form->field($model, 'tutor')->widget(\kartik\select2\Select2::className(),[
                'data'=>\yii\helpers\ArrayHelper::map(\app\models\Tutor::find()->all(),'id','nama_lengkap'),
                'options'=>['prompt'=>'Tutor']
            ]) ?>
            <?= $form->field($model, 'hari')->textInput(['maxlength' => true,'value'=>\backend\components\LocalTime::getLocalDay()]) ?>
            <?= $form->field($model, 'tanggal')->textInput(['maxlength' => true,'value'=>date('d-F-Y')]) ?>
            <?= $form->field($model, 'jam_ke')->textInput() ?>
			<?= $form->field($model, 'p1')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p2')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p3')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p4')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p5')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p6')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p7')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p8')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p9')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p10')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p11')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p12')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p13')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p14')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p15')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p16')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p17')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p18')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p19')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p20')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p21')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p22')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p23')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p24')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
			<?= $form->field($model, 'p25')->inline(true)->radioList($arr_nilai,['style'=>'display:inline']) ?>
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
            '<span class="fa fa-check"></span> ' .
            ($model->isNewRecord ? 'Create' : 'Save'),
            [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success'
            ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>


<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\PengamatanPembelajaran $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'PengamatanPembelajarans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud pengamatan-pembelajaran-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

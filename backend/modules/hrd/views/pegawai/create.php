<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Pegawai $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pegawais'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud pegawai-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var app\models\Pegawai $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="pegawai-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Pegawai',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options'=>['enctype'=>'multipart/form-data'],
    ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
        </div>

        <div class="box-body">
			<?= $form->field($model, 'nama_lengkap')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'tanggal_lahir')->widget(\kartik\date\DatePicker::className(),[
			        'pluginOptions'=>[
			                'format'=>'dd-M-yyyy'
                    ]
            ]) ?>
			<?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'no_telp')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'hobi')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'makanan_favorit')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'warna_favorit')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'motto')->textarea(['rows' => 6]) ?>
            <hr>
			<?= $form->field($model, 'gaji_pokok')->textInput() ?>
			<?= $form->field($model, 'tunjangan')->textInput() ?>
            <?= $form->field($model, 'tunjangan_mengajar')->textInput() ?>
            <?= $form->field($model, 'tunjangan_lain')->textInput() ?>
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
            '<span class="fa fa-check"></span> ' .
            ($model->isNewRecord ? 'Create' : 'Save'),
            [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success'
            ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>


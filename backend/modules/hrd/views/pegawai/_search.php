<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\PegawaiSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="pegawai-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'nama_lengkap') ?>

		<?= $form->field($model, 'tanggal_lahir') ?>

		<?= $form->field($model, 'alamat') ?>

		<?= $form->field($model, 'no_telp') ?>

		<?php // echo $form->field($model, 'email') ?>

		<?php // echo $form->field($model, 'hobi') ?>

		<?php // echo $form->field($model, 'makanan_favorit') ?>

		<?php // echo $form->field($model, 'warna_favorit') ?>

		<?php // echo $form->field($model, 'motto') ?>

		<?php // echo $form->field($model, 'gaji_pokok') ?>

		<?php // echo $form->field($model, 'tunjangan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

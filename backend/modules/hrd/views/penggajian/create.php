<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Penggajian $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Penggajians'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud penggajian-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

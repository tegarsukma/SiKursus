<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
 * @var yii\web\View $this
 * @var app\models\Penggajian $model
 * @var yii\widgets\ActiveForm $form
 */

?>

<div class="penggajian-form">

    <?php $form = ActiveForm::begin([
            'id' => 'Penggajian',
            'layout' => 'horizontal',
            'enableClientValidation' => true,
            'errorSummaryCssClass' => 'error-summary alert alert-error',
            'options' => ['enctype' => 'multipart/form-data'],
        ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Form</h3>
        </div>
        <?php
        $bulan = [
            'Januari'=>'Januari',
            'Februari'=>'Februari',
            'Maret'=>'Maret',
            'April'=>'April',
            'Mei'=>'Mei',
            'Juni'=>'Juni',
            'Juli'=>'Juli',
            'Agustus'=>'Agustus',
            'September'=>'September',
            'Oktober'=> 'Oktober',
            'November'=> 'November',
            'Desember'=>'Desember',
        ]
        ?>

        <div class="box-body">
            <?= $form->field($model, 'pegawai')->widget(kartik\select2\Select2::className(),[
                'data'=>\yii\helpers\ArrayHelper::map(app\models\Pegawai::find()->all(), 'id', 'nama_lengkap'),
                'options'=>[
                    'prompt' => 'Pegawai'
                ]
            ]) ?>
            <?= $form->field($model, 'jumlah')->textInput() ?>
            <?= $form->field($model, 'bulan')->dropDownList($bulan,['prompt' => 'Bulan']) ?>
            <?= $form->field($model, 'tahun')->textInput(['readonly'=>true,'value'=>date('Y')]) ?>
            <?= $form->field($model, 'tanggal')->textInput(['maxlength' => true,'readonly'=>true,'value'=>date('d-M-Y:h.i.s a')]) ?>
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
                '<span class="fa fa-check"></span> ' .
                ($model->isNewRecord ? 'Create' : 'Save'),
                [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-success'
                ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>
<script>
    $("#penggajian-pegawai").change(function () {
        $.ajax({
            url:'<?=\yii\helpers\Url::to(["get-gaji"])?>',
            data:{
                id:this.value
            },
            type:"GET",
            success:function (data) {
                $('#penggajian-jumlah').val(data);
            }

        });
    });
</script>


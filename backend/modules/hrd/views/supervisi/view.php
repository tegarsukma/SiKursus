<?php

use dmstr\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
* @var yii\web\View $this
* @var app\models\Supervisi $model
*/
$copyParams = $model->attributes;

$this->title = Yii::t('app', 'Supervisi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Supervisis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="giiant-crud supervisi-view">

    <!-- flash message -->
    <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
        <span class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <?= \Yii::$app->session->getFlash('deleteError') ?>
        </span>
    <?php endif; ?>
    <div class="clearfix crud-navigation">

        <!-- menu buttons -->
        <div class='pull-left'>
            <?= Html::a(
            '<span class="fa fa-pencil"></span> ' . 'Edit',
            [ 'update', 'id' => $model->id],
            ['class' => 'btn btn-info']) ?>

            <?= Html::a(
            '<span class="fa fa-plus"></span> ' . 'New',
            ['create'],
            ['class' => 'btn btn-success']) ?>
        </div>

        <div class="pull-right">
            <?= Html::a('<span class="fa fa-list"></span> '
            . 'Full list', ['index'], ['class'=>'btn btn-default']) ?>
        </div>

    </div><br  clear="all">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Detail </h3>
        </div>

        <div class="box-body">
            <?php $this->beginBlock('app\models\Supervisi'); ?>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                   [
                        'format' => 'html',
                        'attribute' => 'tutor',
                        'value' => \app\models\Tutor::findOne($model->tutor)->nama_lengkap
                    ],
                    'level',
                    'hari',
                    'tanggal',
                ],
            ]); ?>
            <br>
            <table class="table table-bordered">
                <tr>
                    <th colspan="2">Aspek yang diamati</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                </tr>

                <?php
                $jm_nilai = 0;
                $models = \app\models\Supervisi::find()->where(['id'=>$_GET['id']])->one();
                for($i=1;$i<=39;$i++):?>
                    <?php
                        if($i==1){
                            echo "<tr bgcolor='#f9f9f9'><td colspan='7'><b><i>Aprersepsi dan motivasi</i></b></td></tr>";
                        } else if($i==6){
                            echo "<tr bgcolor='#f9f9f9'><td colspan='7'><b><i>Penyampaian Kompetensi dan Rencana Kegiatan</i></b></td></tr>";
                        } else if($i==8){
                            echo "<tr bgcolor='#f9f9f9'><td colspan='7'><b><i>Penguasaan Materi Pelajaran</i></b></td></tr>";
                        } else if($i==11){
                            echo "<tr bgcolor='#f9f9f9'><td colspan='7'><b><i>Penerapan Strategi Pembelajaran yang Mendidik</i></b></td></tr>";

                        } else if($i==17){
                            echo "<tr bgcolor='#f9f9f9'><td colspan='7'><b><i>Penerapan Pendekatan Saintifik</i></b></td></tr>";

                        }else if($i==23){
                            echo "<tr bgcolor='#f9f9f9'><td colspan='7'><b><i>Pemanfaatan Sumber Belajar/Media dalam Pembelajaran</i></b></td></tr>";

                        } else if($i==28){
                            echo "<tr bgcolor='#f9f9f9'><td colspan='7'><b><i>Menumbuhkan Peserta Didik dalam Pembelajaran</i></b></td></tr>";

                        } else if($i==34){
                            echo "<tr bgcolor='#f9f9f9'><td colspan='7'><b><i>Menumbuhkan Peserta Didik dalam Pembelajaran</i></b></td></tr>";

                        }
                    ?>
                    <tr>
                        <td><?=$i?></td>
                        <td><?=$models->getAttributeLabel("p".$i)?></td>
                        <?php

                        for($j=1;$j<=5;$j++){
                            $jm_nilai +=$models["p$i"];

                            if($models["p$i"]==$j){
                                echo "<td>√</td>";
                            } else {
                                echo "<td></td>";
                            }
                        }?>
                    </tr>
                <?php endfor;?>
                <tr>
                    <td>Jumlah Nilai</td>
                    <td colspan="5"><?=$p = $jm_nilai / 195 * 100 / 146?></td>
                </tr>
                <tr>
                    <td>Prosentase Nilai</td>
                    <td colspan="5"><?=$p * 146 / 195 * 100 / 100?></td>
                </tr>
            </table>

            <hr/>

            <?= Html::a('<span class="fa fa-trash"></span> ' . 'Delete', ['delete', 'id' => $model->id],
            [
            'class' => 'btn btn-danger',
            'data-confirm' => '' . 'Are you sure to delete this item?' . '',
            'data-method' => 'post',
            ]); ?>
            <?php $this->endBlock(); ?>


            
            <?= Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<b class=""># '.$model->id.'</b>',
    'content' => $this->blocks['app\models\Supervisi'],
    'active'  => true,
], ]
                 ]
    );
    ?>        </div>

    </div>


</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\SupervisiSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="supervisi-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'tutor') ?>

		<?= $form->field($model, 'supervisi') ?>

		<?= $form->field($model, 'level') ?>

		<?= $form->field($model, 'hari') ?>

		<?php // echo $form->field($model, 'tanggal') ?>

		<?php // echo $form->field($model, 'jam_ke') ?>

		<?php // echo $form->field($model, 'p1') ?>

		<?php // echo $form->field($model, 'p2') ?>

		<?php // echo $form->field($model, 'p3') ?>

		<?php // echo $form->field($model, 'p4') ?>

		<?php // echo $form->field($model, 'p5') ?>

		<?php // echo $form->field($model, 'p6') ?>

		<?php // echo $form->field($model, 'p7') ?>

		<?php // echo $form->field($model, 'p8') ?>

		<?php // echo $form->field($model, 'p9') ?>

		<?php // echo $form->field($model, 'p10') ?>

		<?php // echo $form->field($model, 'p11') ?>

		<?php // echo $form->field($model, 'p12') ?>

		<?php // echo $form->field($model, 'p13') ?>

		<?php // echo $form->field($model, 'p14') ?>

		<?php // echo $form->field($model, 'p15') ?>

		<?php // echo $form->field($model, 'p16') ?>

		<?php // echo $form->field($model, 'p17') ?>

		<?php // echo $form->field($model, 'p18') ?>

		<?php // echo $form->field($model, 'p19') ?>

		<?php // echo $form->field($model, 'p20') ?>

		<?php // echo $form->field($model, 'p21') ?>

		<?php // echo $form->field($model, 'p22') ?>

		<?php // echo $form->field($model, 'p23') ?>

		<?php // echo $form->field($model, 'p24') ?>

		<?php // echo $form->field($model, 'p25') ?>

		<?php // echo $form->field($model, 'p26') ?>

		<?php // echo $form->field($model, 'p27') ?>

		<?php // echo $form->field($model, 'p28') ?>

		<?php // echo $form->field($model, 'p29') ?>

		<?php // echo $form->field($model, 'p30') ?>

		<?php // echo $form->field($model, 'p31') ?>

		<?php // echo $form->field($model, 'p32') ?>

		<?php // echo $form->field($model, 'p33') ?>

		<?php // echo $form->field($model, 'p34') ?>

		<?php // echo $form->field($model, 'p35') ?>

		<?php // echo $form->field($model, 'p36') ?>

		<?php // echo $form->field($model, 'p37') ?>

		<?php // echo $form->field($model, 'p38') ?>

		<?php // echo $form->field($model, 'p39') ?>

		<?php // echo $form->field($model, 'saran') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

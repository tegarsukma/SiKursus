<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var app\models\Supervisi $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="supervisi-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Supervisi',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options'=>['enctype'=>'multipart/form-data'],
    ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
        </div>

        <div class="box-body">
            
			<?= $form->field($model, 'id')->textInput() ?>
			<?= $form->field($model, 'tutor')->textInput() ?>
			<?= $form->field($model, 'supervisi')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'level')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'hari')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'tanggal')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'jam_ke')->textInput() ?>
			<?= $form->field($model, 'p1')->textInput() ?>
			<?= $form->field($model, 'p2')->textInput() ?>
			<?= $form->field($model, 'p3')->textInput() ?>
			<?= $form->field($model, 'p4')->textInput() ?>
			<?= $form->field($model, 'p5')->textInput() ?>
			<?= $form->field($model, 'p6')->textInput() ?>
			<?= $form->field($model, 'p7')->textInput() ?>
			<?= $form->field($model, 'p8')->textInput() ?>
			<?= $form->field($model, 'p9')->textInput() ?>
			<?= $form->field($model, 'p10')->textInput() ?>
			<?= $form->field($model, 'p11')->textInput() ?>
			<?= $form->field($model, 'p12')->textInput() ?>
			<?= $form->field($model, 'p13')->textInput() ?>
			<?= $form->field($model, 'p14')->textInput() ?>
			<?= $form->field($model, 'p15')->textInput() ?>
			<?= $form->field($model, 'p16')->textInput() ?>
			<?= $form->field($model, 'p17')->textInput() ?>
			<?= $form->field($model, 'p18')->textInput() ?>
			<?= $form->field($model, 'p19')->textInput() ?>
			<?= $form->field($model, 'p20')->textInput() ?>
			<?= $form->field($model, 'p21')->textInput() ?>
			<?= $form->field($model, 'p22')->textInput() ?>
			<?= $form->field($model, 'p23')->textInput() ?>
			<?= $form->field($model, 'p24')->textInput() ?>
			<?= $form->field($model, 'p25')->textInput() ?>
			<?= $form->field($model, 'p26')->textInput() ?>
			<?= $form->field($model, 'p27')->textInput() ?>
			<?= $form->field($model, 'p28')->textInput() ?>
			<?= $form->field($model, 'p29')->textInput() ?>
			<?= $form->field($model, 'p30')->textInput() ?>
			<?= $form->field($model, 'p31')->textInput() ?>
			<?= $form->field($model, 'p32')->textInput() ?>
			<?= $form->field($model, 'p34')->textInput() ?>
			<?= $form->field($model, 'p35')->textInput() ?>
			<?= $form->field($model, 'p36')->textInput() ?>
			<?= $form->field($model, 'p37')->textInput() ?>
			<?= $form->field($model, 'p38')->textInput() ?>
			<?= $form->field($model, 'p39')->textInput() ?>
			<?= $form->field($model, 'saran')->textarea(['rows' => 6]) ?>
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
            '<span class="fa fa-check"></span> ' .
            ($model->isNewRecord ? 'Create' : 'Save'),
            [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success'
            ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>


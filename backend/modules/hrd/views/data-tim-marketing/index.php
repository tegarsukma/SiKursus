<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/**
* @var yii\web\View $this
* @var yii\data\ActiveDataProvider $dataProvider
    * @var app\models\SupervisiSearch $searchModel
*/

$this->title = 'Supervisis';
$this->params['breadcrumbs'][] = $this->title;

if (isset($actionColumnTemplates)) {
$actionColumnTemplate = implode(' ', $actionColumnTemplates);
$actionColumnTemplateString = $actionColumnTemplate;
} else {
Yii::$app->view->params['pageButtons'] = Html::a('<span
    class="fa fa-plus"></span> ' . 'New', ['create'], ['class' => 'btn btn-success']);
$actionColumnTemplateString = "{view} {update} {delete}";
}

?>
<div class="giiant-crud supervisi-index">

    <?php //             echo $this->render('_search', ['model' =>$searchModel]);
        ?>

    
    <?php \yii\widgets\Pjax::begin(['id'=>'pjax-main', 'enableReplaceState'=> false, 'linkSelector'=>'#pjax-main ul.pagination a, th a', 'clientOptions' => ['pjax:success'=>'function(){alert("yo")}']]) ?>

            <div class="pull-left">
            <?= Html::a('<span class="fa fa-plus"></span> ' . 'New', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        
    <div class="pull-right">

                
        <?= 
        \yii\bootstrap\ButtonDropdown::widget(
        [
        'id' => 'giiant-relations',
        'encodeLabel' => false,
        'label' => '<span class="fa fa-paperclip"></span> ' . 'Relations',
        'dropdown' => [
        'options' => [
        'class' => 'dropdown-menu-right'
        ],
        'encodeLabels' => false,
        'items' => []
        ],
        'options' => [
        'class' => 'btn-default'
        ]
        ]
        );
        ?>    </div>
</div><br clear="all"><br>

    <div class="clearfix"></div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data <?= Yii::t('app', 'Supervisis') ?></h3>
        </div>

        <div class="box-body">
            <div class="table-responsive">
                <?= GridView::widget([
                'layout' => '{summary}{pager}{items}{pager}',
                'dataProvider' => $dataProvider,
                'pager' => [
                'class' => yii\widgets\LinkPager::className(),
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'                ],
                                    'filterModel' => $searchModel,
                                'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                'headerRowOptions' => ['class'=>'x'],
                'columns' => [

                        [
            'class' => 'yii\grid\ActionColumn',
            'template' => $actionColumnTemplateString,
            'urlCreator' => function($action, $model, $key, $index) {
                // using the column name as key, not mapping to 'id' like the standard generator
                $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                return Url::toRoute($params);
            },
            'contentOptions' => ['nowrap'=>'nowrap']
        ],
			'tutor',
			'supervisi',
			'level',
			'hari',
			'tanggal',
			'jam_ke',
			'p1',
			/*'p2',*/
			/*'p3',*/
			/*'p4',*/
			/*'p5',*/
			/*'p6',*/
			/*'p7',*/
			/*'p8',*/
			/*'p9',*/
			/*'p10',*/
			/*'p11',*/
			/*'p12',*/
			/*'p13',*/
			/*'p14',*/
			/*'p15',*/
			/*'p16',*/
			/*'p17',*/
			/*'p18',*/
			/*'p19',*/
			/*'p20',*/
			/*'p21',*/
			/*'p22',*/
			/*'p23',*/
			/*'p24',*/
			/*'p25',*/
			/*'p26',*/
			/*'p27',*/
			/*'p28',*/
			/*'p29',*/
			/*'p30',*/
			/*'p31',*/
			/*'p32',*/
			/*'p34',*/
			/*'p35',*/
			/*'p36',*/
			/*'p37',*/
			/*'p38',*/
			/*'p39',*/
			/*'saran:ntext',*/
                ],
                ]); ?>
            </div>
        </div>
    </div>


<?php \yii\widgets\Pjax::end() ?>



<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Supervisi $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Supervisis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud supervisi-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

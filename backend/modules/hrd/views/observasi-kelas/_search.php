<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\ObservasiKelasSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="observasi-kelas-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'tutor') ?>

		<?= $form->field($model, 'level') ?>

		<?= $form->field($model, 'jam_ke') ?>

		<?= $form->field($model, 'tanggal') ?>

		<?php // echo $form->field($model, 'hari') ?>

		<?php // echo $form->field($model, 'p1') ?>

		<?php // echo $form->field($model, 'p2') ?>

		<?php // echo $form->field($model, 'p3') ?>

		<?php // echo $form->field($model, 'p4') ?>

		<?php // echo $form->field($model, 'p5') ?>

		<?php // echo $form->field($model, 'p6') ?>

		<?php // echo $form->field($model, 'p7') ?>

		<?php // echo $form->field($model, 'p9') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

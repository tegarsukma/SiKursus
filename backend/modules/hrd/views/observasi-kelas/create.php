<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\ObservasiKelas $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ObservasiKelas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud observasi-kelas-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

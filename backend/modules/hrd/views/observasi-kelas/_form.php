<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var app\models\ObservasiKelas $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="observasi-kelas-form">

    <?php $form = ActiveForm::begin([
    'id' => 'ObservasiKelas',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options'=>['enctype'=>'multipart/form-data'],
    ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title">Form</h3>
        </div>

        <div class="box-body">
            <?= $form->field($model, 'tutor')->widget(\kartik\select2\Select2::className(),[
                'data'=>\yii\helpers\ArrayHelper::map(\app\models\Tutor::find()->all(),'id','nama_lengkap'),
                'options'=>['prompt'=>'Tutor']
            ]) ?>
            <?= $form->field($model, 'hari')->textInput(['maxlength' => true,'value'=>\backend\components\LocalTime::getLocalDay()]) ?>
            <?= $form->field($model, 'tanggal')->textInput(['maxlength' => true,'value'=>date('d-F-Y')]) ?>
            <?= $form->field($model, 'jam_ke')->textInput() ?>
            <?= $form->field($model, 'level')->textInput() ?>
			<?= $form->field($model, 'p1')->textInput() ?>
			<?= $form->field($model, 'p2')->textInput() ?>
			<?= $form->field($model, 'p3')->textInput() ?>
			<?= $form->field($model, 'p4')->textInput() ?>
			<?= $form->field($model, 'p5')->textInput() ?>
			<?= $form->field($model, 'p6')->textInput() ?>
			<?= $form->field($model, 'p7')->textInput() ?>
			<?= $form->field($model, 'p9')->textInput() ?>
        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
            '<span class="fa fa-check"></span> ' .
            ($model->isNewRecord ? 'Create' : 'Save'),
            [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-success'
            ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>


<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\AbsensiTutorSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="absensi-tutor-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'tutor') ?>

		<?= $form->field($model, 'kelas') ?>

		<?= $form->field($model, 'waktu') ?>

		<?= $form->field($model, 'tanggal') ?>

		<?php // echo $form->field($model, 'bulan') ?>

		<?php // echo $form->field($model, 'tahun') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\AbsensiTutor $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'AbsensiTutors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud absensi-tutor-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

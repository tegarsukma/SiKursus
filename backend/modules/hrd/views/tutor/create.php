<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Tutor $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tutors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud tutor-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

<?php

namespace backend\modules\hrd\controllers;

/**
* This is the class for controller "SupervisiController".
*/
class SupervisiController extends \backend\modules\hrd\controllers\base\SupervisiController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

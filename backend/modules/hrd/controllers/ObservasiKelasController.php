<?php

namespace backend\modules\hrd\controllers;

/**
* This is the class for controller "ObservasiKelasController".
*/
class ObservasiKelasController extends \backend\modules\hrd\controllers\base\ObservasiKelasController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

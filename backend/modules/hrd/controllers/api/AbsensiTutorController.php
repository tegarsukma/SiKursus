<?php

namespace backend\modules\hrd\controllers\api;

/**
* This is the class for REST controller "AbsensiTutorController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class AbsensiTutorController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\AbsensiTutor';
}

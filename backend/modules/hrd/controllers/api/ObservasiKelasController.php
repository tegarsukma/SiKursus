<?php

namespace backend\modules\hrd\controllers\api;

/**
* This is the class for REST controller "ObservasiKelasController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class ObservasiKelasController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\ObservasiKelas';
}

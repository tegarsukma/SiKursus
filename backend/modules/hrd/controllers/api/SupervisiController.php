<?php

namespace backend\modules\hrd\controllers\api;

/**
* This is the class for REST controller "SupervisiController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class SupervisiController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Supervisi';
}

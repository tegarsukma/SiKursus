<?php

namespace backend\modules\hrd\controllers\api;

/**
* This is the class for REST controller "PengamatanPembelajarangController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class PengamatanPembelajarangController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\PengamatanPembelajaran';
}

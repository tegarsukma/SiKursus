<?php

namespace backend\modules\hrd\controllers\api;

/**
* This is the class for REST controller "TutorController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class TutorController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Tutor';
}

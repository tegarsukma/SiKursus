<?php

namespace backend\modules\hrd\controllers\api;

/**
* This is the class for REST controller "PegawaiController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class PegawaiController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Pegawai';
}

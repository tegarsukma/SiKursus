<?php

namespace backend\modules\hrd\controllers\api;

/**
* This is the class for REST controller "PenggajianController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class PenggajianController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Penggajian';
}

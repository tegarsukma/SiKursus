<?php

namespace backend\modules\hrd\controllers;

/**
* This is the class for controller "TutorController".
*/
class TutorController extends \backend\modules\hrd\controllers\base\TutorController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }

}

<?php

namespace backend\modules\hrd\controllers;

/**
* This is the class for controller "PenggajianController".
*/
class PenggajianController extends \backend\modules\hrd\controllers\base\PenggajianController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

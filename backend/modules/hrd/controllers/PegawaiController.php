<?php

namespace backend\modules\hrd\controllers;

/**
* This is the class for controller "PegawaiController".
*/
class PegawaiController extends \backend\modules\hrd\controllers\base\PegawaiController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

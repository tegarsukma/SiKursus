<?php

namespace backend\modules\hrd\controllers;

/**
* This is the class for controller "AbsensiTutorController".
*/
class AbsensiTutorController extends \backend\modules\hrd\controllers\base\AbsensiTutorController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

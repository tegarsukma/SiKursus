<?php

namespace backend\modules\hrd\controllers;

/**
* This is the class for controller "PengamatanPembelajarangController".
*/
class PengamatanPembelajarangController extends \backend\modules\hrd\controllers\base\PengamatanPembelajarangController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

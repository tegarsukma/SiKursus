<?php

namespace backend\modules\kesiswaan\controllers\api;

/**
* This is the class for REST controller "SiswaController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class SiswaController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Siswa';
}

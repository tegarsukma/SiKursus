<?php

namespace backend\modules\kesiswaan\controllers\api;

/**
* This is the class for REST controller "KelasController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class KelasController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Kelas';
}

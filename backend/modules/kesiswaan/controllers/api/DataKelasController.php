<?php

namespace backend\modules\kesiswaan\controllers\api;

/**
* This is the class for REST controller "DataKelasController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class DataKelasController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\DataKelas';
}

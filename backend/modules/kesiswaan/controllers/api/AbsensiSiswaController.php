<?php

namespace backend\modules\kesiswaan\controllers\api;

/**
* This is the class for REST controller "AbsensiSiswaController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class AbsensiSiswaController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\AbsensiSiswa';
}

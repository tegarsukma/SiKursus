<?php

namespace backend\modules\kesiswaan\controllers;

/**
* This is the class for controller "AbsensiSiswaController".
*/
class AbsensiSiswaController extends \backend\modules\kesiswaan\controllers\base\AbsensiSiswaController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

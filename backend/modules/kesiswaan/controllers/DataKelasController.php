<?php

namespace backend\modules\kesiswaan\controllers;

/**
* This is the class for controller "DataKelasController".
*/
class DataKelasController extends \backend\modules\kesiswaan\controllers\base\DataKelasController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

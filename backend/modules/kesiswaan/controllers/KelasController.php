<?php

namespace backend\modules\kesiswaan\controllers;

/**
* This is the class for controller "KelasController".
*/
class KelasController extends \backend\modules\kesiswaan\controllers\base\KelasController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

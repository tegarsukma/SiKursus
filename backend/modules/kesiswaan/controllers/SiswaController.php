<?php

namespace backend\modules\kesiswaan\controllers;

/**
* This is the class for controller "SiswaController".
*/
class SiswaController extends \backend\modules\kesiswaan\controllers\base\SiswaController
{
    public function beforeAction($action)
    {
        $this->layout = 'main'; //your layout name
        return parent::beforeAction($action);
    }
}

<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\DataKelas $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'DataKelas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud data-kelas-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

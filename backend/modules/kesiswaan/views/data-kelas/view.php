<?php

use dmstr\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var app\models\DataKelas $model
 */
$copyParams = $model->attributes;

$this->title = Yii::t('app', 'DataKelas');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'DataKelas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="giiant-crud data-kelas-view">

    <!-- flash message -->
    <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
        <span class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <?= \Yii::$app->session->getFlash('deleteError') ?>
        </span>
    <?php endif; ?>
    <div class="clearfix crud-navigation">

        <!-- menu buttons -->
        <div class='pull-left'>
            <?= Html::a(
                '<span class="fa fa-pencil"></span> ' . 'Edit',
                ['update', 'id' => $model->id],
                ['class' => 'btn btn-info']) ?>

            <?= Html::a(
                '<span class="fa fa-plus"></span> ' . 'New',
                ['create'],
                ['class' => 'btn btn-success']) ?>
        </div>

        <div class="pull-right">
            <?= Html::a('<span class="fa fa-list"></span> '
                . 'Full list', ['index'], ['class' => 'btn btn-default']) ?>
        </div>

    </div>
    <br clear="all">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Detail </h3>
        </div>

        <div class="box-body">
            <?php $this->beginBlock('app\models\DataKelas'); ?>


            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'nama_kelas',
                    [
                        'label' => 'Program',
                        'value' => \app\models\Program::findOne($model->program)->program
                    ],
                    'tingkat',
                    [
                        'label' => 'Jumlah Siswa',
                        'format' => 'raw',
                        'value' => call_user_func(function ($data) {
                            $jumlah_siswa = \app\models\Kelas::find()->where(['kelas' => $data->id])->count();
                            return "<span class='badge'>" . $jumlah_siswa . " Siswa </span>";
                        },$model),
                    ]
                ],
            ]); ?>


            <hr/>

            <?= Html::a('<span class="fa fa-trash"></span> ' . 'Delete', ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'data-confirm' => '' . 'Are you sure to delete this item?' . '',
                    'data-method' => 'post',
                ]); ?>
            <?php $this->endBlock(); ?>

            <?php $this->beginBlock('data-siswa') ?>
            <?php $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => \app\models\Kelas::find()->where(['kelas' => $model->id])
            ]);
            ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                'headerRowOptions' => ['class' => 'x'],
                'columns' => [
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'siswa',
                        'value' => function ($model) {
                            if ($rel = $model->getSiswa0()->one()) {
                                return Html::a($rel->nama_lengkap, ['siswa/view', 'id' => $rel->id,], ['data-pjax' => 0]);
                            } else {
                                return '';
                            }
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'kelas',
                        'value' => function ($data) {
                            return \app\models\DataKelas::findOne($data->kelas)->nama_kelas;
                        }
                    ],
                ],
            ]); ?>


            <?php $this->endBlock() ?>



            <?= Tabs::widget(
                [
                    'id' => 'relation-tabs',
                    'encodeLabels' => false,
                    'items' => [[
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['app\models\DataKelas'],
                        'active' => true,
                    ], [
                        'label' => '<b class="">Data Siswa</b>',
                        'content' => $this->blocks['data-siswa'],

                    ],]
                ]
            );
            ?>        </div>

    </div>


</div>

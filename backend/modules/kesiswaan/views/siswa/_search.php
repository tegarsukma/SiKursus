<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\SiswaSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="siswa-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'id_siswa') ?>

		<?= $form->field($model, 'nama_lengkap') ?>

		<?= $form->field($model, 'alamat') ?>

		<?= $form->field($model, 'tanggal_lahir') ?>

		<?php // echo $form->field($model, 'status_keaktifan') ?>

		<?php // echo $form->field($model, 'status_kursus') ?>

		<?php // echo $form->field($model, 'catatan_siswa') ?>

		<?php // echo $form->field($model, 'program') ?>

		<?php // echo $form->field($model, 'tempat_lahir') ?>

		<?php // echo $form->field($model, 'no_hp') ?>

		<?php // echo $form->field($model, 'status') ?>

		<?php // echo $form->field($model, 'nama_ayah') ?>

		<?php // echo $form->field($model, 'nama_ibu') ?>

		<?php // echo $form->field($model, 'alamat_ayah') ?>

		<?php // echo $form->field($model, 'alamat_ibu') ?>

		<?php // echo $form->field($model, 'pekerjaan_ayah') ?>

		<?php // echo $form->field($model, 'pekerjaan_ibu') ?>

		<?php // echo $form->field($model, 'foto') ?>

		<?php // echo $form->field($model, 'tahun_akademik') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

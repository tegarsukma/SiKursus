<?php

use dmstr\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var app\models\Siswa $model
 */
$copyParams = $model->attributes;

$this->title = Yii::t('app', 'Siswa');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Siswas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="giiant-crud siswa-view">

    <!-- flash message -->
    <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
        <span class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <?= \Yii::$app->session->getFlash('deleteError') ?>
        </span>
    <?php endif; ?>
    <div class="clearfix crud-navigation">

        <!-- menu buttons -->
        <div class='pull-left'>
            <?= Html::a(
                '<span class="fa fa-pencil"></span> ' . 'Edit',
                ['update', 'id' => $model->id],
                ['class' => 'btn btn-info']) ?>

            <?= Html::a(
                '<span class="fa fa-plus"></span> ' . 'New',
                ['create'],
                ['class' => 'btn btn-success']) ?>
        </div>

        <div class="pull-right">
            <?= Html::a('<span class="fa fa-list"></span> '
                . 'Full list', ['index'], ['class' => 'btn btn-default']) ?>
        </div>

    </div>
    <br clear="all">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Detail </h3>
        </div>

        <div class="box-body">

            <?php $this->beginBlock('app\models\Siswa'); ?>

            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'id_siswa',
                        'nama_lengkap',
                        'alamat',
                        'tanggal_lahir',
                        'sekolah',
                        [
                            'attribute'=>'status_keaktifan',
                            'value'=>\app\models\StatusKeaktifan::findOne($model->status_keaktifan)->status
                        ],
                        [
                            'attribute'=>'status_keaktifan',
                            'value'=>\app\models\StatusKursus::findOne($model->status_kursus)->status_kursus
                        ],
                        'catatan_siswa',
                        [
                            'attribute'=>'program',
                            'value'=>\app\models\Program::findOne($model->program)->program
                        ],
                        'status_siswa',
                        'tempat_lahir',
                        'no_hp',
                        'status',
                        'nama_ayah',
                        'nama_ibu',
                        'alamat_ayah',
                        'alamat_ibu',
                        'pekerjaan_ayah',
                        'pekerjaan_ibu',
                        'tahun_akademik',
                    ],
                ]); ?>
            </div>

            <div class="col-md-6">
                <?=Html::img("../../../../common/uploads/foto_siswa/".$model->foto,['width'=>'200px',"height"=>'auto','class'=>'img img-border'])?>
            </div>



            <br clear="all">
            <hr/>

            <?= Html::a('<span class="fa fa-trash"></span> ' . 'Delete', ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'data-confirm' => '' . 'Are you sure to delete this item?' . '',
                    'data-method' => 'post',
                ]); ?>
            <?php $this->endBlock(); ?>



            <?php $this->beginBlock('PembayaranSiswas'); ?>
            <div style='position: relative'>
                <div style='position:absolute; right: 0px; top: 0px;'>
                    <?= Html::a(
                        '<span class="fa fa-list"></span> ' . 'List All' . ' Pembayaran Siswas',
                        ['pembayaran-siswa/index'],
                        ['class' => 'btn text-muted btn-xs']
                    ) ?>
                    <?= Html::a(
                        '<span class="fa fa-plus"></span> ' . 'New' . ' Pembayaran Siswa',
                        ['pembayaran-siswa/create', 'PembayaranSiswa' => ['siswa' => $model->id]],
                        ['class' => 'btn btn-success btn-xs']
                    ); ?>
                </div>
            </div><?php Pjax::begin(['id' => 'pjax-PembayaranSiswas', 'enableReplaceState' => false, 'linkSelector' => '#pjax-PembayaranSiswas ul.pagination a, th a', 'clientOptions' => ['pjax:success' => 'function(){alert("yo")}']]) ?>
            <?= '<div class="table-responsive">' . \yii\grid\GridView::widget([
                'layout' => '{summary}{pager}<br/>{items}{pager}',
                'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getPembayaranSiswas(), 'pagination' => ['pageSize' => 20, 'pageParam' => 'page-pembayaransiswas']]),
                'pager' => [
                    'class' => yii\widgets\LinkPager::className(),
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'
                ],
                'columns' => [[
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update}',
                    'contentOptions' => ['nowrap' => 'nowrap'],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
                        $params[0] = 'pembayaran-siswa' . '/' . $action;
                        return $params;
                    },
                    'buttons' => [

                    ],
                    'controller' => 'pembayaran-siswa'
                ],
                    'id',
                    'keterangan:ntext',
                    'tanggal',
                    'jumlah',
                ]
            ]) . '</div>' ?>
            <?php Pjax::end() ?>
            <?php $this->endBlock() ?>


            <?= Tabs::widget(
                [
                    'id' => 'relation-tabs',
                    'encodeLabels' => false,
                    'items' => [[
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['app\models\Siswa'],
                        'active' => true,
                    ], [
                        'content' => $this->blocks['PembayaranSiswas'],
                        'label' => '<small>Pembayaran Siswas <span class="badge badge-default">' . count($model->getPembayaranSiswas()->asArray()->all()) . '</span></small>',
                        'active' => false,
                    ],]
                ]
            );
            ?>        </div>

    </div>


</div>

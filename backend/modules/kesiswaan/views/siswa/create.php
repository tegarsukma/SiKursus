<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Siswa $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Siswas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud siswa-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

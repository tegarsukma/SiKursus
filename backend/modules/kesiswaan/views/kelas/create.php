<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Kelas $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kelas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud kelas-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

<?php
/**
 * Created by PhpStorm.
 * User: Irfan
 * Date: 5/3/17
 * Time: 13:04
 */

use yii\helpers\Html;
$k = @$_GET['id_kel'];
if(empty($k)){
    $kelas = \app\models\Kelas::find()->all();
} else {
    $kelas = \app\models\Kelas::find()->where(['kelas'=>$k])->all();
}


?>
<div class="pull-left">
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Pilih Kelas
            <span class="caret"></span>
        </button>

        <ul class="dropdown-menu">
            <?php foreach (\app\models\DataKelas::find()->all() as $kls):?>
            <li><?=Html::a($kls->nama_kelas,['lihat','id_kel'=>$kls->id])?></li>
            <?php endforeach;?>
        </ul>
    </div>
</div>
<br clear="all"><br>

<div class="clearfix"></div>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Data <?= Yii::t('app', 'AbsensiSiswas') ?></h3>
    </div>

    <div class="box-body">
        <div class="table table-responsive">
            <table class="table table-bordered table-responsive">
                <tr>
                    <th rowspan="2">Nama Siswa</th>
                    <th colspan="31">Absensi</th>
                </tr>
                <tr>
                    <?php for ($i=0;$i<31;$i++){?>
                        <td><?=$i+1?></td>
                    <?php } ?>
                </tr>
                <?php foreach ($kelas as $kel){?>
                    <tr>
                        <td>
                            <?=$sis = \app\models\Siswa::findOne($kel->siswa)->nama_lengkap?>
                        </td>
                        <?php for ($i=0;$i<31;$i++){?>
                            <td>
                                <?php
                                    $ab = \app\models\AbsensiSiswa::find()->where(['siswa'=>$kel->siswa,'tgl'=>$i+1,'bulan'=>\backend\components\LocalTime::getLocalMonth()])->all();
                                    foreach ($ab as $b){
                                        echo $b->keterangan;
                                        break;
                                    }
                                ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </table>
        </div>

    </div>

</div>


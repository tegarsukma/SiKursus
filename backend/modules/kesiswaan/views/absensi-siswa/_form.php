<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
 * @var yii\web\View $this
 * @var app\models\AbsensiSiswa $model
 * @var yii\widgets\ActiveForm $form
 */

?>

<div class="absensi-siswa-form">

    <?php $form = ActiveForm::begin([
            'id' => 'AbsensiSiswa',
            'layout' => 'horizontal',
            'enableClientValidation' => true,
            'errorSummaryCssClass' => 'error-summary alert alert-error',
            'options' => ['enctype' => 'multipart/form-data'],
        ]
    );
    ?>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Form</h3>
        </div>

        <div class="box-body">
            <?= $form->field($model, 'siswa')->widget(\kartik\select2\Select2::className(), [
                'data' => \yii\helpers\ArrayHelper::map(app\models\Siswa::find()->all(), 'id', 'nama_lengkap'),
                'options' => [
                    'prompt' => 'Pilih Siswa'
                ]
            ]) ?>
            <?= // generated by schmunk42\giiant\generators\crud\providers\RelationProvider::activeField
            $form->field($model, 'program')->dropDownList(
                \yii\helpers\ArrayHelper::map(app\models\Program::find()->all(), 'id', 'program'),
                ['prompt' => 'Program']
            ); ?>

            <?= $form->field($model, 'tanggal')->textInput(['value'=>date('d-M-Y'),'readonly'=>true]) ?>
            <?= $form->field($model, 'pertemuan_ke')->textInput() ?>
            <div style="display: none">
                <?= $form->field($model, 'tgl')->textInput(['value'=>date('j')]) ?>
                <?= $form->field($model, 'bulan')->textInput(['value'=>\backend\components\LocalTime::getLocalMonth()]) ?>
                <?= $form->field($model, 'tahun')->textInput(['value'=>date('Y')]) ?>
            </div>
            <?= // generated by schmunk42\giiant\generators\crud\providers\RelationProvider::activeField
            $form->field($model, 'keterangan')->dropDownList(
                ['H'=>'H (hadir)','A'=>'A (alpha)','I'=>'I (izin)','S'=>'S (sakit)'],
                ['prompt' => 'Keterangan Siswa']
            ); ?>

        </div>

        <div class="box-footer">
            <?php echo $form->errorSummary($model); ?>

            <?= Html::submitButton(
                '<span class="fa fa-check"></span> ' .
                ($model->isNewRecord ? 'Create' : 'Save'),
                [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-success'
                ]
            );
            ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>


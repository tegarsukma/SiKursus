<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\AbsensiSiswa $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'AbsensiSiswas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud absensi-siswa-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

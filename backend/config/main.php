<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'name'=> 'SIM Kursus ',
    'timezone'=>'Asia/jakarta',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'marketing' => [
            'class' => 'backend\modules\marketing\Module',
        ],
        'hrd' => [
            'class' => 'backend\modules\hrd\Module',
        ],
        'kesiswaan' => [
            'class' => 'backend\modules\kesiswaan\Module',
        ],
        'akademik' => [
            'class' => 'backend\modules\akademik\Module',
        ],
        'keuangan' => [
            'class' => 'backend\modules\keuangan\Module',
        ],
        'administrasi' => [
            'class' => 'backend\modules\administrasi\Module',
        ],
        'tutor' => [
            'class' => 'backend\modules\tutor\Module',
        ],

    ],

    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],

        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-black',
                ],
            ],
        ],

    ],

    'params' => $params,
];

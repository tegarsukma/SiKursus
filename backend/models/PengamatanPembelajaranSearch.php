<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PengamatanPembelajaran;

/**
* PengamatanPembelajaranSearch represents the model behind the search form about `app\models\PengamatanPembelajaran`.
*/
class PengamatanPembelajaranSearch extends PengamatanPembelajaran
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'tutor', 'jam_ke', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8', 'p9', 'p10', 'p11', 'p12', 'p13', 'p14', 'p15', 'p16', 'p17', 'p18', 'p19', 'p20', 'p21', 'p22', 'p23', 'p24', 'p25'], 'integer'],
            [['tanggal', 'hari'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = PengamatanPembelajaran::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'tutor' => $this->tutor,
            'jam_ke' => $this->jam_ke,
            'p1' => $this->p1,
            'p2' => $this->p2,
            'p3' => $this->p3,
            'p4' => $this->p4,
            'p5' => $this->p5,
            'p6' => $this->p6,
            'p7' => $this->p7,
            'p8' => $this->p8,
            'p9' => $this->p9,
            'p10' => $this->p10,
            'p11' => $this->p11,
            'p12' => $this->p12,
            'p13' => $this->p13,
            'p14' => $this->p14,
            'p15' => $this->p15,
            'p16' => $this->p16,
            'p17' => $this->p17,
            'p18' => $this->p18,
            'p19' => $this->p19,
            'p20' => $this->p20,
            'p21' => $this->p21,
            'p22' => $this->p22,
            'p23' => $this->p23,
            'p24' => $this->p24,
            'p25' => $this->p25,
        ]);

        $query->andFilterWhere(['like', 'tanggal', $this->tanggal])
            ->andFilterWhere(['like', 'hari', $this->hari]);

return $dataProvider;
}
}
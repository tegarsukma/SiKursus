<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Aktifitas;

/**
* AktifitasSearch represents the model behind the search form about `app\models\Aktifitas`.
*/
class AktifitasSearch extends Aktifitas
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'status_aktifitas', 'target'], 'integer'],
            [['judul', 'keterangan', 'tanggal_mulai', 'tanggal_selesai', 'waktu_mulai', 'waktu_berakhir', 'pic', 'profil_market'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Aktifitas::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'status_aktifitas' => $this->status_aktifitas,
            'target' => $this->target,
        ]);

        $query->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'tanggal_mulai', $this->tanggal_mulai])
            ->andFilterWhere(['like', 'tanggal_selesai', $this->tanggal_selesai])
            ->andFilterWhere(['like', 'waktu_mulai', $this->waktu_mulai])
            ->andFilterWhere(['like', 'waktu_berakhir', $this->waktu_berakhir])
            ->andFilterWhere(['like', 'pic', $this->pic])
            ->andFilterWhere(['like', 'profil_market', $this->profil_market]);

return $dataProvider;
}
}
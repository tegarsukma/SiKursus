<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ObservasiKelas;

/**
* ObservasiKelasSearch represents the model behind the search form about `app\models\ObservasiKelas`.
*/
class ObservasiKelasSearch extends ObservasiKelas
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'tutor', 'level', 'jam_ke', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p9'], 'integer'],
            [['tanggal', 'hari'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = ObservasiKelas::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'tutor' => $this->tutor,
            'level' => $this->level,
            'jam_ke' => $this->jam_ke,
            'p1' => $this->p1,
            'p2' => $this->p2,
            'p3' => $this->p3,
            'p4' => $this->p4,
            'p5' => $this->p5,
            'p6' => $this->p6,
            'p7' => $this->p7,
            'p9' => $this->p9,
        ]);

        $query->andFilterWhere(['like', 'tanggal', $this->tanggal])
            ->andFilterWhere(['like', 'hari', $this->hari]);

return $dataProvider;
}
}
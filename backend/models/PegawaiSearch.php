<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pegawai;

/**
* PegawaiSearch represents the model behind the search form about `app\models\Pegawai`.
*/
class PegawaiSearch extends Pegawai
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'gaji_pokok', 'tunjangan'], 'integer'],
            [['nama_lengkap', 'tanggal_lahir', 'alamat', 'no_telp', 'email', 'hobi', 'makanan_favorit', 'warna_favorit', 'motto'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Pegawai::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'gaji_pokok' => $this->gaji_pokok,
            'tunjangan' => $this->tunjangan,
        ]);

        $query->andFilterWhere(['like', 'nama_lengkap', $this->nama_lengkap])
            ->andFilterWhere(['like', 'tanggal_lahir', $this->tanggal_lahir])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'no_telp', $this->no_telp])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'hobi', $this->hobi])
            ->andFilterWhere(['like', 'makanan_favorit', $this->makanan_favorit])
            ->andFilterWhere(['like', 'warna_favorit', $this->warna_favorit])
            ->andFilterWhere(['like', 'motto', $this->motto]);

return $dataProvider;
}
}
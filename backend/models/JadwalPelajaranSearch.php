<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JadwalPelajaran;

/**
* JadwalPelajaranSearch represents the model behind the search form about `app\models\JadwalPelajaran`.
*/
class JadwalPelajaranSearch extends JadwalPelajaran
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'mata_pelajaran'], 'integer'],
            [['kelas', 'hari'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = JadwalPelajaran::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'mata_pelajaran' => $this->mata_pelajaran,
        ]);

        $query->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'hari', $this->hari]);

return $dataProvider;
}
}
<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Raport;

/**
* RaportSearch represents the model behind the search form about `app\models\Raport`.
*/
class RaportSearch extends Raport
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'siswa', 'vocabulary', 'fluency', 'accuracy', 'pronunciation', 'intonation', 'understanding', 'diction', 'respect', 'honest', 'care', 'brave', 'convidence', 'communicative', 'sosial_awarnes', 'curiousity', 'team_work'], 'integer'],
            [['mengulang_materi', 'mempelajari_materi', 'mengerjakan_tugas', 'memutar_audio', 'bermain_game', 'belajar_online', 'praktek_materi'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Raport::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'siswa' => $this->siswa,
            'vocabulary' => $this->vocabulary,
            'fluency' => $this->fluency,
            'accuracy' => $this->accuracy,
            'pronunciation' => $this->pronunciation,
            'intonation' => $this->intonation,
            'understanding' => $this->understanding,
            'diction' => $this->diction,
            'respect' => $this->respect,
            'honest' => $this->honest,
            'care' => $this->care,
            'brave' => $this->brave,
            'convidence' => $this->convidence,
            'communicative' => $this->communicative,
            'sosial_awarnes' => $this->sosial_awarnes,
            'curiousity' => $this->curiousity,
            'team_work' => $this->team_work,
        ]);

        $query->andFilterWhere(['like', 'mengulang_materi', $this->mengulang_materi])
            ->andFilterWhere(['like', 'mempelajari_materi', $this->mempelajari_materi])
            ->andFilterWhere(['like', 'mengerjakan_tugas', $this->mengerjakan_tugas])
            ->andFilterWhere(['like', 'memutar_audio', $this->memutar_audio])
            ->andFilterWhere(['like', 'bermain_game', $this->bermain_game])
            ->andFilterWhere(['like', 'belajar_online', $this->belajar_online])
            ->andFilterWhere(['like', 'praktek_materi', $this->praktek_materi]);

return $dataProvider;
}
}
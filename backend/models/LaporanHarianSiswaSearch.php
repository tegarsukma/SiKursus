<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LaporanHarianSiswa;

/**
* LaporanHarianSiswaSearch represents the model behind the search form about `app\models\LaporanHarianSiswa`.
*/
class LaporanHarianSiswaSearch extends LaporanHarianSiswa
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'jurnal', 'siswa'], 'integer'],
            [['catatan'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = LaporanHarianSiswa::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'jurnal' => $this->jurnal,
            'siswa' => $this->siswa,
        ]);

        $query->andFilterWhere(['like', 'catatan', $this->catatan]);

return $dataProvider;
}
}
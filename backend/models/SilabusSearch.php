<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Silabus;

/**
* SilabusSearch represents the model behind the search form about `app\models\Silabus`.
*/
class SilabusSearch extends Silabus
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'mata_pelajaran'], 'integer'],
            [['standar_kompetensi', 'kompetensi_dasar', 'hasil_belajar', 'indokator_hasil_belajar', 'materi_pokok', 'alokasi_waktu'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Silabus::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'mata_pelajaran' => $this->mata_pelajaran,
        ]);

        $query->andFilterWhere(['like', 'standar_kompetensi', $this->standar_kompetensi])
            ->andFilterWhere(['like', 'kompetensi_dasar', $this->kompetensi_dasar])
            ->andFilterWhere(['like', 'hasil_belajar', $this->hasil_belajar])
            ->andFilterWhere(['like', 'indokator_hasil_belajar', $this->indokator_hasil_belajar])
            ->andFilterWhere(['like', 'materi_pokok', $this->materi_pokok])
            ->andFilterWhere(['like', 'alokasi_waktu', $this->alokasi_waktu]);

return $dataProvider;
}
}
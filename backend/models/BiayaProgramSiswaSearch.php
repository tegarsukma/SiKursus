<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BiayaProgramSiswa;

/**
* BiayaProgramSiswaSearch represents the model behind the search form about `app\models\BiayaProgramSiswa`.
*/
class BiayaProgramSiswaSearch extends BiayaProgramSiswa
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'siswa', 'biaya_program'], 'integer'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = BiayaProgramSiswa::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'siswa' => $this->siswa,
            'biaya_program' => $this->biaya_program,
        ]);

return $dataProvider;
}
}
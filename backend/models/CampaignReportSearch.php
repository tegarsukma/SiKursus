<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CampaignReport;

/**
* CampaignReportSearch represents the model behind the search form about `app\models\CampaignReport`.
*/
class CampaignReportSearch extends CampaignReport
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'aktifitas', 'total_registran', 'total_revenue', 'roas'], 'integer'],
            [['evaluasi', 'tanggal'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = CampaignReport::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'aktifitas' => $this->aktifitas,
            'total_registran' => $this->total_registran,
            'total_revenue' => $this->total_revenue,
            'roas' => $this->roas,
        ]);

        $query->andFilterWhere(['like', 'evaluasi', $this->evaluasi])
            ->andFilterWhere(['like', 'tanggal', $this->tanggal]);

return $dataProvider;
}
}
<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NeracaLajur;

/**
* NeracaLajurSearch represents the model behind the search form about `app\models\NeracaLajur`.
*/
class NeracaLajurSearch extends NeracaLajur
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'no_akun', 'neraca_saldo_debet', 'neraca_saldo_kredit', 'penyesuaian_debet', 'penyesuaian_kredit', 'ns_disesuaikan_debet', 'ns_disesuaikan_kredit', 'laba_rugi_debet', 'laba_rugi_kredit', 'neraca_debet', 'neraca_kredit'], 'integer'],
            [['nama_akun'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = NeracaLajur::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'no_akun' => $this->no_akun,
            'neraca_saldo_debet' => $this->neraca_saldo_debet,
            'neraca_saldo_kredit' => $this->neraca_saldo_kredit,
            'penyesuaian_debet' => $this->penyesuaian_debet,
            'penyesuaian_kredit' => $this->penyesuaian_kredit,
            'ns_disesuaikan_debet' => $this->ns_disesuaikan_debet,
            'ns_disesuaikan_kredit' => $this->ns_disesuaikan_kredit,
            'laba_rugi_debet' => $this->laba_rugi_debet,
            'laba_rugi_kredit' => $this->laba_rugi_kredit,
            'neraca_debet' => $this->neraca_debet,
            'neraca_kredit' => $this->neraca_kredit,
        ]);

        $query->andFilterWhere(['like', 'nama_akun', $this->nama_akun]);

return $dataProvider;
}
}
<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DataKelas;

/**
* DataKelasSearch represents the model behind the search form about `app\models\DataKelas`.
*/
class DataKelasSearch extends DataKelas
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'program', 'tingkat'], 'integer'],
            [['nama_kelas'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = DataKelas::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'program' => $this->program,
            'tingkat' => $this->tingkat,
        ]);

        $query->andFilterWhere(['like', 'nama_kelas', $this->nama_kelas]);

return $dataProvider;
}
}
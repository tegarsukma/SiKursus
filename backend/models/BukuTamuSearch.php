<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BukuTamu;

/**
* BukuTamuSearch represents the model behind the search form about `app\models\BukuTamu`.
*/
class BukuTamuSearch extends BukuTamu
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id'], 'integer'],
            [['nama_lengkap', 'alamat', 'perihal', 'orang_tujuan', 'jam_kedatangan', 'tanggal'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = BukuTamu::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'nama_lengkap', $this->nama_lengkap])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'perihal', $this->perihal])
            ->andFilterWhere(['like', 'orang_tujuan', $this->orang_tujuan])
            ->andFilterWhere(['like', 'jam_kedatangan', $this->jam_kedatangan])
            ->andFilterWhere(['like', 'tanggal', $this->tanggal]);

return $dataProvider;
}
}
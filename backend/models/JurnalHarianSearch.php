<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JurnalHarian;

/**
* JurnalHarianSearch represents the model behind the search form about `app\models\JurnalHarian`.
*/
class JurnalHarianSearch extends JurnalHarian
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'level', 'pertemuan_ke', 'jumlah_siswa', 'jumlah_hadir', 'jumlah_absen'], 'integer'],
            [['nama_pengajar', 'pukul', 'program', 'materi', 'uraian', 'media', 'tambahan', 'pembukaan', 'inti', 'penutupan'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = JurnalHarian::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'level' => $this->level,
            'pertemuan_ke' => $this->pertemuan_ke,
            'jumlah_siswa' => $this->jumlah_siswa,
            'jumlah_hadir' => $this->jumlah_hadir,
            'jumlah_absen' => $this->jumlah_absen,
        ]);

        $query->andFilterWhere(['like', 'nama_pengajar', $this->nama_pengajar])
            ->andFilterWhere(['like', 'pukul', $this->pukul])
            ->andFilterWhere(['like', 'program', $this->program])
            ->andFilterWhere(['like', 'materi', $this->materi])
            ->andFilterWhere(['like', 'uraian', $this->uraian])
            ->andFilterWhere(['like', 'media', $this->media])
            ->andFilterWhere(['like', 'tambahan', $this->tambahan])
            ->andFilterWhere(['like', 'pembukaan', $this->pembukaan])
            ->andFilterWhere(['like', 'inti', $this->inti])
            ->andFilterWhere(['like', 'penutupan', $this->penutupan]);

return $dataProvider;
}
}
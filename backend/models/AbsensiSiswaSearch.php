<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AbsensiSiswa;

/**
* AbsensiSiswaSearch represents the model behind the search form about `app\models\AbsensiSiswa`.
*/
class AbsensiSiswaSearch extends AbsensiSiswa
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'program', 'siswa', 'pertemuan_ke'], 'integer'],
            [['tanggal'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = AbsensiSiswa::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'program' => $this->program,
            'siswa' => $this->siswa,
            'pertemuan_ke' => $this->pertemuan_ke,
        ]);

        $query->andFilterWhere(['like', 'tanggal', $this->tanggal]);

return $dataProvider;
}
}
<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "absensi_tutor".
 *
 * @property integer $id
 * @property integer $tutor
 * @property integer $kelas
 * @property string $waktu
 * @property integer $tanggal
 * @property string $bulan
 * @property integer $tahun
 * @property string $aliasModel
 */
abstract class AbsensiTutor extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'absensi_tutor';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tutor', 'kelas', 'waktu', 'tanggal', 'bulan', 'tahun'], 'required'],
            [['tutor', 'kelas', 'tanggal', 'tahun'], 'integer'],
            [['waktu', 'bulan'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tutor' => 'Tutor',
            'kelas' => 'Kelas',
            'waktu' => 'Waktu',
            'tanggal' => 'Tanggal',
            'bulan' => 'Bulan',
            'tahun' => 'Tahun',
        ];
    }




}

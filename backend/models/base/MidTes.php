<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "mid_tes".
 *
 * @property integer $id
 * @property integer $siswa
 * @property integer $pelajaran
 * @property integer $nilai
 * @property string $aliasModel
 */
abstract class MidTes extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mid_tes';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['siswa', 'pelajaran', 'nilai'], 'required'],
            [['siswa', 'pelajaran', 'nilai'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'siswa' => 'Siswa',
            'pelajaran' => 'Pelajaran',
            'nilai' => 'Nilai',
        ];
    }




}

<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "biaya_program_siswa".
 *
 * @property integer $id
 * @property integer $siswa
 * @property integer $biaya_program
 * @property string $aliasModel
 */
abstract class BiayaProgramSiswa extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'biaya_program_siswa';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['siswa', 'biaya_program'], 'required'],
            [['siswa', 'biaya_program'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'siswa' => 'Siswa',
            'biaya_program' => 'Biaya Program',
        ];
    }




}

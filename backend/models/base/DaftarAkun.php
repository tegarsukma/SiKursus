<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "daftar_akun".
 *
 * @property integer $id
 * @property integer $nomor_akun
 * @property integer $saldo
 * @property string $aliasModel
 */
abstract class DaftarAkun extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daftar_akun';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomor_akun', 'saldo'], 'required'],
            [['nomor_akun', 'saldo'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomor_akun' => 'Nomor Akun',
            'saldo' => 'Saldo',
        ];
    }




}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BiayaProgram;

/**
* BiayaProgramSearch represents the model behind the search form about `app\models\BiayaProgram`.
*/
class BiayaProgramSearch extends BiayaProgram
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'program', 'jumlah_siswa', 'banyak_angsuran', 'jumlah_biaya'], 'integer'],
            [['durasi', 'jenis_kursus'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = BiayaProgram::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'program' => $this->program,
            'jumlah_siswa' => $this->jumlah_siswa,
            'banyak_angsuran' => $this->banyak_angsuran,
            'jumlah_biaya' => $this->jumlah_biaya,
        ]);

        $query->andFilterWhere(['like', 'durasi', $this->durasi])
            ->andFilterWhere(['like', 'jenis_kursus', $this->jenis_kursus]);

return $dataProvider;
}
}
<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AbsensiTutor;

/**
* AbsensiTutorSearch represents the model behind the search form about `app\models\AbsensiTutor`.
*/
class AbsensiTutorSearch extends AbsensiTutor
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'tutor', 'kelas', 'tanggal', 'tahun'], 'integer'],
            [['waktu', 'bulan'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = AbsensiTutor::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'tutor' => $this->tutor,
            'kelas' => $this->kelas,
            'tanggal' => $this->tanggal,
            'tahun' => $this->tahun,
        ]);

        $query->andFilterWhere(['like', 'waktu', $this->waktu])
            ->andFilterWhere(['like', 'bulan', $this->bulan]);

return $dataProvider;
}
}
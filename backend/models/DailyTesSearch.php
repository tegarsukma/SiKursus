<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DailyTes;

/**
* DailySearch represents the model behind the search form about `app\models\DailyTes`.
*/
class DailyTesSearch extends DailyTes
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'siswa', 'pelajaran', 'nilai_1', 'nilai_2', 'nilai_3', 'nilai_4', 'nilai_5', 'nilai_6', 'nilai_7', 'nilai_8'], 'integer'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = DailyTes::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'siswa' => $this->siswa,
            'pelajaran' => $this->pelajaran,
            'nilai_1' => $this->nilai_1,
            'nilai_2' => $this->nilai_2,
            'nilai_3' => $this->nilai_3,
            'nilai_4' => $this->nilai_4,
            'nilai_5' => $this->nilai_5,
            'nilai_6' => $this->nilai_6,
            'nilai_7' => $this->nilai_7,
            'nilai_8' => $this->nilai_8,
        ]);

return $dataProvider;
}
}
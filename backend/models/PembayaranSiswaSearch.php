<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PembayaranSiswa;

/**
* PembayaranSiswaSearch represents the model behind the search form about `app\models\PembayaranSiswa`.
*/
class PembayaranSiswaSearch extends PembayaranSiswa
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'siswa', 'jumlah'], 'integer'],
            [['keterangan', 'tanggal'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = PembayaranSiswa::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'siswa' => $this->siswa,
            'jumlah' => $this->jumlah,
        ]);

        $query->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'tanggal', $this->tanggal]);

return $dataProvider;
}
}
<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Siswa;

/**
 * SiswaSearch represents the model behind the search form about `app\models\Siswa`.
 */
class SiswaSearch extends Siswa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_keaktifan', 'status_kursus', 'catatan_siswa', 'program', 'tahun_akademik'], 'integer'],
            [['id_siswa', 'nama_lengkap', 'alamat', 'tanggal_lahir', 'tempat_lahir', 'no_hp', 'status', 'nama_ayah', 'nama_ibu', 'alamat_ayah', 'alamat_ibu', 'pekerjaan_ayah', 'pekerjaan_ibu', 'foto'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Siswa::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status_keaktifan' => $this->status_keaktifan,
            'status_kursus' => $this->status_kursus,
            'catatan_siswa' => $this->catatan_siswa,
            'program' => $this->program,
            'tahun_akademik' => $this->tahun_akademik,
        ]);

        $query->andFilterWhere(['like', 'id_siswa', $this->id_siswa])
            ->andFilterWhere(['like', 'nama_lengkap', $this->nama_lengkap])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'tanggal_lahir', $this->tanggal_lahir])
            ->andFilterWhere(['like', 'tempat_lahir', $this->tempat_lahir])
            ->andFilterWhere(['like', 'no_hp', $this->no_hp])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'nama_ayah', $this->nama_ayah])
            ->andFilterWhere(['like', 'nama_ibu', $this->nama_ibu])
            ->andFilterWhere(['like', 'alamat_ayah', $this->alamat_ayah])
            ->andFilterWhere(['like', 'alamat_ibu', $this->alamat_ibu])
            ->andFilterWhere(['like', 'pekerjaan_ayah', $this->pekerjaan_ayah])
            ->andFilterWhere(['like', 'pekerjaan_ibu', $this->pekerjaan_ibu])
            ->andFilterWhere(['like', 'foto', $this->foto]);

        return $dataProvider;
    }

    public function searchNew($params)
    {
        $query = Siswa::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status_keaktifan' => $this->status_keaktifan,
            'status_kursus' => $this->status_kursus,
            'catatan_siswa' => $this->catatan_siswa,
            'program' => $this->program,
            'tahun_akademik' => $this->tahun_akademik,
            'status_siswa'=>'baru',
        ]);

        $query->andFilterWhere(['like', 'id_siswa', $this->id_siswa])
            ->andFilterWhere(['like', 'nama_lengkap', $this->nama_lengkap])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'tanggal_lahir', $this->tanggal_lahir])
            ->andFilterWhere(['like', 'tempat_lahir', $this->tempat_lahir])
            ->andFilterWhere(['like', 'no_hp', $this->no_hp])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'nama_ayah', $this->nama_ayah])
            ->andFilterWhere(['like', 'nama_ibu', $this->nama_ibu])
            ->andFilterWhere(['like', 'alamat_ayah', $this->alamat_ayah])
            ->andFilterWhere(['like', 'alamat_ibu', $this->alamat_ibu])
            ->andFilterWhere(['like', 'pekerjaan_ayah', $this->pekerjaan_ayah])
            ->andFilterWhere(['like', 'pekerjaan_ibu', $this->pekerjaan_ibu])
            ->andFilterWhere(['like', 'foto', $this->foto]);

        return $dataProvider;
    }
}
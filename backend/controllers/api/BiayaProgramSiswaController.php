<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "BiayaProgramSiswaController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class BiayaProgramSiswaController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\BiayaProgramSiswa';
}

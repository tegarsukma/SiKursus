<?php
/**
 * Created by PhpStorm.
 * User: Irfan
 * Date: 10/25/16
 * Time: 19:58
 */

namespace backend\components;


use app\models\Module;
use app\models\Menu;
use app\models\RoleMenu;
use yii\bootstrap\Widget;
use yii\helpers\Url;

class SidebarMenu extends Widget
{
    public static function getMenu($module=2){
        $menu = [];
        $urole = \app\models\User::findOne(\Yii::$app->user->id)->role;

        $role_menu = Menu::find()
            ->leftJoin('role_menu','menu.id = role_menu.menu')
            ->where([
                "menu.parent"=>"",
                'role_menu.role'=>$urole,
                'module'=>$module
            ])
            ->all();

        foreach($role_menu as $menus){
            $mnu = [
                "label" =>$menus->menu,
                "icon"  =>$menus->icon,
                "url"   =>SidebarMenu::getUrl($menus),
            ];

            if(count($menus->menu) != 0){
                $mnu["items"] = SidebarMenu::getMenuByParent($menus->id,$module);
            }

            $menu[]= $mnu;
        }
        return $menu;
    }

    public static function getAllMenu(){
        $menu = [];
        $urole = \app\models\User::findOne(\Yii::$app->user->id)->role;

        $role_menu = Menu::find()
            ->leftJoin('role_menu','menu.id = role_menu.menu')
            ->where([
                "menu.parent"=>"",
                'role_menu.role'=>$urole,
            ])
            ->all();

        foreach($role_menu as $menus){
            $mnu = [
                "label" =>$menus->menu,
                "icon"  =>$menus->icon,
                "url"   =>SidebarMenu::getUrlAllMenu($menus),
            ];

            if(count($menus->menu) != 0){
                $mnu["items"] = SidebarMenu::getAllMenuByParent($menus->id);
            }

            $menu[]= $mnu;
        }
        return $menu;
    }

    public static function getMenuByParent($prt,$module=2){
        $menu = [];
        $urole = \app\models\User::findOne(\Yii::$app->user->id)->role;
        $role_menus = Menu::find()
            ->leftJoin('role_menu','menu.id = role_menu.menu')
            ->where([
                "menu.parent"=>$prt,
                'role_menu.role'=>$urole,
                'module'=>$module
            ])
            ->all();

        foreach($role_menus as $menus){
            $mnu = [
                "label" =>$menus->menu,
                "icon"  =>$menus->icon,
                "url"   =>SidebarMenu::getUrl($menus),
            ];

            $menu[]= $mnu;
        }
        return $menu;
    }

    public static function getAllMenuByParent($prt){
        $menu = [];
        $urole = \app\models\User::findOne(\Yii::$app->user->id)->role;
        $role_menus = Menu::find()
            ->leftJoin('role_menu','menu.id = role_menu.menu')
            ->where([
                "menu.parent"=>$prt,
                'role_menu.role'=>$urole,
            ])
            ->all();

        foreach($role_menus as $menus){
            $mnu = [
                "label" =>$menus->menu,
                "icon"  =>$menus->icon,
                "url"   =>SidebarMenu::getUrlAllMenu($menus),
            ];

            $menu[]= $mnu;
        }
        return $menu;
    }

    private static function getUrl($menu){
        if($menu->controller == NULL){
            return "#";
        }else{
            return [$menu->controller."/"];
        }
    }

    private static function getUrlAllMenu($menu){
        $module = Module::findOne($menu->module)->module;
        if($menu->controller == NULL){
            return "#";
        }else{
            if($module=="backend"){
                $a= [$menu->controller."/"];
            } else {
                $a= Url::to([strtolower("$module/")."$menu->controller"]);
            }

            return $a;

        }
    }

}

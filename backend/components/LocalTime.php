<?php
/**
 * Created by PhpStorm.
 * User: Irfan
 * Date: 5/3/17
 * Time: 14:28
 */

namespace backend\components;


class LocalTime
{
    static function getLocalMonth(){
        $ar = ['januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember'];
        $m = round(date('m'))-1;
        return $ar[$m];
    }

    static function getLocalDay(){
        $arr_day = ['senin','selasa','rabu','kamis','jumat','sabtu','minggu'];
        return $arr_day[date('w')-1];
    }
}
<?php

/* @var $this yii\web\View */

$this->title = 'ILF Indonesia - Filosofi Logo';
?>
<div class='container'>
    <div class='slogan'>
        <h1>Filosofi Logo</h1>
    </div>
    <hr>
</div>
</div>
<div class='container'>

    <div class='isotope-w' style="text-align: left;">
       <center><img src="<?= Yii::$app->request->baseUrl ?>/images/logo.jpg" /></center>
        <br>
        1. Tulisan ILF dengan  model tegak garis – garis seperti  susunan batu bata melambangkan Bahwa ILF Merupakan sebuah perusahaan yang dibangun dari bawah dengan pelan – pelan akan tetapi pada akhirnya semoga nanti menjadi perusahan yang bisa berdiri dengan kokoh.
        <br>
        2. Tulisan ILF dengan warna Emas,melambangkan bahwa dengan bergabung dengan ILF kita dapat mewujudkan kesejahteraan bersama yang lebih baik.
        <br>
        3. Tulisan International Language Foundation melambangkan bahwa ILF merupakan lembaga kursus Bhs.International
        <br>
        4. Tulisan International Language Foundation berwarna putih melambangkan bahwa ILF selalu memberikan pelayanan dengan setulus hati serta setiap melakukan tindakan berpegang teguh pada kejujuran
        <br>
        5. Warna Merah dengan bentuk elips melambangkan bahwa Keberanian dalam meraih mimpi dan selalu siap mengikuti perkembangan zaman.
    </div>
</div>
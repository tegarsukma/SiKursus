<div class='header-main header-tp' id='anchorHome'>
    <div class='container'>
        <nav class='navbar navbar-default' role='navigation'>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class='navbar-header'>
                <button class='navbar-toggle' data-target='.navbar-ex1-collapse' data-toggle='collapse' type='button'>
                    <span class='sr-only'>Toggle navigation</span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                </button>
                <a class='current navbar-brand' href='<?= Yii::$app->request->baseUrl ?>'>
                    <img alt='' height='45'
                         src='<?= Yii::$app->request->baseUrl ?>/images/colors/blue/logo-small-top.png'>
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class='collapse navbar-collapse navbar-ex1-collapse'>
                <ul class='nav navbar-nav navbar-right'>
                    <li>
                        <a class="dropdown-toggle" type="button" data-toggle="dropdown" style="cursor: pointer">Profil
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= Yii::$app->request->baseUrl.'/tentang-kami' ?>">Tentang Kami</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/mengapa-ilf' ?>">Mengapa ILF?</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/visi-misi' ?>">Visi & Misi</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/filosofi-logo' ?>">Filosofi Logo</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/struktur' ?>">Struktur</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/guru' ?>">Guru</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/fasilitas' ?>">Fasilitas</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/alumni' ?>">Alumni</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="dropdown-toggle" type="button" data-toggle="dropdown" style="cursor: pointer">Fun Club
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= Yii::$app->request->baseUrl.'/outbond' ?>">English & Mandarin Outbond</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/morning-tea' ?>">English & Mandarin Morning Tea</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/cooking-class' ?>">English & Mandarin Cooking Class</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/cafe' ?>">English & Mandarin On Cafe</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/gardening' ?>">English & Mandarin Gardening</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/shopping' ?>">Shopping</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/dentist' ?>">Being a Dentist</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/news-reader' ?>">Being News Reader</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="dropdown-toggle" type="button" data-toggle="dropdown" style="cursor: pointer">Program
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= Yii::$app->request->baseUrl.'/diploma' ?>">Diploma 1</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/tk' ?>">English & Mandarin Golden Star (TK)</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/sd' ?>">English & Mandarin Fun Children (SD)</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/smp-sma' ?>">English & Mandarin For Teenager (SMP/SMA)</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/mahasiswa-umum' ?>">English & Mandarin For Adult (Mahasiswa/Umum)</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/conversation' ?>">English & Mandarin Conversation <br>(SD,SMP,SMA,Mahasiswa,Umum)</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/bussinees' ?>">English & Mandarin For Bussinees (Mahasiswa/Umum)</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/toefl' ?>">TOEFL - Preparation</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href='<?= Yii::$app->request->baseUrl.'/prestasi' ?>'>Prestasi</a>
                    </li>
                    <li>
                        <a class="dropdown-toggle" type="button" data-toggle="dropdown" style="cursor: pointer">Raport
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="ilf-lamongan">Siswa ILF Lamongan</a></li>
                            <li><a href="ilf-bojonegoro">Siswa ILF Bojonegoro</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="dropdown-toggle" type="button" data-toggle="dropdown" style="cursor: pointer">E-Learning
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= Yii::$app->request->baseUrl.'/karya-siswa-ilf' ?>">Karya Siswa ILF</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/english-game' ?>">English Game</a></li>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/download' ?>">Download</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="dropdown-toggle" type="button" data-toggle="dropdown" style="cursor: pointer">Galeri
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= Yii::$app->request->baseUrl.'/ilf-in-action' ?>">ILF In Action</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href='<?= Yii::$app->request->baseUrl.'/karir' ?>'>Karir</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

</div>
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?= Yii::$app->request->baseUrl.'/images/favicon1.png' ?>" />
    <?php $this->head() ?>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
</head>
<body>
<?php $this->beginBody() ?>
<?php
if (Yii::$app->controller->id == 'site') {
    $containerClass = 'noise-wrapper';
}
else {
    $containerClass = '';
};
?>
<div class='<?= $containerClass ?>'>

    <?= $this->render('navbar') ?>
    <div class='separator-shadow-bottom'>
        <img alt='' src='<?= Yii::$app->request->baseUrl ?>/images/shadow-separator-wide-bottom.png'>
    </div>
    <?= $content ?>
</div>
<footer id="main-footer" style="padding: 50px 0px;
    background-color: #246BA5;
    background: url(<?= Yii::$app->request->baseUrl ?>/images/backgrounds/hero-bg.jpg);
    background-repeat: repeat;
    margin-top: 30px;
    bottom: 0px;
    width: 100%;
    ">
    <?= $this->render('footer') ?>
</footer>

<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>

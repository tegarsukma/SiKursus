<section class='leaderboard leaderboard-style-one'>
    <div class='container'>
        <h1 class='animated fadeInDown'>International Language Foundation</h1>
        <h2 class='animated fadeInDown'>International Language Foundation (ILF) Indonesia</h2>
        <div class='relative-w'>
            <div class='loupe loupe-left animated fadeInLeft'>
                <img alt='' src='<?= Yii::$app->request->baseUrl ?>/images/showcase.png'>
            </div>
            <div class='loupe loupe-right animated fadeInRight'>
                <img alt='' src='<?= Yii::$app->request->baseUrl ?>/images/showcase.png'>
            </div>
            <div class='frame-browser animated bounceInUp'>
                <div class='frame-buttons'>
                    <div class='frame-button-close'></div>
                    <div class='frame-button-max'></div>
                    <div class='frame-button-min'></div>
                </div>
                <div class='frame-browser-image'>
                    <img alt='' src='<?= Yii::$app->request->baseUrl ?>/images/showcase.png'>
                </div>
            </div>
        </div>
    </div>
</section>
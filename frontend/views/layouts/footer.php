<div class="container">
    <div class="row">
        <div class="col-md-6">
            <ul class="footer-menu">

            </ul>
            <div class="copyright">
                <?= date("Y") ?> &copy; copyright by ILF Indonesia
            </div>
        </div>
        <div class="col-md-6">
            <ul class="footer-social">
                <li><a href="https://www.facebook.com/ilfindo"><i class="icon-facebook"></i></a></li>
                <li><a href="https://twitter.com/ilfindonesia"><i class="icon-twitter"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UCaiJEtNstBXVHLwJOAHvgBA/featured"><i class="icon-youtube"></i></a></li>
            </ul>
        </div>
    </div>
</div>
<?php

/* @var $this yii\web\View */

$this->title = 'ILF Indonesia - Visi & Misi';
?>
<div class='container'>
    <div class='slogan'>
        <h1>Visi & Misi</h1>
    </div>
    <hr>
</div>
</div>
<div class='container'>

    <div class='isotope-w' style="text-align: left;">
        VISI
        <br>
        “Menjadi lembaga kursus Bahasa Inggris Terbaik dan Terbesar Se-Asia Tenggara Pada Tahun 2040”
        <br>
        <br>
        <br>
        MISI
        <br>
        1. Membantu peserta didik mengembangkan kualitas diri untuk mencapai prestasi terbaik.
        <br>
        2. Membantu setiap insan ILF mengembangkan kualitas diri dan mewujudkan kesejahteraan bersama.
        <br>
        3. Mengedepankan IT dalam proses pembelajaran maupun manajemen perusahaan.
        <br>
        4. Menjadi sebuah lembaga kursus Bahasa Inggris asli asal Indonesia
        <br>
        yang disegani se Asia Tenggara.
        <br>
        <br>
        TUJUAN
        <br>
        1. Mendidik peserta didik mampu menguasai bahasa International agar mereka mampu bersaing baik dikanca Nasional
        maupun Internasional
        <br>
        2. Menyiapkan para genarasi emas bangsa yang berwawasan luas dan berkarakter
        <br>
        3. Memberikan pengajaran Bhs. Asing yang profesional dan berkualitas
        <br>
        4. Membantu pemerintah untuk mengurangi pengangguran dengan membuka lapangan kerja seluas – luasnya
        <br>
        5. Sabagai lembaga kursus bhs. International yang menjadi kebanggaan Indonesia.
        <br>
        <br>
    </div>
</div>
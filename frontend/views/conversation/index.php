<?php

/* @var $this yii\web\View */

$this->title = 'ILF Indonesia - English & Mandarin Conversation (SD,SMP,SMA,Mahasiswa,Umum)';
?>
<div class='container'>
    <div class='slogan'>
        <h1>English & Mandarin Conversation (SD,SMP,SMA,Mahasiswa,Umum)
        </h1>
    </div>
    <hr>
</div>
</div>
<div class='container'>

    <div class='isotope-w' style="text-align: left;">
        <div style="text-align:justify;"><img src="<?= Yii::$app->request->baseUrl ?>/images/cafe.jpg" width="350px"
                                              height="250px" style="float:left; margin:0 8px 4px 0;"/>
            Program yang didesain khusus bagi anda yang ingin meningkatkan kemampuan dalam berbicara Bahasa Inggris atau
            Mandarin dengan Lancar,baik dan benar.
        </div>
        <br>
        <br>
        <table width="100%" border="1" cellspacing="0" cellpadding="0" style="text-align: center">
            <tbody>
            <tr>
                <td valign="top" width="36">NO</td>
                <td valign="top" width="109">LEVEL</td>
                <td valign="top" width="113">DURASI</td>
                <td valign="top" width="393">TARGET PENGUASAAN</td>
            </tr>
            <tr>
                <td valign="top" width="36"> 1</td>
                <td valign="top" width="109"> Elementary</td>
                <td valign="top" width="113"> 6 Bulan</td>
                <td width="393">
                    <p dir="RTL">Siswa mampu berkomunikasi mengenai Conversation starters,
                        Asking and giving opinion, Agreeing and disagreeing, Asking for details, Asking permission,
                        Asking for and giving advice, Description, Occupation, Dreams, Traveling, Asking about place
                        and location, Direction, Dating and relationship, Shopping, Music and movie.<br/>
                    </p>
                </td>
            </tr>
            <tr>
                <td valign="top" width="36"> 2</td>
                <td valign="top" width="109"> Intermediate</td>
                <td valign="top" width="113"> 6 Bulan</td>
                <td valign="bottom" width="393">
                    <p dir="RTL">Siswa mampu berkomunikasi mengenai
                        Happiness, Sport, Big city ,Religion, Health, Family, Personal Quality, Renting house,
                        Culture ,Visiting tourzm place, Comparing, The memory, Goals, Rumors, Advertisement,
                        Science, and technology, Secret, Plagiarism, Celebrities. </p>
                </td>
            </tr>
            </tbody>
        </table>
        <br>
        <br>
        <br>
        <br>
        <br>
    </div>
</div>
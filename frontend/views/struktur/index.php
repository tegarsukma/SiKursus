<?php

/* @var $this yii\web\View */

$this->title = 'ILF Indonesia - Struktur';
?>
<div class='container'>
    <div class='slogan'>
        <h1>Struktur</h1>
    </div>
    <hr>
</div>
</div>
<div class='container'>

    <div class='isotope-w' style="text-align: left;">
        <table style="width: 455px;">
            <tbody>
            <tr style="height: 33px;">
                <td style="width: 187px; height: 33px;">Direktur</td>
                <td style="width: 10px; height: 33px;">:</td>
                <td style="width: 310px; height: 33px;">
                    <p>Ahmad Latif, S.Pd</p>
                </td>
            </tr>
            <tr style="height: 23px;">
                <td style="width: 187px; height: 23px;">&nbsp;</td>
                <td style="width: 10px; height: 23px;">&nbsp;</td>
                <td style="width: 310px; height: 23px;">&nbsp;</td>
            </tr>
            <tr style="height: 23px;">
                <td style="width: 187px; height: 23px;">Manager Marketing</td>
                <td style="width: 10px; height: 23px;">:</td>
                <td style="width: 310px; height: 23px;">Riya Agustina, S.Pd</td>
            </tr>
            <tr style="height: 23px;">
                <td style="width: 187px; height: 23px;">&nbsp;</td>
                <td style="width: 10px; height: 23px;">&nbsp;</td>
                <td style="width: 310px; height: 23px;">&nbsp;</td>
            </tr>
            <tr style="height: 23px;">
                <td style="width: 187px; height: 23px;">Manager&nbsp;Academic</td>
                <td style="width: 10px; height: 23px;">:</td>
                <td style="width: 310px; height: 23px;">Eny Yuliana</td>
            </tr>
            <tr style="height: 23px;">
                <td style="width: 187px; height: 23px;">&nbsp;</td>
                <td style="width: 10px; height: 23px;">&nbsp;</td>
                <td style="width: 310px; height: 23px;">&nbsp;</td>
            </tr>
            <tr style="height: 23px;">
                <td style="width: 187px; height: 23px;">Manager Keuangan</td>
                <td style="width: 10px; height: 23px;">:</td>
                <td style="width: 310px; height: 23px;">Nur Ainiya, S.E &amp; Helen yuniar,SE</td>
            </tr>
            <tr style="height: 23px;">
                <td style="width: 187px; height: 23px;">&nbsp;</td>
                <td style="width: 10px; height: 23px;">&nbsp;</td>
                <td style="width: 310px; height: 23px;">&nbsp;</td>
            </tr>
            <tr style="height: 23px;">
                <td style="width: 187px; height: 23px;">Staff Administrasi</td>
                <td style="width: 10px; height: 23px;">:</td>
                <td style="width: 310px; height: 23px;">Ayu Septiana Pu</td>
            </tr>
            </tbody>
        </table>
        <br>
        <br>
        <br>
        <br>
        <br>
    </div>
</div>
<?php

/* @var $this yii\web\View */

$this->title = 'ILF Indonesia - TOEFL – Preparation';
?>
<div class='container'>
    <div class='slogan'>
        <h1>TOEFL – Preparation
        </h1>
    </div>
    <hr>
</div>
</div>
<div class='container'>

    <div class='isotope-w' style="text-align: left;">
        Program yang didesain khusus untuk siswa yang akan mengikuti test Toefl dengan sistem pengajaran yang berfokus
        pada peningkatan kemampuan mengerjakan soal Toefl yang terdiri dari Mendengarkan (listening), Tata bahasa
        (structure) teks bacaan(reading) dengan baik dan benar.
        <br>
        <br>
        STUDENT’S BOOK
        <br>
        <br>
        Dalam program TOEFL PREPARATION , siswa menggunakan buku TOEFL yang telah dirancang dari sumber atau refrensi
        yang tepat.
        <br>
        <br>
        METHODOLOGY YANG MENARIK
        <br>
        <br>
        Methode pengajaran dalam program TOEFL PREPARATION adalah :
        <br>
        <br>
        – Berpusat pada pengembangan rasa percaya diri dan kemampuan mengerjakan soal TOEFL baik yang berbentuk
        Listening, Writing, Structure maupun Reading.
        <br>
        <br>
    </div>
</div>
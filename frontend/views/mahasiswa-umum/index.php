<?php

/* @var $this yii\web\View */

$this->title = 'ILF Indonesia - English & Mandarin For Adult (Mahasiswa/Umum)';
?>
<div class='container'>
    <div class='slogan'>
        <h1>English & Mandarin For Adult (Mahasiswa/Umum)
        </h1>
    </div>
    <hr>
</div>
</div>
<div class='container'>

    <div class='isotope-w' style="text-align: left;">
        <div style="text-align:justify;"><img src="<?= Yii::$app->request->baseUrl ?>/images/cafe.jpg" width="350px"
                                              height="250px" style="float:left; margin:0 8px 4px 0;"/>
            Program yang didesain khusus untuk siswa Dewasa terdiri dari 2 ( Dua ) level dengan sistem pengajaran yang
            berfokus pada peningkatan kemampuan Mendengar( Listening ), Berbicara (Speaking), Membaca(Reading), dan
            Menulis(Writing) dalam bahasa inggris dengan baik dan benar , aplikasikan bahasa inggris mereka untuk
            situasi di dunia nyata, lingkungan bisnis dan profesional secara percaya diri, tepat dan fasih dalam program
            ini.
            <br>
            <br>
            STUDENT’S BOOK
            <br>
            <br>
            Dalam program English For Adult, siswa menggunakan buku pelajaran Bahasa Inggris SPEAK OUT terdiri dari 2
            level yang berisi tema-tema menarik, sangat cocok dengan lingkungan kerja atau kehidupan sehari-hari orang
            dewasa. Buku SPEAK OUT ini telah di akui secara internasional dan diterbitkan oleh penerbit terbaik dunia
            PearsonLongman.
            <br>
            <br>
            ONLINE WORLD
            <br>
            <br>
            Dalam program English For Adult ini siswa dapat terus belajar kapanpun dan dimanapun secara online, siswa
            dapat mengakses materi listening Audio atau mengakses siaran dari TV ataupun Radio BBC.
            <br>
            <br>
            CD-ROM
            <br>
            <br>
            Dalam program English For Adult siswa akan mendapatkan CD-ROM yang berisi interaktif aktivity, Listening
            Audio sehingga siswa dapat meriview materi yang telah diajarkan Bapak/Ibu guru dirumah.
            <br>
            <br>
            METHODOLOGY YANG MENARIK
            <br>
            <br>
            Methode pengajaran dalam program English For Adult adalah :
            <br>
            <br>
            1. Berpusat pada pengembangan rasa percaya diri dan kemampuan berkomunikasi dalam Bahasa Inggris yang sesuai
            dengan dunia nyata, Lingkungan kerja atau bisnis.
            <br>
            <br>
            2. Mengembangkan dasar keterampilan berbahasa Inggris yang kuat, serta memperbaiki kefasihan Bahasa Inggris
            siswa yang diakui secara international.
            <br>
            <br>
            3. Meningkatkan kemampuan berkomunikasi dalam Bahasa Inggris seperti melakukan diskusi, memimpin rapat
            ataupun presentasi.
            <br>
            <br>
            4. Didalam kelas para guru kami menggunakan peralatan mengajar dengan tecnology canggih yaitu Active Teach
            yang dapat membuat proses belajar dikelas lebih interaktif, menarik, menyenangkan dan efektif seperti :
            Digital Picture Card, Interactive Whiteboard ( IWB ), Listening Audio, Digital task, Online movie, Dll.
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
    </div>
</div>
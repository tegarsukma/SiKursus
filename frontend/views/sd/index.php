<?php

/* @var $this yii\web\View */

$this->title = 'ILF Indonesia - English & Mandarin Fun Children (SD)';
?>
<div class='container'>
    <div class='slogan'>
        <h1>English & Mandarin Fun Children (SD)
        </h1>
    </div>
    <hr>
</div>
</div>
<div class='container'>

    <div class='isotope-w' style="text-align: left;">
        <div style="text-align:justify;"><img src="<?= Yii::$app->request->baseUrl ?>/images/sd.jpg" width="350px"
                                              height="250px" style="float:left; margin:0 8px 4px 0;"/>

            Program yang didesain khusus untuk siswa SD (Primary School) yang terdiri dari 6 (enam) level dengan sistem
            pengajaran yang berfokus pada peningkatan kemampuan mendengar (listening), Berbicara(speaking), Membaca
            (reading), dan Menulis (writing) dalam bahasa inggris dengan baik dan benar serta didukung dengan berbagai
            macam kegiatan Fun Club yang dapat membantu siswa menerapkan pengetahuan bahasa inggris dalam situasi
            pecakapan sesungguhnya dengan penuh percaya diri.
        </div>
        <br>
        <br>
        STUDENT’S BOOK
        <br>
        <br>
        Dalam program ini siswa menggunakan buku pelajaran bahasa inggris anak-anak yang diakui secara international
        yaitu Our Discovery Island. Buku ini terdiri dari 6 level dan diterbitkan oleh penerbit terbaik dunia Longman
        Pearson. Dalam program ini siswa akan sangat antusias dan tertarik terus belajar bahasa inggris karena mereka
        akan ditemani tokoh karakter yang lucu dan menggemaskan seperti : Oscar, Millie, Rita, Zak dan Waldo si dragon.
        Siswa juga akan mendapatkan workbook yang berisi soal atau tugas yang dapat dikerjakan di kelas maupun dirumah.
        siswa juga akan diajak mengenal berbagai macam budaya yang ada didunia ( cross culture understanding )
        <br>
        <br>
        ONLINE WORLD
        <br>
        <br>
        Dalam program English Fun Children ini siswa dapat terus belajar dirumah secara online, siswa akan sangat
        menikmati petualangan belajar Bahasa Inggris bersama character-character yang ada didunia maya, siswa akan
        diajak bermain puzzle atau game yang seru.
        <br>
        <br>
        CD-ROM INTERAKTIF
        <br>
        <br>
        Dalam program English Fun Children siswa akan mendapatkan CD-ROM yang berisi interaktif aktivity dimana siswa
        dapat meriview matari yang telah diajarkan Bapak/Ibu guru dikelas bersama orang tua dirumah.
        <br>
        <br>
        METHODOLOGY YANG MENARIK
        <br>
        <br>
        Methode pengajaran dalam program English Fun Children berpusat pada pengembangan rasa percaya diri dan kemampuan
        berkomunikasi dalam bahasa Inggris, Membangun dasar keterampilan berbahasa Inggris yang kuat, serta memperbaiki
        pelafalan siswa dengan baik dan benar. Didalam jelas para guru kami menerapkan methode 5Ps ( Presetational,
        Practice, Production, Personalization dan Pronuncation ) serta menggunakan peralatan mengajar dengan tecnology
        canggih yaitu Active Teach yang dapat membuat proses belajar dikelas lebih interaktif, menarik, menyenangkan dan
        efektif seperti :
        <br>
        <br>
        – Digital Picture Card
        <br>
        <br>
        – Interactive Whiteboard ( IWB )
        <br>
        <br>
        – Listening task
        <br>
        <br>
        – Digital atau interaktif tugas
        <br>
        <br>
        – English song dan karaoke version
        <br>
        <br>
        – Digital English story, Dll.
        <br>
        <br>
        <table width="100%" border="1" cellspacing="0" cellpadding="0" style="text-align: center">
            <tbody>
            <tr>
                <td valign="top" width="38">
                    <p align="center">No</p>
                </td>
                <td valign="top" width="151">
                    <p align="center">LEVEL</p>
                </td>
                <td valign="top" width="744">
                    <p align="center">TARGET PENGUASAAN</p>

                </td>
            </tr>
            <tr>
                <td valign="top" width="38">
                    <p align="center">1</p>
                </td>
                <td valign="top" width="151">English Fun Children I</td>
                <td valign="top" width="744">Siswa mampu berkomunikasi dalam Bhs. Inggris Sederhana dengan topik: My birthday, At school, My family, My body, Pets, My house, Food, I’m exited.</td>
            </tr>
            <tr>
                <td valign="top" width="38">
                    <p align="center">2</p>
                </td>
                <td valign="top" width="151">English Fun Children 2</td>
                <td valign="top" width="744">Siswa mampu berkomunikasi dalam Bhs. Inggris lebih baik dengan topik: My toys, My family, Move your body, My face, Animals, Food, Clothes, Weather.</td>
            </tr>
            <tr>
                <td valign="top" width="38">
                    <p align="center">3</p>
                </td>
                <td valign="top" width="151">English Fun Children 3</td>
                <td valign="top" width="744">Siswa mampu berkomunikasi dalam Bhs. Inggris dengan lancar  dengan topik: Nature, My Family, Pets, Home, Clothes, Sports, Food, Things we do.</td>
            </tr>
            <tr>
                <td valign="top" width="38">
                    <p align="center">4</p>
                </td>
                <td valign="top" width="151">English Fun Children 4</td>
                <td valign="top" width="744">Siswa mampu berkomunikasi dalam Bhs. Inggris dengan lancar  dan alami dengan topik: Free time, Wild animals, The seasons, My week, Jobs, In the rain forest, Feelings, Action.</td>
            </tr>
            <tr>
                <td valign="top" width="38">
                    <p align="center">5</p>
                </td>
                <td valign="top" width="151">English Fun Children 5</td>
                <td valign="top" width="744">Siswa mampu berkomunikasi dalam Bhs. Inggris dengan Sempurna dengan topik : Friends, My life, Free time, Around the world, Shopping, Party time, School, Entertaiment.</td>
            </tr>
            <tr>
                <td valign="top" width="38">
                    <p align="center">6</p>
                </td>
                <td valign="top" width="151">English Fun Children 6</td>
                <td valign="top" width="744">Siswa mampu berkomunikasi dengan Baik,Lancar,Alami dan Sempurna dengan Topik : Adventure camp, Wildlife park, Where we live, Good days and bad days, Trips, Arts and entertainment, Space, The environtment.</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<?php

/* @var $this yii\web\View */

$this->title = 'ILF Indonesia - English & Mandarin For Teenager (SMP/SMA)';
?>
<div class='container'>
    <div class='slogan'>
        <h1>English & Mandarin For Teenager (SMP/SMA)
        </h1>
    </div>
    <hr>
</div>
</div>
<div class='container'>

    <div class='isotope-w' style="text-align: left;">
        <div style="text-align:justify;"><img src="<?= Yii::$app->request->baseUrl ?>/images/smpsma.jpg" width="350px"
                                              height="250px" style="float:left; margin:0 8px 4px 0;"/>
            Program yang didesain khusus untuk siswa remaja atau setingkat SMP dan SMA yang terdiri dari 4 ( Empat )
            level dengan sistem pengajaran yang berfokus pada peningkatan kemampuan Mendengar (listening),
            Berbicara(speaking), Membaca(reading), dan Menulis(Writing) dalam bahasa inggris dengan baik dan benar serta
            didukung dengan berbagai macam kegiatan Fun Club yang dapat membantu siswa menerapkan pengetahuan bahasa
            inggris dalam situasi percakapan sesungguhnya dengan penuh percaya diri.
            <br>
            <br>
            Dalam program English For Teenager, siswa menggunakan buku pelajaran bahasa inggris NEW CHALLANGES trdiri
            dari 4 level yang berisi tema-tema menarik, sangat cocok dengan dunia remaja, serta dapat membentuk karakter
            mereka menjadi pribadai yang pantang menyerah, Berani bermimpi besar, percaya diri, disiplin, sosialis,
            inisiatif, kreatif, dan mempunyai integritas yang baik.
            <br>
            <br>
            Buku NEW CHALLANGES ini telah diakui secara international dan diterbitkan oleh penerbit terbaik dunia
            PearsonLongman.
            <br>
            <br>
            ONLINE WORLD
            <br>
            <br>
            Dalam program English For Teenager ini siswa dapat terus belajar dirumah secara online, siswa dapat
            mengakses materi Listening Audio atau Berbagai macam materi tambahan serta dapat bermain game interaktif
            dalam bahasa inggris yang sangat menyenangkan.
            <br>
            <br>
            CD – ROM
            <br>
            <br>
            Dalam program English For Teenager siswa akan mendapatkan CD-ROM yang berisi interaktif aktifity, Listening
            Audio sehingga siswa dapat meriview materi yang telah diajarkan Bapak/Ibu guru dirumah.
            <br>
            <br>
            METHODOLOGY YANG MENARIK
            <br>
            <br>
            Methode pengajaran dalam program English For Teenager adalah
            <br>
            <br>
            1. Berpusat pada pengembangan rasa percaya diri dan kemampuan berkomunikasi dalam Bahasa Inggris dengan
            sempurna
            <br>
            <br>
            2. Mengembangkan dasar keterampilan berbahasa inggris yang kuat, serta memperbaiki kefasihan bahasa inggris
            siswa yang diakui secara international
            <br>
            <br>
            3. Memahami inti dari berbagai jenis teks dalam rambu-rambu, surat kabar, majalah, iklan atau cerita pendek
            <br>
            <br>
            4. Melatih siswa mahir dalam menulis Berbahasa inggris seperti menulis laporan, pesan singkat, pengalaman,
            peristiwa, cerita dan surat
            <br>
            <br>
            5. Didalam kelas para guru kami menggunakan peralatan mengajar dengan tecnology canggih yaitu Active Teach
            yang dapat membuat proses belajar dikelas lebih interaktif,menarik, menyenangkan, dan efektif seperti :
            Digital picture card, Interactive whiteboard(IWB), Listening audio, Digital task, Online movie, dll.
            <br>
            <br>
            <table width="100%" border="1" cellspacing="0" cellpadding="0" style="text-align: center">
                <tbody>
                <tr>
                    <td valign="top" width="37">
                        <p style="text-align: center;">No.</p>
                    </td>
                    <td valign="top" width="162">
                        <p style="text-align: center;">LEVEL</p>
                    </td>
                    <td valign="top" width="486">
                        <p style="text-align: center;">TARGET PENGUASAAN</p>
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="37">
                        <p style="text-align: center;">1.</p>
                    </td>
                    <td valign="top" width="162">English For Teenager I</td>
                    <td valign="top" width="486">Siswa mampu berkomunikasi dalam Bhs. Inggris dengan lancar, baik dan
                        benar dengan topik: The Challenge, Exercise, Risk, Out and About, The Weather, Expeditions,
                        Helping, Television.
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="37">
                        <p style="text-align: center;">2.</p>
                    </td>
                    <td valign="top" width="162">English For Teenager 2</td>
                    <td valign="top" width="486">Siswa mampu berkomunikasi dalam Bhs. Inggris dengan lancar, baik dan
                        benar dengan topik: Our World, Neighbours, Life Stories, Mysteries, Performers, Looks,
                        Technology, Holidays.
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="37">
                        <p style="text-align: center;">3.</p>
                    </td>
                    <td valign="top" width="162">English For Teenager 3</td>
                    <td valign="top" width="486">Siswa mampu berkomunikasi dalam Bhs. Inggris dengan lancar, baik dan
                        benar dengan topik: Schools, Talent, Health, People, On the Move, Films and Books, Music,
                        Discoveries.
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="37">
                        <p style="text-align: center;">4.</p>
                    </td>
                    <td valign="top" width="162">English For Teenager 4</td>
                    <td valign="top" width="486">Siswa mampu berkomunikasi dalam Bhs. Inggris dengan lancar, baik dan
                        benar dengan topik: Animal Talk, Social Networking, www.radiochill.org.</p>
                        </td>
                </tr>
                </tbody>
            </table>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
    </div>
</div>
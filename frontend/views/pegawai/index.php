<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Url;

?>
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <h1>pegawai/index</h1>
            <div class="table-responsive">
                <?= GridView::widget([
                    'layout' => '{summary}{pager}{items}{pager}',
                    'dataProvider' => $dataProvider,
                    'pager' => [
                        'class' => yii\widgets\LinkPager::className(),
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last'                ],
                    //'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                    'headerRowOptions' => ['class'=>'x'],
                    'columns' => [

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => "{view}",
                            'urlCreator' => function($action, $model, $key, $index) {
                                // using the column name as key, not mapping to 'id' like the standard generator
                                $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                                $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                                return Url::toRoute($params);
                            },
                            'contentOptions' => ['nowrap'=>'nowrap']
                        ],
                        'nip',
                        'nama',
                        'jenis_kelamin',
                        [
                            'attribute'=>'jabatan',
                            'value'=>function($data){
                                return \app\models\Jabatan::findOne($data->jabatan)->jabatan;
                            }
                        ],
                        [
                            'attribute'=>'tempat_lab',
                            'value'=>function($data){
                                return \app\models\Lab::findOne($data->tempat_lab)->nama_lab;
                            }
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>



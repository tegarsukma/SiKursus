<?php

use dmstr\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var app\models\Pegawai $model
 */
$copyParams = $model->attributes;

$this->title = Yii::t('app', 'Pegawai');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pegawais'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="container">
    <div class="row">
        <div class="giiant-crud pegawai-view">

            <!-- flash message -->
            <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
                <span class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
                    <?= \Yii::$app->session->getFlash('deleteError') ?>
        </span>
            <?php endif; ?>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail </h3>
                </div>

                <div class="box-body">


                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'nip',
                            'nama',
                            'jenis_kelamin',
                            [
                                'attribute'=>'jabatan',
                                'value'=>\app\models\Jabatan::findOne($model->jabatan)->jabatan,
                            ],
                            [
                                'attribute'=>'tempat_lab',
                                'value'=>\app\models\Lab::findOne($model->tempat_lab)->nama_lab

                            ],
                        ],
                    ]); ?>

                    <hr/>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Url;

?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1>Jadwal Praktikum</h1>

            <p>
            <div class="box-body">
                <div class="table-responsive">
                    <?= GridView::widget([
                        'layout' => '{summary}{pager}{items}{pager}',
                        'dataProvider' => $dataProvider,
                        'pager' => [
                            'class' => yii\widgets\LinkPager::className(),
                            'firstPageLabel' => 'First',
                            'lastPageLabel' => 'Last'],
                        //'filterModel' => $searchModel,
                        'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                        'headerRowOptions' => ['class' => 'x'],
                        'columns' => [
                            'jadwal:ntext',
                            [
                                'attribute' => 'dosen',
                                'value' => function ($data) {
                                    return \app\models\Pegawai::findOne($data->dosen)->nama;
                                }
                            ],
                            'hari',
                            'waktu',
                            'kelas',
                            [
                                'attribute' => 'lab',
                                'value' => function ($data) {
                                    return \app\models\Lab::findOne($data->lab)->nama_lab;
                                }
                            ]
                        ],
                    ]); ?>
                </div>
            </div>
            </p>
        </div>
    </div>
</div>
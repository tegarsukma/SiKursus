<?php

/* @var $this yii\web\View */

$this->title = 'ILF Indonesia - English & Mandarin Golden Star (TK)';
?>
<div class='container'>
    <div class='slogan'>
        <h1>English & Mandarin Golden Star (TK)
        </h1>
    </div>
    <hr>
</div>
</div>
<div class='container'>

    <div class='isotope-w' style="text-align: left;">
        <div style="text-align:justify;"><img src="<?= Yii::$app->request->baseUrl ?>/images/shopping.jpg" width="350px"
                                              height="250px" style="float:left; margin:0 8px 4px 0;"/>
            Program yang didesain khusus untuk membantu siswa TK ( nursery school ) belajar berkomunikasi dalam Bahasa
            Inggris melalui pendekatan empat skill yang dapat membangun dan meningkatkan kemampuan Bahasa Inggris mereka
            yaitu Pre listening, Pre speaking, Pre reading, Pre writing.
            <br>
            <br>
            <br>STUDENT’S BOOK
            <br>
            <br>Dalam program ini siswa menggunakan buku pelajaran bahasa Inggris anak-anak yang diakui secara
            international yaitu My Little Island. Buku ini terdiri dari 3 level dan diterbitkan oleh penerbit terbaik
            dunia Longman Pearson. Dalam program ini siswa akan sangat antusias dan tertarik terus belajar bahasa
            Inggris karena mereka akan ditemani tokoh karakter yang lucu dan menggemaskan seperti : Kimmy, Timmy, Billy,
            Lilly, Lou dan Sou serta maskot Puppet.

        </div>
        <br>
        ONLINE WORLD
        <br>
        <br>
        Dalam program English Golden Star ini siswa dapat terus belajar di rumah secara online, siswa akan sangat
        menikmati petualangan belajar Bahasa Inggris bersama character-character yang ada didunia maya, siswa akan
        diajak bermain puzzle atau game yang seru.
        <br>
        <br>
        CD-ROM INTERAKTIF
        <br>
        <br>
        Dalam program English Golden Star siswa akan mendapatkan CD-ROM yang berisi interaktif aktivity dimana siswa
        dapat meriview materi yang telah diajarkan Bapak/Ibu guru dikelas bersama orang tua dirumah.
        <br>
        <br>
        METHODOLOGY YANG MENARIK
        <br>
        <br>
        Methode pengajaran dalam program English Golden Star berpusat pada pengembangan rasa percaya diri dan kemampuan
        berkomunikasi dalam Bahasa Inggris pada diri setiap siswa. Didalam kelas para guru kami menggunakan peralatan
        mengajar dengan tecnology canggih yaitu Active Teach yang dapat membuat proses belajar dikelas lebih interaktif,
        menarik, menyenangkan, dan efektif seperti :
        <br>
        <br>
        – Digital Picture card
        <br>
        <br>
        – Interactive Whiteboard (IWB)
        <br>
        <br>
        – Listening task
        <br>
        <br>
        – Digital atau interaktif task
        <br>
        <br>
        – English song dan Karaoke version
        <br>
        <br>
        – Digital English story, Dll.
        <br>
        <br>
        <center><table style="width: 100%; text-align:center" border="1">
            <tbody>
            <tr style="height: 23px;">
                <td style="height: 23px; width: 30px;">No</td>
                <td style="height: 23px; width: 60px;">Level</td>
                <td style="height: 23px; width: 80px;">Durasi</td>
                <td style="height: 23px; width: 659px;">Target Penguasaan</td>
            </tr>
            <tr style="height: 43px;">
                <td style="height: 43px; width: 25px;">1.</td>
                <td style="height: 43px; width: 41px;">1</td>
                <td style="height: 43px; width: 48px;">6 bulan</td>
                <td style="height: 43px; width: 659px;">Siswa mampu memahami beberapa kosakata dan menggunakannya dalam percakapan seederhana tentang : My Class, My Family, My Room, My Toys, My Face, Food, Animals, My Backyard</td>
            </tr>
            <tr style="height: 43px;">
                <td style="height: 43px; width: 25px;">2.&nbsp;</td>
                <td style="height: 43px; width: 41px;">2&nbsp;</td>
                <td style="height: 43px; width: 48px;">6 bulan&nbsp;</td>
                <td style="height: 43px; width: 659px;">Siswa mampu memahami beberapa kosakata dan menggunakannya dalam percakapan seederhana tentang : My School, Family, Play Time, My House, My Body, Time To Eat, On The Farm, The Weather</td>
            </tr>
            <tr style="height: 43px;">
                <td style="height: 43px; width: 25px;">3.&nbsp;</td>
                <td style="height: 43px; width: 41px;">3&nbsp;</td>
                <td style="height: 43px; width: 48px;">6 bulan&nbsp;</td>
                <td style="height: 43px; width: 659px;">Siswa mampu memahami beberapa kosakata dan menggunakannya dalam percakapan seederhana tentang : At School, Workers, My Town, Clothes, Feelings, Healthy Food, The Zoo, Places</td>
            </tr>
            </tbody>
        </table>
        </center>
    </div>
</div>
<?php

/* @var $this yii\web\View */

$this->title = 'ILF Indonesia - Tentang Kami';
?>
<div class='container'>
    <div class='slogan'>
        <h1>Tentang Kami</h1>
    </div>
    <hr>
</div>
</div>
<div class='container'>

    <div class='isotope-w' style="text-align: left;">
        International Language Foundation (ILF) adalah sebuah lembaga kursus Bahasa Inggris dan Mandarin yang
        berkualitas dan profesional yang telah dipercaya beberapa instansi baik dalam negeri maupun luar negeri
        (PT.HOTROX Australia) untuk membimbing mereka dalam belajar Bhs.Inggris atau Mandarin.ILF menyediakan program
        pembelajaran mulai dari tingkat TK, SD, SMP, SMA, Mahasiswa dan Umum. ILF didirikan pada tanggal 8 Maret 2009.
        Dengan No.Izin Diknas .421.9/2144/413.101/2012.
        <br><br>
        International Language Foundation (ILF) didirikan oleh seorang guru Bhs.Inggris (Ahmad Latif,SPd) yang sudah
        berpengalaman dalam membimbing peserta didiknya untuk meraih kesuksesan dalam belajar Bhs.Inggris.Bahkan sejak
        masih duduk di bangku SMA beliau sudah mengajar Bhs.Inggris dari rumah ke rumah.
        <br><br>
        Pada awal berdirinya International Language Foundation hanya bermodalkan Rp.1500.000 (Satu juta lima ratus ribu
        rupiah) dimana uang itu merupakan pinjaman dari seorang temanya yang akan dipakai biaya wisuda sebesar satu juta
        rupiah,sedangkan yang lima ratus ribu rupiah dipinjam dari kepala sekolahnya tempat beliau mengajar.Pada awal
        buka ILF menempati sebuah rumah yang hanya berukuran 6 x 12 meter yang beralamat di jl. Merpati Gg. Louhan akan
        tetapi tidak lama kemudian pindah ke Jl. Dr. Wahidin Sudiro Husodo No. 192 Lamongan. Setelah berjalan satu tahun
        setengah ILF pindah ke tempat yang lebih besar, bagus dan sangat strategis yang sampai sekarang masih ditempati
        tepat berada di Jl. Dr. Wahidin Sudiro Husodo No. 133 Lamongan.
        <br><br>
        Pada awal berdiri ILF hanya mendapatkan dua siswa akan tetapi dengan berjalanya waktu kini International
        Language Foundation telah membimbing ribuan siswa.
        <br><br>
        International Language Foundation (ILF) terus berusaha menjadi sebuah lembaga kursus bhs.Inggris dan Mandarin
        asal Indonesia yang dapat menjadi kebanggaan bangsa Indonesia dan mampu membawa nama harum Indonesia baik
        dikanca nasional maupun international. semoga!
    </div>
</div>
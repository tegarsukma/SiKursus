<?php

/* @var $this yii\web\View */

$this->title = 'ILF Indonesia - Diploma 1';
?>
<div class='container'>
    <div class='slogan'>
        <h1>Diploma 1</h1>
    </div>
    <hr>
</div>
</div>
<div class='container'>

    <div class='isotope-w' style="text-align: left;">
        <center><img src="<?= Yii::$app->request->baseUrl ?>/images/d1a.jpg" /></center>
        <br>
        <center><img src="<?= Yii::$app->request->baseUrl ?>/images/d1b.jpg" /></center>
        <br>
        <center><img src="<?= Yii::$app->request->baseUrl ?>/images/d1c.jpg" /></center>
    </div>
</div>
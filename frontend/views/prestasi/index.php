<?php

/* @var $this yii\web\View */

$this->title = 'ILF Indonesia - Prestasi';
?>
<div class='container'>
    <div class='slogan'>
        <h1>PRESTASI
        </h1>
    </div>
    <hr>
</div>
</div>
<div class='container'>

    <div class='isotope-w' style="text-align: left;">
        Prestasi Siswa ILF
        <br>
        <br>
        <ul>
            1. Juara 1 Olimpiade Bhs.Inggris Tk.SMA Se -Kab.Lamongan Th.2011 (Dias Tiara Putri Utomo)
            <br>
            2. Juara 1 Pidato Bhs.Inggris Tk.SMP Se-Kab.Lamongan Th.2011 (Chabib Fahri Al-Bab)
            <br>
            3. Juara 1 S2MEC Bahasa Inggris Tk.SD Se -Kab.Lamongan Th.2012 (M.Arsy Reza Suyudi)
            <br>
            4. Juara 1 English Telling Story Tk. SD Se- Kab.Lamongan Th.2013 (Hasnah Mazaya Anandafa Arimi)
            <br>
            5. Juara 1 Olimpiade Bhs.Inggris Tk.SD Se -Kab.Lamongan Th.2013 (Bima Putra Dharma)
            <br>
            6. Juara 1 Olimpiade Bhs.Inggris Tk.SD Se- Kab.Lamongan Th.2013 (M.Arsy Rza Suyudi)
            <br>
            7. Juara 1 Olimpiade Bhs.Inggris Tk SD Se-.Kab.Lamongan Th.2013 (Elizabeth C.D)
            <br>
            8. Juara 1 S2MEC Bahasa Inggris Tk.SD Se-.Kab.Lamongan Th. 2013 (M.Arsy Reza Suyudi)
            <br>
            9. Juara 1 Pidato Bhs.Inggris Tk.SMP Se -.Kab.Lamongan TH.2013 (Yoshinta)
            <br>
            10. Juara 2 Pidato Bahasa Inggris Tk.SMA Se-Jawa timur Th. 2011 (Dias Tiara Putri Utomo)
            <br>
            11. Juara 2 English Competition Tk SD Se-Kab.Gresik Th.2013 (M.Arsy Reza Suyudi)
            <br>
            12. Juara 2 English Story Telling Tk.SD Se- Kab.Lamongan Th. 2013 (Nisrina Shabry Habibie)
            <br>
            13. Juara 3 English Story Telling Tk.SD Se- Kab.Lamongan Th.2013 (Elizabeth C.D)
            <br>
            14. Juara 3 Pidato Bahasa Inggris Tk.SMP Se-Kab.Lamongan Th.2013 (Winda Furidatul Husnah)
            <br>
            15. Juara harapan 1S2MEC Bhs.Inggris Tk.SMP Se-Kab.Lamongan Th.2012 (Aurora)
            <br>
            16. Semi Final American Corner Tk .Nasional Th.2011 (Yoshinta )
        </ul>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
</div>
</div>
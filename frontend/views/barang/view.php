<?php

use dmstr\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var app\models\BarangInventaris $model
 */
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="giiant-crud barang-inventaris-view">

                <!-- flash message -->
                <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
                    <span class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
                        <?= \Yii::$app->session->getFlash('deleteError') ?>
        </span>
                <?php endif; ?>
                <div class="clearfix crud-navigation">
                    <div class="box-body">
                        <h1>Detail Barang</h1>
                        <?php $this->beginBlock('app\models\BarangInventaris'); ?>


                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute'=>'foto',
                                    'format'=>'raw',
                                    'value'=>Html::img("../../../backend/web/uploads/barang/foto/".$model->foto,[
                                        'width'=>250,
                                        'height'=>'auto',
                                        'class'=>'img img-bordered'
                                    ]),
                                ],
                                'id',
                                'serial_number',
                                'nama_alat',
                                'jumlah',
                                'merk',
                                [
                                    'attribute' => 'nama_lab',
                                    'value' => \app\models\Lab::findOne($model->nama_lab)->nama_lab,
                                ],
                                'tanggal_masuk',

                            ],
                        ]); ?>


                        <hr/>

                        <?php $this->endBlock(); ?>



                        <?= Tabs::widget(
                            [
                                'id' => 'relation-tabs',
                                'encodeLabels' => false,
                                'items' => [[
                                    'label' => '<b class=""># ' . $model->id . '</b>',
                                    'content' => $this->blocks['app\models\BarangInventaris'],
                                    'active' => true,
                                ],]
                            ]
                        );
                        ?>        </div>

                </div>


            </div>
        </div>
    </div>
</div>

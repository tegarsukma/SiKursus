<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Url;

?>


<div class="container">
    <div class="row">
        <div class="col-lg-12">
        <p>
            <h1>barang/index</h1>
            <div class="box-body">
                <div class="table-responsive">
                    <?= GridView::widget([
                        'layout' => '{summary}{pager}{items}{pager}',
                        'dataProvider' => $dataProvider,
                        'pager' => [
                            'class' => yii\widgets\LinkPager::className(),
                            'firstPageLabel' => 'First',
                            'lastPageLabel' => 'Last'],
                        //'filterModel' => $searchModel,
                        'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                        'headerRowOptions' => ['class' => 'x'],
                        'columns' => [

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{view}',
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    // using the column name as key, not mapping to 'id' like the standard generator
                                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
                                    $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                                    return Url::toRoute($params);
                                },
                                'contentOptions' => ['nowrap' => 'nowrap']
                            ],
                            'serial_number',
                            'nama_alat',
                            'jumlah',
                            'merk',
                            [
                                'attribute'=>'nama_lab',
                                'value'=>function ($barang){
                                    return \app\models\Lab::findOne($barang->nama_lab)->nama_lab;
                                }
                            ],
                            'tanggal_masuk',
                        ],
                    ]); ?>
                </div>
            </div>
        </p>
    </div>
</div>
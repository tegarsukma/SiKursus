<?php

/* @var $this yii\web\View */

$this->title = 'ILF Indonesia';
?>
<section class='leaderboard leaderboard-style-one'>
    <div class='container'>
        <h1 class='animated fadeInDown'>International Language Foundation</h1>
        <h2 class='animated fadeInDown'>International Language Foundation (ILF) Indonesia</h2>
        <div class='relative-w'>
            <div class='loupe loupe-left animated fadeInLeft'>
                <img alt='' src='<?= Yii::$app->request->baseUrl ?>/images/showcase.png'>
            </div>
            <div class='loupe loupe-right animated fadeInRight'>
                <img alt='' src='<?= Yii::$app->request->baseUrl ?>/images/showcase.png'>
            </div>
            <div class='frame-browser animated bounceInUp'>
                <div class='frame-buttons'>
                    <div class='frame-button-close'></div>
                    <div class='frame-button-max'></div>
                    <div class='frame-button-min'></div>
                </div>
                <div class='frame-browser-image'>
                    <img alt='' src='<?= Yii::$app->request->baseUrl ?>/images/showcase.png'>
                </div>
            </div>
        </div>
    </div>
</section>
<div class='separator-shadow-top above-screenshot-shadow'>
    <img alt='' src='<?= Yii::$app->request->baseUrl ?>/images/shadow-separator-wide-top.png'>
</div>
</div>
<div class='area-content'>
    <div class='container'>
        <div class='separator-shadow-top sub-screenshot-shadow'>
            <img alt='' src='<?= Yii::$app->request->baseUrl ?>/images/shadow-separator-wide-top.png'>
            <div class='iconed-features lift-on-hover style-1'>
                <div class='row'>
                    <div class='col-md-4'>
                        <div class='iconed-feature'>
                            <div class='feature-icon-w'>
                                <i class='icon-group'></i>
                            </div>
                            <h4>Tentang Kami</h4>
                            <p>Profil Tentang International Language Foundation (ILF)</p>
                            <div class='btn-show-on-parent-hover'>
                                <a class='read-more-link' href='<?= Yii::$app->request->baseUrl."/tentang-kami"?>'>Read More</a>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-4'>
                        <div class='iconed-feature'>
                            <div class='feature-icon-w'>
                                <i class='icon-question'></i>
                            </div>
                            <h4>Mengapa ILF?</h4>
                            <p>Mengapa Harus International Language Foundation (ILF)?</p>
                            <div class='btn-show-on-parent-hover'>
                                <a class='read-more-link' href='<?= Yii::$app->request->baseUrl."/mengapa-ilf"?>'>Read More</a>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-4'>
                        <div class='iconed-feature'>
                            <div class='feature-icon-w'>
                                <i class='icon-rocket'></i>
                            </div>
                            <h4>Visi & Misi</h4>
                            <p>Visi & Misi International Language Foundation (ILF)</p>
                            <div class='btn-show-on-parent-hover'>
                                <a class='read-more-link' href='<?= Yii::$app->request->baseUrl."/visi-misi"?>'>Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

/* @var $this yii\web\View */

$this->title = 'ILF Indonesia - ILF in Action';
?>
<div class='container'>
    <div class='slogan'>
        <h1>ILF in Action
        </h1>
    </div>
    <hr>
</div>
</div>
<div class='container'>

    <div class='isotope-w'>
        <div class='item filter-html filter-logo'>
            <div class='portfolio-item portfolio-style-3'>
                <div class='frame-browser'>
                    <figure>
                        <img alt='' src='<?= Yii::$app->request->baseUrl . "/images/cafe.jpg" ?>'>
                        <figcaption>
                            <h5 class='fp-title'>
                                On Cafe
                            </h5>

                            <div class='fp-buttons'>
                                <a class='btn btn-default btn-sm fp-lightbox-btn swipebox hidden-mobile-ib'
                                   href='<?= Yii::$app->request->baseUrl . "/images/cafe.jpg" ?>'
                                   title='Lightbox usage example'>
                                    <i class='icon-zoom-in'></i>
                                    View Bigger
                                </a>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
        <div class='item filter-design'>
            <div class='portfolio-item portfolio-style-3'>
                <div class='frame-browser'>
                    <figure>
                        <img alt='' src='<?= Yii::$app->request->baseUrl . "/images/bussinees.jpg" ?>'>
                        <figcaption>
                            <h5 class='fp-title'>
                                Bussinees
                            </h5>

                            <div class='fp-buttons'>
                                <a class='btn btn-default btn-sm fp-lightbox-btn swipebox hidden-mobile-ib'
                                   href='<?= Yii::$app->request->baseUrl . "/images/bussinees.jpg" ?>'
                                   title='Lightbox usage example'>
                                    <i class='icon-zoom-in'></i>
                                    View Bigger
                                </a>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
        <div class='item filter-design filter-development'>
            <div class='portfolio-item portfolio-style-3'>
                <div class='frame-browser'>
                    <figure>
                        <img alt='' src='<?= Yii::$app->request->baseUrl . "/images/cooking-class.jpg" ?>'>
                        <figcaption>
                            <h5 class='fp-title'>
                                Coking Class
                            </h5>

                            <div class='fp-buttons'>
                                <a class='btn btn-default btn-sm fp-lightbox-btn swipebox hidden-mobile-ib'
                                   href='<?= Yii::$app->request->baseUrl . "/images/cooking-class.jpg" ?>'
                                   title='Lightbox usage example'>
                                    <i class='icon-zoom-in'></i>
                                    View Bigger
                                </a>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
        <div class='item filter-design'>
            <div class='portfolio-item portfolio-style-3'>
                <div class='frame-browser'>
                    <figure>
                        <img alt='' src='<?= Yii::$app->request->baseUrl . "/images/dentist.jpg" ?>'>
                        <figcaption>
                            <h5 class='fp-title'>
                                Being Doctor
                            </h5>

                            <div class='fp-buttons'>
                                <a class='btn btn-default btn-sm fp-lightbox-btn swipebox hidden-mobile-ib'
                                   href='<?= Yii::$app->request->baseUrl . "/images/dentist.jpg" ?>'
                                   title='Lightbox usage example'>
                                    <i class='icon-zoom-in'></i>
                                    View Bigger
                                </a>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
        <div class='item filter-development filter-photoshop'>
            <div class='portfolio-item portfolio-style-3'>
                <div class='frame-browser'>
                    <figure>
                        <img alt='' src='<?= Yii::$app->request->baseUrl . "/images/gardening.jpg" ?>'>
                        <figcaption>
                            <h5 class='fp-title'>
                                Gardening
                            </h5>

                            <div class='fp-buttons'>
                                <a class='btn btn-default btn-sm fp-lightbox-btn swipebox hidden-mobile-ib'
                                   href='<?= Yii::$app->request->baseUrl . "/images/gardening.jpg" ?>'
                                   title='Lightbox usage example'>
                                    <i class='icon-zoom-in'></i>
                                    View Bigger
                                </a>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
        <div class='item filter-design filter-html filter-logo filter-photoshop'>
            <div class='portfolio-item portfolio-style-3'>
                <div class='frame-browser'>
                    <figure>
                        <img alt='' src='<?= Yii::$app->request->baseUrl . "/images/outbond.jpg" ?>'>
                        <figcaption>
                            <h5 class='fp-title'>
                                Outbond
                            </h5>

                            <div class='fp-buttons'>
                                <a class='btn btn-default btn-sm fp-lightbox-btn swipebox hidden-mobile-ib'
                                   href='<?= Yii::$app->request->baseUrl . "/images/outbond.jpg" ?>'
                                   title='Lightbox usage example'>
                                    <i class='icon-zoom-in'></i>
                                    View Bigger
                                </a>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
    </div>
</div>
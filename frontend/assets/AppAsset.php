<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css_new/swipebox.css',
        'css_new/blue.css',
        'css_new/animate.min.css',

    ];

    public $js = [
        'js/jquery.js',
        'js/bootstrap/carousel.js',
        'js/bootstrap/transition.js',
        'js/bootstrap/collapse.js',
        'js/bootstrap/modal.js',
        'js/bootstrap/dropdown.js',
        'js/jquery.scrollUp.min.js',
        'js/jquery.swipebox.min.js',
        'js/jquery.smoothScroll.js',
        'js/application.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = ['position'=>View::POS_HEAD];
}

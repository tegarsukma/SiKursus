<?php

namespace frontend\controllers;

use app\models\Pegawai;
use dmstr\bootstrap\Tabs;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\HttpException;

class PegawaiController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $query = Pegawai::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $searchModel = new $dataProvider;

        Url::remember();
        \Yii::$app->session['__crudReturnUrl'] = null;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            //'searchModel' => $searchModel,
        ]);
    }

    public function actionView($id)
    {
        \Yii::$app->session['__crudReturnUrl'] = Url::previous();
        Url::remember();
        Tabs::rememberActiveState();

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Pegawai::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
}

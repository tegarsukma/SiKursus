<?php

namespace frontend\controllers;

use app\models\Jadwal;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

class JadwalController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $query = Jadwal::find();

        $dataProvider = new ActiveDataProvider([
            'query'=>$query,
        ]);

        //Tabs::clearLocalStorage();

        Url::remember();
        \Yii::$app->session['__crudReturnUrl'] = null;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
           // 'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Jadwal model.
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        \Yii::$app->session['__crudReturnUrl'] = Url::previous();
        Url::remember();
        Tabs::rememberActiveState();

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

}

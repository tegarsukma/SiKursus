<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models;

use Yii;

/**
 * This is the base-model class for table "barang_inventaris".
 *
 * @property integer $id
 * @property string $serial_number
 * @property integer $nip_pegawai
 * @property string $nama_alat
 * @property integer $jumlah
 * @property string $merk
 * @property integer $nama_lab
 * @property string $tanggal_masuk
 * @property string $foto
 * @property string $kondisi
 * @property string $aliasModel

 *
 */
class BarangInventaris extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'barang_inventaris';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['serial_number', 'nama_alat', 'jumlah', 'merk', 'nama_lab', 'tanggal_masuk','kondisi'], 'required'],
            [['jumlah', 'nama_lab'], 'integer'],
            [['serial_number', 'nama_alat', 'merk', 'tanggal_masuk','kondisi'], 'string', 'max' => 100],
            [['foto'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'serial_number' => 'Serial Number',
            'nip_pegawai' => 'Nip Pegawai',
            'nama_alat' => 'Nama Alat',
            'jumlah' => 'Jumlah',
            'merk' => 'Merk',
            'nama_lab' => 'Nama Lab',
            'tanggal_masuk' => 'Tanggal Masuk',
            'kondisi'=>'Kondisi'
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->foto->saveAs('uploads/barang/foto/' . $this->foto->baseName . '.' . $this->foto->extension);
            return true;
        } else {
            return false;
        }
    }


}
